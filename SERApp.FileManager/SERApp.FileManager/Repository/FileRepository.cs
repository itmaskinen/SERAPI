﻿using Google.Apis.Drive.v3;
using SendGrid;
using SendGrid.Helpers.Mail;
using SER.FileManager.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;

namespace SER.FileManager.Repository
{
    public class FileRepository
    {
        readonly DriveService driveService;
        public FileRepository()
        {

            //GoogleCredential creds;

            //using (var stream = new FileStream("C:\\Users\\Langskie\\Downloads\\credentials.json", FileMode.Open, FileAccess.Read))
            //{
            //    creds = GoogleCredential.FromStream(stream).CreateScoped(new[]
            //    {
            //        DriveService.ScopeConstants.DriveFile
            //    });
            //}

            //driveService = new DriveService(new BaseClientService.Initializer()
            //{
            //    HttpClientInitializer = creds,
            //    ApplicationName = "GDrive upload"
            //});
        }

        public bool SendFiles(EmailModel email)
        {
            var apiKey = ConfigurationManager.AppSettings["sendgridkey"];
            var client = new SendGridClient(apiKey);

            var from = new EmailAddress("no-reply@serapp.com", "SER4");

            var res = client.RequestAsync(
                method: BaseClient.Method.GET,
                urlPath: "sender"
            ).Result;

            var body = email.Body ?? "";
            if (!body.Contains("<html>"))
            {
                body = $"<html>{body}<html>";
            }

            var msg = MailHelper.CreateSingleEmail(from, new EmailAddress(email.To), email.Subject, body, body);

            if (!string.IsNullOrEmpty(email.CC))
            {
                msg.AddCc(email.CC);
            }

            email.AttachmentPaths.ForEach(a =>
            {
                var attr = File.GetAttributes(a);
                var zipPath = "";
                var bytes = new byte[0];

                var name = new DirectoryInfo(a).Name;
                if (attr.HasFlag(FileAttributes.Directory))
                {
                    string startPath = a;
                    zipPath = Path.GetFullPath(Path.Combine(a, "..", $"{name}.zip"));


                    ZipFile.CreateFromDirectory(startPath, zipPath);

                    bytes = System.IO.File.ReadAllBytes(zipPath);
                    String base64 = Convert.ToBase64String(bytes);

                    msg.AddAttachment(Path.GetFileName(zipPath), base64);
                }
                else
                {
                    bytes = File.ReadAllBytes(a);
                    String base64 = Convert.ToBase64String(bytes);

                    msg.AddAttachment(Path.GetFileName(a), base64);
                }
                if (!string.IsNullOrEmpty(zipPath))
                    System.IO.File.Delete(zipPath);
            });

            var response = client.SendEmailAsync(msg).Result;

            var error = response.Body.ReadAsStringAsync();
            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {
                return true;
            }
            else
            {
                throw new Exception(error.Result);
            }
        }

        public string UploadFileToGDrive(string filePath)
        {

            var metadata = new Google.Apis.Drive.v3.Data.File()
            {
                Name = Path.GetFileName(filePath),
                Parents = new List<string> { "1pAojezwQWPTIZCKVunA39_eVl0EVL4Ce" }
            };

            FilesResource.CreateMediaUpload request;
            using (var fileStream = new FileStream(filePath, FileMode.Open))
            {
                request = driveService.Files.Create(metadata, fileStream, "");
                request.Fields = "id";
                request.Upload();
            }

            var uploadFile = request.ResponseBody;

            return uploadFile.Id;
        }

        public bool DeleteFile(string id)
        {
            var request = driveService.Files.Delete(id);
            request.Execute();

            return true;
        }

        public bool DownloadFile(string filePath, string id)
        {

            var request = driveService.Files.Get(id);

            MemoryStream stream1 = new MemoryStream();
            request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
            {
                switch (progress.Status)
                {
                    case Google.Apis.Download.DownloadStatus.Completed:
                        {
                            using (FileStream file = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite))
                            {
                                stream1.WriteTo(file);
                            }
                            break;
                        }
                }
            };

            request.Download(stream1);

            return true;
        }
    }
}