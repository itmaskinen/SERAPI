﻿namespace SER.FileManager.Models
{
    public class SiteModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}