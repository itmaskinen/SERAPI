﻿namespace SER.FileManager.Models
{
    public class SiteFolderRole
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string Folder { get; set; }
        public string Path { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }
}