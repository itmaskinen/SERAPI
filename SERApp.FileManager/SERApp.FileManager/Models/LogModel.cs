﻿using System;

namespace SER.FileManager.Models
{
    public class LogModel
    {
        public int LogType { get; set; }
        public string ShortDescription { get; set; }
        public string Message { get; set; }
        public int AccountId { get; set; }
        public int UserId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}