﻿using System.Collections.Generic;

namespace SER.FileManager.Models
{
    public class DataResponseModel
    {
        public List<fileReturnData> files;
        public string message;
        public bool? success;
    }
}
