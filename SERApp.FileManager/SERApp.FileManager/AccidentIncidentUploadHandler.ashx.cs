﻿using Newtonsoft.Json;
using SER.FileManager.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace SER.FileManager
{
    /// <summary>
    /// Summary description for AccidentIncidentUploadHandler
    /// </summary>
    public class AccidentIncidentUploadHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            int i = 0;
            var files = context.Request.Files["uploadedFiles"];

            var AccountId = context.Request.Form["AccountId"];
            var IncidentAccidentId = context.Request.Form["IncidentAccidentId"];
            //var SiteId = context.Request.Form["SiteId"];

            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            string rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", AccountId, "INCIDENT-ACCIDENT", IncidentAccidentId);
            string fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName + @"\" + rootFolderPath;


            var httpRequest = HttpContext.Current.Request;
            foreach (string file in httpRequest.Files)
            {
                var postedFile = httpRequest.Files[i];
                var filePath = Path.Combine(fileManagerRoot, postedFile.FileName);// HttpContext.Current.Server.MapPath("~/UploadedFiles/" + postedFile.FileName);
                try
                {
                    var d = System.IO.Path.GetFileName(filePath);

                    var test = System.IO.Path.GetFullPath(filePath);
                    var g = filePath.Replace(d, "");
                    if (!System.IO.Directory.Exists(g))
                    {
                        Directory.CreateDirectory(g);
                    }


                    postedFile.SaveAs(filePath);
                    // uploadedFileNames.Add(httpRequest.Files[i].FileName);
                    // cntSuccess++;
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                i++;
            }


            var dir = new DirectoryInfo(fileManagerRoot);
            var filesadd = new List<fileReturnData>();
            var html = new StringBuilder();
            foreach (var f in dir.GetFiles())
            {
                //ConvertLargeFile(f.FullName, "test");


                Byte[] bytes = File.ReadAllBytes(f.FullName);
                String file = Convert.ToBase64String(bytes);

                var mime = MimeMapping.GetMimeMapping(f.Name);

                var theIcon = IconFromFilePath(f.FullName); //f.FullName - full path to file
                byte[] imageArray = ImageToByte(FromIconToBitmap(theIcon));
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);

                filesadd.Add(new fileReturnData()
                {
                    Base64 = file,
                    Name = f.Name,
                    Mime = mime,
                    Icon = FileHelper.GetIconFromMimeType(mime)//$"data:image/png;base64,{base64ImageRepresentation}",
                });
                //  html.Append($"<a download='{f.Name}' target='_blank' href=\"data:{mime};base64," + file + "\">");
                // html.Append(f.Name.ToString() + "</a><br />");
            }
            if (filesadd.Any())
            {
                filesadd = filesadd.OrderBy(x => x.Name).ToList();
            }
            //context.Response.ContentType = "text/html";
            context.Response.ContentType = "application/json";
            var json = JsonConvert.SerializeObject(filesadd);


            var tt = html.ToString();
            context.Response.Write(json);


            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
        }

        public Icon IconFromFilePath(string filePath)
        {
            var result = (Icon)null;

            try
            {
                result = Icon.ExtractAssociatedIcon(filePath);
            }
            catch (System.Exception)
            {
            }

            return result;
        }

        public Bitmap FromIconToBitmap(Icon icon)
        {
            Bitmap bmp = new Bitmap(icon.Width, icon.Height);
            using (Graphics gp = Graphics.FromImage(bmp))
            {
                gp.Clear(Color.Transparent);
                gp.DrawIcon(icon, new Rectangle(0, 0, icon.Width, icon.Height));
            }
            return bmp;
        }

        public byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        private class fileReturnData
        {
            public string Name { get; set; }
            public String Base64 { get; set; }
            public string Mime { get; set; }
            public String Icon { get; set; }
        }
    }
}