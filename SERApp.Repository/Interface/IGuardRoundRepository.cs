﻿using SERApp.Data.Models;
using SERApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface IGuardRoundRepository
    {
        int StartRound(GuardRoundLogs data);
        void StopRound(GuardRoundLogs data);
        void SaveNotes(GuardRoundLogs data);
        List<GuardRoundLogs> GetRoundsByGuardId(int guardRoundId, int guardId, DateTime date);
        List<GuardRoundLogModel> GetActiveRoundsByDate(DateTime start, DateTime end, int siteId, int accountId);
        bool IsPendingRound(int guardRoundId, int guardId);
        int GetGuardId(int userId, int guardRoundId);
    }
}
