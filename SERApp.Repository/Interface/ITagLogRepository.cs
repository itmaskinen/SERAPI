﻿using SERApp.Data.Models;
using System.Collections.Generic;

namespace SERApp.Repository.Interface
{
    public interface ITagLogRepository : IRepository<TagLog>
    {
        IEnumerable<TagLog> GetAllTagLogsIncludeChilds();
        TagLog GetTagLogsIncludeChilds(int id);
    }
}
