﻿using SERApp.Data.Models;
using SERApp.Models;
using System.Collections.Generic;

namespace SERApp.Repository.Interface
{
    public interface ICustomerRepository
    {
        Customer Get(int id);
        List<Customer> GetAll();
        List<Customer> GetAllActive();
        void DeleteCustomer(int id);
        void SaveCustomer(CustomerModel model);
    }
}
