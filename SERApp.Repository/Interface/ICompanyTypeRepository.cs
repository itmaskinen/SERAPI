﻿using SERApp.Data.Models;
using System.Collections.Generic;

namespace SERApp.Repository.Interface
{
    public interface ICompanyTypeRepository : IRepository<CompanyType>
    {
        IEnumerable<CompanyType> GetAllGuardsIncldeChild();
    }
}
