﻿using SERApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface ILocationRepository
    {
        List<Building> GetBuildings(int accountId);
        List<Level> GetLevels(int accountId);
        List<Room> GetRooms(int accountId);
        void AddBuilding(Building building);
        void AddLevel(Level level);
        void AddRoom(Room room);
        void DeleteBuilding(int id);
        void DeleteLevel(int id);
        void DeleteRoom(int id);
    }
}
