﻿using SERApp.Data.Models;
using System.Collections.Generic;

namespace SERApp.Repository.Interface
{
    public interface IType1FieldRepository : IRepository<Type1Field>
    {
        List<Type1Field> FindByTypeId(int id);
    }
}
