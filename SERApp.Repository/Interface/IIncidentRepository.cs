﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using System;
using System.Collections.Generic;

namespace SERApp.Repository.Interface
{
    public interface IIncidentRepository : IRepository<Incident>
    {
        IEnumerable<Incident> GetAllIncidentsChildIncluded(int siteId);
        Incident GetWithChildsIncluded(int id);
        IEnumerable<IncidentListModel> GetAllForList(int siteId, int? accountId, DateTime? date = null, DateTime? endDate = null, int? selectedType1 = null, int? selectedType2 = null, int? selectedType3 = null, int reportType = 0, int objectType = 0, bool isLatestOnly = false, int id = 0, List<int> tenantIds = null);
        Incident GetWithChildsIncludedV2(int id);
        List<Type1FieldValue> GetType1FieldValue(int id);
        IncidentStatisticsModel GetStatistics(DateTime start, DateTime end, DailyReportStatisticsRequestModel model);
        List<dynamic> GetMinMaxStatsDate(int accountId, int siteId);
        IEnumerable<PublicIncidentModel> GetPublicIncidents(int accountId, string startDate, string endDate);
        void FixImages();
        string GetImage(int order, int id);
        ExportStatisticsFields GetExportPreFields(int category, int siteId);
        void SaveExportPreFields(List<ExportStatisticsFields> data);
        List<IncidentObjectModel> GetIncidentByRounds(int siteId, int accountId, int guardRoundId, DateTime start, DateTime end);
        List<GuardModel> GetGuardAccounts(int accountId);
        void UpdateDescription(int id, string message);
    }
}
