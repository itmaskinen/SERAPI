﻿using SERApp.Data.Models;
using System;
using System.Collections.Generic;

namespace SERApp.Repository.Interface
{
    public interface IGuardRepository : IRepository<Guard>
    {
        IEnumerable<Guard> GetAllGuardsIncldeChild();
        IEnumerable<Guard> GetBySiteIdAndDate(int siteId, DateTime date);
    }
}
