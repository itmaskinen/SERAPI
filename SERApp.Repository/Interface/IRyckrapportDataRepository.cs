﻿using SERApp.Data.Models;
using System.Collections.Generic;

namespace SERApp.Repository.Interface
{
    public interface IRyckrapportDataRepository : IRepository<RyckrapportData>
    {
        IEnumerable<RyckrapportData> GetAllRyckrapportChildIncluded();
    }
}
