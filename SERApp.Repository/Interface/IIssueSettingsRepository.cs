﻿using SERApp.Data.Models;

namespace SERApp.Repository.Interface
{
    public interface IIssueSettingsRepository : IRepository<IssueSettings>
    {
        IssueSettings GetByAccountId(int accountId);
    }
}
