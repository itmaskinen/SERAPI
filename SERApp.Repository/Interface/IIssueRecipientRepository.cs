﻿using SERApp.Data.Models;
using System.Collections.Generic;

namespace SERApp.Repository.Interface
{
    public interface IIssueRecipientRepository : IRepository<IssueRecipients>
    {
        List<IssueRecipients> GetAllRecipients(int siteId);
        List<IssueRecipientTypes> GetAllTypes();
    }
}
