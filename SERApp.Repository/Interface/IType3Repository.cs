﻿using SERApp.Data.Models;
using System.Collections.Generic;

namespace SERApp.Repository.Interface
{
    public interface IType3Repository : IRepository<Type3>
    {
        IEnumerable<Type3> GetAllType3IncludeChildren();
        Type3 GetType3IncldeChildren(int id);
        Type3 GetType3ByTenantTypeIdAndSiteId(int tenantType, int siteId);
    }
}
