﻿using SERApp.Data.Models;
using System.Collections.Generic;

namespace SERApp.Repository.Interface
{
    public interface IDailyReportFieldsRepository : IRepository<DailyReportFields>
    {
        new List<DailyReportFields> GetAll();
    }
}
