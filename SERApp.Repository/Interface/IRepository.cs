﻿using System;
using System.Linq;

namespace SERApp.Repository.Interface
{
    public interface IRepository<T> where T : class
    {
        void Delete(int id);
        T Save(T data);
        IQueryable<T> GetAll();
        IQueryable<T> GetByPredicate(Func<T, bool> predicate);
        T Get(int Id);
        IQueryable<T> GetAllByRawString(string query);
        void Update(T data);
        int UpdateAll(string query);
        int ExecuteRawSql(string command);
    }
}
