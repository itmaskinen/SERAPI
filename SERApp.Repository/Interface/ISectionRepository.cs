﻿using SERApp.Data.Models;
using System.Collections.Generic;

namespace SERApp.Repository.Interface
{
    public interface ISectionRepository : IRepository<Section>
    {
        IEnumerable<Section> GetSectionIncludeChildren();
    }
}
