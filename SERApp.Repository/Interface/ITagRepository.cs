﻿using SERApp.Data.Models;
using System.Collections.Generic;

namespace SERApp.Repository.Interface
{
    public interface ITagRepository : IRepository<Tag>
    {
        IEnumerable<Tag> GetAllTagsIncludeChilds();
        Tag GetTagIncludeChilds(int id);
    }
}
