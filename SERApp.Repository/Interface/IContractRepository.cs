﻿using SERApp.Models;
using SERApp.Models.Common;
using System.Collections.Generic;

namespace SERApp.Repository.Interface
{
    public interface IContractRepository
    {
        List<ContractModel> GetContractsByIds(string ids);
        List<ContractModel> GetAllContractsForExport(ContractFilters filters);
    }
}
