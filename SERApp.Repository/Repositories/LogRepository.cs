﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Models.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SERApp.Repository.Repositories
{
    public class LogRepository
    {
        private string connString = AppSettings.ConnectionString;
        private SERAppDBContext db;
        public LogRepository()
        {
            db = new SERAppDBContext(connString);
        }

        public Log GetLog(int id)
        {
            return db.Logs.Where(l => l.Id == id).SingleOrDefault();
        }

        public List<LogModel> GetLogs(string module, int accountId, int? siteId, string reportType, string shortDesc = "SENDEMAIL")
        {
            var result = db.Logs.Include(e => e.Site)
                .Where(x => x.AccountId == accountId &&
                (!siteId.HasValue || x.SiteId.Value == siteId) &&
                x.ShortDescription == shortDesc &&
                (string.IsNullOrEmpty(module) || x.Module == module) &&
                (string.IsNullOrEmpty(reportType) || x.ReportType == reportType))
                .OrderByDescending(e => e.CreatedDate)
                .ToList();

            return result.Select(e => new LogModel()
            {
                Id = e.Id,
                LogType = e.LogType,
                ShortDescription = e.ShortDescription,
                Message = e.Message,
                Site = e.Site.Name,
                Recepients = e.Recepients,
                Module = module,
                CreatedDate = e.CreatedDate,
            }).ToList();
        }

        public Log GetLog(LogTypeEnum logType, string shortDesc, int accountId, int userId)
        {
            return db.Logs.Where(l => l.UserId == userId && l.LogType == (int)logType && l.ShortDescription == shortDesc
                && (accountId > 0 ? l.AccountId == accountId : true))
                .OrderByDescending(l => l.CreatedDate)
                .FirstOrDefault();
        }


        public Log GetLogFromLogs(LogTypeEnum logType, string shortDesc, int accountId, int userId, List<Log> logs)
        {
            return logs.Where(l => l.UserId == userId && l.LogType == (int)logType && l.ShortDescription == shortDesc
                && (accountId <= 0 || l.AccountId == accountId))
                .OrderByDescending(l => l.CreatedDate)
                .FirstOrDefault();
        }

        public List<Log> GetAll()
        {
            return db.Logs.ToList();
        }

        public List<Log> GetAllInfoLogs()
        {
            return db.Logs.Where(l => l.LogType == (int)LogTypeEnum.Information).ToList();
        }

        public List<Log> GetAllWarningLogs()
        {
            return db.Logs.Where(l => l.LogType == (int)LogTypeEnum.Warning).ToList();
        }

        public List<Log> GetAllErrorLogs()
        {
            return db.Logs.Where(l => l.LogType == (int)LogTypeEnum.Error).ToList();
        }

        public List<Log> GetAllNotificationLogs()
        {
            return db.Logs.Where(l => l.LogType == (int)LogTypeEnum.Notification).ToList();
        }

        public void Log(LogTypeEnum logType, string shortDesc, string message, int accountId, int userId, string recepients = null, int? siteId = null, string reportType = null, string module = null)
        {
            CreateLog(new LogModel()
            {
                LogType = (int)logType,
                ShortDescription = shortDesc,
                Message = message,
                AccountId = accountId,
                UserId = userId,
                SiteId = siteId,
                ReportType = reportType,
                Recepients = recepients,
                Module = module
            });
        }

        public void LogException(Exception ex, string shortDesc, int accountId, int userId)
        {
            string message = string.Empty;
            if (!string.IsNullOrEmpty(ex.Message))
            {
                message = ex.Message;
            }
            if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
            {
                message += "/n" + ex.InnerException.Message;
            }
            if (!string.IsNullOrEmpty(ex.StackTrace))
            {
                message += "/n" + ex.StackTrace;
            }
            CreateLog(new LogModel()
            {
                LogType = (int)LogTypeEnum.Error,
                ShortDescription = shortDesc,
                Message = message,
                AccountId = accountId,
                UserId = userId
            });
        }

        private void CreateLog(LogModel model)
        {
            Log newLog = new Log()
            {
                LogType = model.LogType,
                ShortDescription = model.ShortDescription,
                Message = model.Message,
                AccountId = model.AccountId,
                UserId = model.UserId,
                CreatedDate = DateTime.Now,
                Recepients = model.Recepients,
                SiteId = model.SiteId,
                Module = model.Module,
                ReportType = model.ReportType
            };
            db.Logs.Add(newLog);
            db.SaveChanges();
        }
    }
}
