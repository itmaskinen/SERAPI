﻿using Newtonsoft.Json;
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Models.Constants;
using SERApp.Repository.Interface;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using static SERApp.Models.Enums.IncidentStatisticsEnum;

namespace SERApp.Repository.Repositories
{
    public class IncidentRepository : Repository<Incident>, IRepository<Incident>, IIncidentRepository
    {
        private IRepository<Type1> _type1Repository;
        private IRepository<Type2> _type2Repository;
        private IRepository<Type3> _type3Repository;
        private IRepository<Incident> _incidentRepository;
        private IRepository<ExportStatisticsFields> _exportFieldsRepository;
        private MediaService _mediaService;
        public IncidentRepository()
        {
            _type1Repository = new Repository<Type1>();
            _type2Repository = new Repository<Type2>();
            _type3Repository = new Repository<Type3>();
            _incidentRepository = new Repository<Incident>();
            _exportFieldsRepository = new Repository<ExportStatisticsFields>();
            _mediaService = new MediaService();
        }

        public IEnumerable<PublicIncidentModel> GetPublicIncidents(int accountId, string startDate, string endDate)
        {
            string query = $@"
                select DISTINCT i.Id, t.[Name] as 'Vad', t2.[Name] as 'Var', t3.[Name] as 'Vem', s.[Name] as 'Site',
                        CASE WHEN i.ActionDescription IS NULL THEN 
	                            REPLACE((SELECT TOP 1 fv.[value] FROM Field_Values fv 
                                INNER JOIN Type1_Fields tf on tf.fieldId = fv.Type1_FieldId
                                WHERE IncidentId = i.Id and Type='TEXTAREA' and fv.value != 'null' and tf.Type1Id = t.id and tf.Label = 'DESCRIPTION'), '""', '') 
                            ELSE i.ActionDescription
                        END
                        as 'Description',
                    i.[time] as 'Date'
                from incidents i
                inner join [sites] s on s.id = i.siteid AND s.IsActive = 1
                inner join type1 t on t.id = i.type1id
                LEFT join type2 t2 on t2.id = i.type2id
                LEFT join type3 t3 on t3.id = i.type3id
                where i.accountid={accountId} and i.time >= '{startDate}' AND i.time < '{endDate}'
            ";

            return dbContext.Database.SqlQuery<PublicIncidentModel>(query).ToList();
        }

        public IEnumerable<IncidentListModel> GetAllForList(int siteId, int? accountId, DateTime? date, DateTime? endDate, int? selectedType1, int? selectedType2, int? selectedType3, int reportType = 0, int objectType = 0, bool isLatestOnly = false, int id = 0, List<int> tenantIds = null)
        {
            var datequery = "";
            var accountquery = "";
            var siteQuery = "";
            var type1Query = selectedType1 > 0 ? $"AND i.type1id = {selectedType1}" : "";
            var type2Query = selectedType2 > 0 ? $"AND i.type2id = {selectedType2}" : "";
            var type3Query = selectedType3 > 0 ? $"AND i.type3id = {selectedType3}" : "";
            var tenantQuery = "";
            if (tenantIds != null)
            {
                tenantQuery = $"AND i.tenantId in ({string.Join(", ", tenantIds)})";
            }

            var reportTypeJoinQuery = reportType > 1 ? "LEFT JOIN TenantTypes tt on t.TenantType = tt.Id" : string.Empty;
            var reportTypeWhereQuery = reportType > 1 ? $"AND tt.isEquipment = {(reportType == 2 ? 1 : 0)} AND i.TenantId is not null" : (reportType == 1 ? "AND  i.TenantId is null" : "");

            var objectTypeWhereQuery = objectType > 0 ? $"AND i.tenantId = {objectType}" : string.Empty;


            if (accountId.HasValue)
            {
                accountquery = $"AND i.AccountId = {accountId}";
            }

            if (date.HasValue)
            {
                datequery += $"AND Cast(Time as DATE)>= '{date.Value.ToString("yyyy-MM-dd")} 00:00' AND Cast(Time as DATE)< '{endDate.Value.AddDays(1):yyyy-MM-dd} 00:00'";
            }

            if (siteId != 0)
            {
                siteQuery = $@"AND i.SiteId = {siteId}";
            }
            var query = string.Empty;

            if (isLatestOnly)
            {
                query = $@"SELECT  TOP 1 i.id AS Id, 
                                  Date = i.time,
                                  i.time AS Time, 
                                  t1.id AS Type1Id, 
                                  t1.NAME AS Type1Name, 
                                  t2.id AS Type2Id, 
                                  t2.NAME AS Type2Name, 
                                  t3.id AS Type3Id, 
                                  ISNULL(t3.NAME, '') + CASE 
                                        WHEN t.NAME IS NULL then ''
                                        WHEN t3.Name is null and t.Name is not null THEN '_ - ' + t.NAME
                                        ELSE ' - ' + t.NAME END AS Type3Name, 
                                  i.quantity AS Q, 
                                  i.policeresponse AS P, 
                                  i.accountid AS AccountId, 
                                  i.siteid AS SiteId, 
                                  s.NAME AS SiteName, 
					              Guard = g.Name,
                                  Description = i.ActionDescription,
                                  FieldValues = fv.[value],
					              FieldType = fv.type,
					              FieldLabel = tf.label,
                                  i.RoundLogId
                        FROM       incidents i 
                                  LEFT JOIN type1 t1 ON i.type1id = t1.id 
                                  LEFT JOIN type2 t2 ON i.type2id = t2.id 
                                  LEFT JOIN type3 t3 ON i.type3id = t3.id 
                                  LEFT JOIN Type1Setting ts on ts.type1Id = t1.id and ts.siteId = i.siteId
                                  LEFT JOIN Field_Values fv on fv.incidentId = i.id
                                  LEFT JOIN IncidentGuard ig on ig.IncidentId = i.Id
                                  LEFT JOIN Guards g on g.Id = ig.GuardId
                                  LEFT JOIN Type1_Fields tf on tf.fieldId = fv.Type1_FieldId and tf.Type1Id = t1.id
                                  LEFT JOIN sites s ON i.siteid = s.id 
                                  LEFT JOIN tenants t ON t.id = i.tenantid
                        WHERE ({siteId} = 0 OR i.siteId = {siteId}) and ({accountId} = 0 or i.accountId = {accountId}) and ({id} = 0 or i.id = {id})
                        ORDER BY i.Time DESC";
            }
            else
            {

                var otherDateFieldQuery = @"SELECT fv1.incidentid
			                    from Field_Values fv1
				                    WHERE fv1.Value like '%{""year"" % ' AND
					                    CAST(CONCAT(JSON_VALUE(fv1.[value], '$.year'), '-',JSON_VALUE(fv1.[value], '$.month'), '-',JSON_VALUE(fv1.[value], '$.day')) AS DATE) >=  '" + date.Value.ToString("yyyy-MM-dd") + "' AND " +
                                            "CAST(CONCAT(JSON_VALUE(fv1.[value], '$.year'), '-',JSON_VALUE(fv1.[value], '$.month'), '-',JSON_VALUE(fv1.[value], '$.day')) AS DATE) < '" + endDate.Value.AddDays(1).ToString("yyyy-MM-dd") + "'";

                var incidentsOtherFieldDate = dbContext.Database.SqlQuery<int>(otherDateFieldQuery).ToList();
                incidentsOtherFieldDate = !incidentsOtherFieldDate.Any() ? new List<int>() { 0 } : incidentsOtherFieldDate;
                var ids = string.Join(",", incidentsOtherFieldDate);
                query = $@"
                SELECT 
                      i.id AS Id, 
                      Date = i.time,
                      i.time AS Time, 
                      t1.id AS Type1Id, 
                      t1.NAME AS Type1Name, 
                      t2.id AS Type2Id, 
                      t2.NAME AS Type2Name, 
                      t3.id AS Type3Id, 
                      ISNULL(t3.NAME, '') + CASE 
                                                WHEN t.NAME IS NULL then ''
                                                WHEN t3.Name is null and t.Name is not null THEN '_ - ' + t.NAME
                                                ELSE ' - ' + t.NAME END AS Type3Name, 
                      i.quantity AS Q, 
                      i.policeresponse AS P, 
                      i.accountid AS AccountId, 
                      i.siteid AS SiteId, 
                      i.TenantId,
                      s.NAME AS SiteName, 
					  Guard = g.Name,
                      Description = i.ActionDescription,
                      FieldValues = fv.[value],
					  FieldType = fv.type,
					  FieldLabel = tf.label,
                      i.RoundLogId
                    FROM 
                      incidents i 
                      LEFT JOIN type1 t1 ON i.type1id = t1.id 
                      LEFT JOIN type2 t2 ON i.type2id = t2.id 
                      LEFT JOIN type3 t3 ON i.type3id = t3.id 
                      LEFT JOIN Type1Setting ts on ts.type1Id = t1.id and ts.siteId = i.siteId
                      LEFT JOIN Field_Values fv on fv.incidentId = i.id
                      LEFT JOIN IncidentGuard ig on ig.IncidentId = i.Id
                      LEFT JOIN Guards g on g.Id = ig.GuardId
                      LEFT JOIN Type1_Fields tf on tf.fieldId = fv.Type1_FieldId and tf.Type1Id = t1.id
                      LEFT JOIN sites s ON i.siteid = s.id 
                      LEFT JOIN tenants t ON t.id = i.tenantid
                        {reportTypeJoinQuery}
                      WHERE (ts.isactive = 1 or ts.type1id is null) AND (t2.isactive = 1 or t2.id is null) AND (t3.isactive = 1 or t3.id is null) {siteQuery} {accountquery} {type1Query} {type2Query} {type3Query} {reportTypeWhereQuery} {objectTypeWhereQuery}
                      {tenantQuery} AND (i.id in ({ids}) OR (i.id not in ({ids}) {datequery}))";

            }

            var dataIQueryable = dbContext.Database.SqlQuery<IncidentTempList>(query).ToList();

            var distinct = dataIQueryable.GroupBy(e => e.Id);

            var list = new List<IncidentListModel>();
            foreach (var item in distinct)
            {
                var curr = new IncidentListModel()
                {
                    Id = item.Key,
                    Date = item.First().Date,
                    Time = item.First().Time,
                    Type1Id = item.First().Type1Id,
                    Type1Name = item.First().Type1Name,
                    Type2Id = item.First().Type2Id,
                    Type2Name = item.First().Type2Name,
                    Type3Id = item.First().Type3Id,
                    Type3Name = item.First().Type3Name,
                    Description = item.First().Description,
                    Q = item.First().Q,
                    P = item.First().P,
                    AccountId = item.First().AccountId,
                    SiteId = item.First().SiteId,
                    SiteName = item.First().SiteName,
                    HasImageAttached = _mediaService.HasMedias($"DailyReport/{item.Key}"),
                    TenantId = item.First().TenantId,
                    RoundLogId = item.First().RoundLogId,
                };

                var customFields = item.Select(e => new { e.FieldLabel, e.FieldType, e.FieldValues });
                var customDate = customFields.FirstOrDefault(e => e.FieldLabel == "Date")?.FieldValues?.ToLower();
                var customTime = customFields.FirstOrDefault(e => e.FieldLabel == "Time")?.FieldValues?.ToLower();
                if (!string.IsNullOrEmpty(customDate))
                {
                    var parsed = JsonConvert.DeserializeObject<dynamic>(customDate);
                    curr.Date = new DateTime((int)parsed.year, (int)parsed.month, (int)parsed.day, curr.Time.Hour, curr.Time.Minute, 0);
                    curr.Time = curr.Date;
                }

                if (!string.IsNullOrEmpty(customTime))
                {
                    var parsed = JsonConvert.DeserializeObject<dynamic>(customTime);
                    curr.Time = new DateTime(curr.Date.Year, curr.Date.Month, curr.Date.Day, (int)parsed.hour, (int)parsed.minute, 0);
                }

                var closedFrom = customFields.FirstOrDefault(e => e.FieldLabel == "Closed From")?.FieldValues;
                curr.ClosedFrom = closedFrom;

                var closedTo = customFields.FirstOrDefault(e => e.FieldLabel == "Closed To")?.FieldValues;
                curr.ClosedTo = closedTo;

                var description = customFields.FirstOrDefault(e =>
                    new string[] { "Additional Text", "Reason", "Description" }.Contains(e.FieldLabel) &&
                    e.FieldValues != "'null'" &&
                    e.FieldType == "TEXTAREA"
                    )?.FieldValues?.Replace("\"", "");

                curr.Description = description ?? curr.Description;

                var guards = item.Select(e => e.Guard).Where(e => !string.IsNullOrEmpty(e)).Distinct();
                if (guards.Any())
                {
                    //added createddate to fix mobile app version in prod
                    curr.Guards.AddRange(guards.Select(e => new GuardIncidentModel() { Name = e, CreatedDate = DateTime.Now }).ToList());
                }

                list.Add(curr);
            }

            return list;

        }

        public IEnumerable<Incident> GetAllIncidentsChildIncluded(int siteId)
        {
            if (siteId == 0)
            {
                return dbContext.Incidents.Include(x => x.Type2)
                              .Include(x => x.Type1)
                              .Include(x => x.Type3)
                              .Include(x => x.IncidentGuard)
                              .Include(x => x.Tenant)
                              .Include(x => x.Tenant.Site)
                              .Include(x => x.IncidentGuard.Select(y => y.Guard))
                              .Include(x => x.Reports)
                              .Include(x => x.Reports.Select(y => y.ReportType))
                              .ToList();
            }

            return dbContext.Incidents.Include(x => x.Type2)
                .Include(x => x.Type1)
                .Include(x => x.Type3)
                .Include(x => x.IncidentGuard)
                .Include(x => x.Tenant)
                .Include(x => x.Tenant.Site)
                .Include(x => x.IncidentGuard.Select(y => y.Guard))
                .Include(x => x.Reports)
                .Include(x => x.Reports.Select(y => y.ReportType))
                .Where(x => x.SiteId == siteId)
                .ToList();
        }

        public Incident GetWithChildsIncluded(int id)
        {

            return dbContext.Incidents.Include(x => x.Type2)
            .Include(x => x.Type1)
            .Include(x => x.Type3)
            .Include(x => x.IncidentGuard)
            .Include(x => x.Tenant)
            .Include(x => x.Tenant.Site)
            .Include(x => x.IncidentGuard.Select(y => y.Guard))
            .Include(x => x.Reports)
            .Include(x => x.Reports.Select(y => y.ReportImages))
            .Include(x => x.Reports.Select(y => y.ReportType))
             .SingleOrDefault(x => x.Id == id);
        }

        public string GetImage(int order, int id)
        {
            var query = $"SELECT image{order} FROM Incidents WHERE Id = {id}";
            return dbContext.Database.SqlQuery<string>(query).FirstOrDefault();
        }

        public Incident GetWithChildsIncludedV2(int id)
        {
            var query = $"SELECT * FROM Incidents WHERE Id = {id}";
            var data = dbContext.Database.SqlQuery<Incident>(query).FirstOrDefault();

            var type1s = $"SELECT * FROM Type1 WHERE AccountId = {data.AccountId}";
            var type2s = $"SELECT * FROM Type2 WHERE AccountId = {data.AccountId}";
            var type3s = $"SELECT * FROM Type3 WHERE AccountId = {data.AccountId}";

            var incidentGuards = $"SELECT * FROM IncidentGuard WHERE IncidentId = {id}";
            var tenants = $"SELECT * FROM Tenants WHERE SiteId = {data.SiteId}";

            var tenantsData = dbContext.Database.SqlQuery<Tenant>(tenants).ToList();
            var guardsData = new List<Guard>();
            if (tenantsData.Any())
            {
                var idsstring = string.Join(",", tenantsData.Select(x => x.SiteId.ToString()).ToArray());
                var sites = $"SELECT * FROM Sites WHERE Id IN ({idsstring})";

                var sitesData = dbContext.Database.SqlQuery<Site>(sites).ToList();

                var siteidsstring = string.Join(",", sitesData.Select(x => x.Id.ToString()).ToArray());
                var guards = $"SELECT * FROM Guards WHERE SiteId IN ({siteidsstring})";
                guardsData = dbContext.Database.SqlQuery<Guard>(guards).ToList();
            }

            var type1Data = dbContext.Database.SqlQuery<Type1>(type1s).ToList();
            var type2Data = dbContext.Database.SqlQuery<Type2>(type2s).ToList();
            var type3Data = dbContext.Database.SqlQuery<Type3>(type3s).ToList();
            var incidentGuardData = dbContext.Database.SqlQuery<IncidentGuard>(incidentGuards).ToList();



            data.Type1 = type1Data.SingleOrDefault(x => x.Id == data.Type1Id);
            data.Type2 = type2Data.SingleOrDefault(x => x.Id == data.Type2Id);
            data.Type3 = type3Data.SingleOrDefault(x => x.Id == data.Type3Id);

            data.Tenant = tenantsData.SingleOrDefault(x => x.Id == data.TenantId);
            data.IncidentGuard = incidentGuardData.Where(x => x.IncidentId == data.Id).ToList();
            data.IncidentGuard.ToList().ForEach(x =>
            {
                x.Guard = guardsData.SingleOrDefault(r => r.Id == x.GuardId);
            });

            return data;
        }

        public List<Type1FieldValue> GetType1FieldValue(int id)
        {
            var query = $"SELECT * FROM Field_Values WHERE IncidentId = {id}";
            var data = dbContext.Database.SqlQuery<Type1FieldValue>(query).ToList();

            return data;
        }

        public IncidentStatisticsModel GetStatistics(DateTime start, DateTime end, DailyReportStatisticsRequestModel model)
        {
            return GetStatsByCategory(start, end, model);
        }

        public IncidentStatisticsModel GetStatsByMonthComparison(DateTime start, DateTime end, DailyReportStatisticsRequestModel model)
        {
            var stats = new IncidentStatisticsModel
            {
                labels = new List<string>(),
                datasets = new List<Datasets>()
            };

            var dates = new List<DateTime>();

            string type = string.Empty;
            switch ((DataType)model.DataType)
            {
                case DataType.Vad:
                    type = "Type1";
                    break;
                case DataType.Var:
                    type = "Type2";
                    break;
                case DataType.Vem:
                    type = "Type3";
                    break;
            }

            var monthCtr = 0;
            while (monthCtr < 12)
            {
                var date = DateTime.MinValue.AddMonths(monthCtr);
                stats.labels.Add(date.ToString("MMMM"));
                monthCtr++;
            }

            var currYear = start.Date;
            dates.Add(currYear);
            while (currYear.Date <= end.Date)
            {
                currYear = currYear.AddYears(1);
                dates.Add(currYear);
            }


            var groupedQuery = CountByDateQuery(dates.First(), dates.Last(), model.AccountId, model.SiteId, "MMMM yyyy", type, model.SelectedTypes);
            var idQuery = IdQuery(model.SelectedTypes, type, model.SiteId, model.AccountId);

            var typeInfos = dbContext.Database.SqlQuery<TypeInfo>(idQuery).ToList();
            var typeGrouped = dbContext.Database.SqlQuery<IncidentStatsGrouped>(groupedQuery).ToList();

            var index = 0;
            dates.RemoveAt(dates.Count - 1);
            foreach (var row in dates)
            {
                var values = new List<int>();
                values = stats.labels.Select(date => typeGrouped.FirstOrDefault(e => e.Date == $"{date} {row:yyyy}")?.Count ?? 0).ToList();
                stats.datasets.Add(new Datasets { data = values, label = $"{row:yyyy}" });
                index++;
            }

            stats.title = $"{typeInfos.First().Name} ({start:yyyy} to {end:yyyy})";
            return stats;
        }

        public IncidentStatisticsModel GetStatsByDate(DateTime start, DateTime end, DailyReportStatisticsRequestModel model)
        {
            var stats = new IncidentStatisticsModel
            {
                labels = new List<string>(),
                datasets = new List<Datasets>()
            };
            string format = string.Empty;
            string type = string.Empty;
            var dates = new List<DateTime>();
            var labels = new List<string>();

            switch ((DataType)model.DataType)
            {
                case DataType.Vad:
                    type = "Type1";
                    break;
                case DataType.Var:
                    type = "Type2";
                    break;
                case DataType.Vem:
                    type = "Type3";
                    break;
            }

            switch ((DateType)model.DateType)
            {
                case DateType.DAY:
                    format = "yyyy-MM-dd";
                    var currDay = start.Date;
                    dates.Add(currDay);
                    while (currDay.Date <= end.Date)
                    {
                        labels.Add(currDay.ToString("yyyy-MM-dd"));

                        currDay = currDay.AddDays(1);
                        dates.Add(currDay.Date);
                    }
                    //groupedQuery = ByDayQuery(dates.First(), dates.Last(), accountId, siteId);
                    break;
                case DateType.MONTH:
                    format = "yyyy-MM";
                    var currMonth = start.Date;
                    dates.Add(currMonth);
                    while (currMonth.Date <= end.Date)
                    {
                        labels.Add($"{currMonth:MMMM} ({currMonth:yyyy})");

                        currMonth = currMonth.AddMonths(1);
                        dates.Add(currMonth.Date);
                    }
                    //groupedQuery = ByMonthQuery(dates.First(), dates.Last(), accountId, siteId);
                    break;
                case DateType.YEAR:
                    format = "yyyy";
                    var currYear = start.Date;
                    dates.Add(currYear);
                    while (currYear.Date <= end.Date)
                    {
                        labels.Add(currYear.ToString("yyyy"));

                        currYear = currYear.AddYears(1);
                        dates.Add(currYear);
                    }
                    break;
                default:
                    break;
            }

            stats.labels = labels;

            var groupedQuery = CountByGroupDateQuery(dates.First(), dates.Last(), model.AccountId, model.SiteId, format, type);
            var idQuery = IdQuery(model.SelectedTypes, type, model.SiteId, model.AccountId);

            var typeInfos = dbContext.Database.SqlQuery<TypeInfo>(idQuery).ToList();
            var typeGrouped = dbContext.Database.SqlQuery<IncidentStatsGrouped>(groupedQuery).ToList();

            var index = 0;
            dates.RemoveAt(dates.Count - 1);
            foreach (var row in typeInfos)
            {
                var values = new List<int>();
                values = dates.Select(date => typeGrouped.FirstOrDefault(e => e.Id == row.Id && e.Date == date.ToString(format))?.Count ?? 0).ToList();
                stats.datasets.Add(new Datasets { data = values, label = row.Name, backgroundColor = new Colors().Get(index) });
                index++;
            }

            stats.title = $"{string.Join(", ", typeInfos.Select(e => e.Name))} ({start.ToString(format)} to {end.ToString(format)})";
            return stats;
        }

        public IncidentStatisticsModel GetStatsByCategory(DateTime start, DateTime end, DailyReportStatisticsRequestModel model)
        {
            var idQuery = "";
            var type = "";
            var format = "";
            var actualEnd = end;

            if (model.IsBasic)
            {
                model.DataType = (int)DataType.Vad;
                model.SelectedTypes = null;
                var selectedCategory = _exportFieldsRepository.GetByPredicate(e => e.SiteId == model.SiteId).ToList();

                var cat = selectedCategory.FirstOrDefault(e => e.Category == 3)?.Value ?? null;
                if (selectedCategory != null)
                {
                    model.DataType = string.IsNullOrEmpty(cat) ? (int)DataType.Vad : Convert.ToInt32(selectedCategory.FirstOrDefault(e => e.Category == 3)?.Value);
                }

                var vad = selectedCategory.FirstOrDefault(e => e.Category == 0)?.Value ?? null;
                var var = selectedCategory.FirstOrDefault(e => e.Category == 1)?.Value ?? null;
                var vem = selectedCategory.FirstOrDefault(e => e.Category == 2)?.Value ?? null;

                switch ((DataType)model.DataType)
                {
                    case DataType.Vad:
                        model.SelectedTypes = string.IsNullOrEmpty(vad) ? null : vad.Split(',').Select(e => Convert.ToInt32(e)).ToList();
                        break;
                    case DataType.Var:
                        model.SelectedTypes = string.IsNullOrEmpty(var) ? null : var.Split(',').Select(e => Convert.ToInt32(e)).ToList();
                        break;
                    case DataType.Vem:
                        model.SelectedTypes = string.IsNullOrEmpty(vem) ? null : vem.Split(',').Select(e => Convert.ToInt32(e)).ToList();
                        break;
                }
            }

            switch ((DataType)model.DataType)
            {
                case DataType.Vad:
                    type = "Type1";
                    break;
                case DataType.Var:
                    type = "Type2";
                    break;
                case DataType.Vem:
                    type = "Type3";
                    break;
            }

            switch ((DateType)model.DateType)
            {
                case DateType.DAY:
                    format = "yyyy-MM-dd";
                    break;
                case DateType.MONTH:
                    format = "MMMM yyyy";
                    actualEnd = end.AddMonths(1);
                    break;
                case DateType.YEAR:
                    format = "yyyy";
                    actualEnd = end.AddYears(1);
                    break;
            }

            var isCompare = model.Start2.HasValue && model.End2.HasValue;


            var groupedQuery = CountByIdQuery(start, actualEnd, model.AccountId, model.SiteId, model.SelectedTypes, type);

            idQuery = IdQuery(model.SelectedTypes, type, model.SiteId, model.AccountId);
            var typeInfos = dbContext.Database.SqlQuery<TypeInfo>(idQuery).ToList();

            var typeGrouped = new List<IncidentStatsByIdGrouped>();
            typeGrouped = dbContext.Database.SqlQuery<IncidentStatsByIdGrouped>(groupedQuery).ToList();

            var index = 0;

            var stats = new IncidentStatisticsModel
            {
                labels = new List<string>(),
                datasets = new List<Datasets>()
            };
            foreach (var row in typeInfos)
            {
                stats.labels.Add(row.Name);
                var values = new List<int>
                {
                    typeGrouped.FirstOrDefault(e => e.Id == row.Id)?.Count ?? 0
                };
                stats.datasets.Add(new Datasets { data = values, label = row.Name, backgroundColor = new Colors().Get(index) });
                index++;
            }
            stats.title = $"{string.Join(", ", typeInfos.Select(e => e.Name))} ({start.ToString(format)} to {end.ToString(format)})";
            return stats;
        }

        public string IdQuery(List<int> ids, string type, int siteId, int accountId) =>
            $@"SELECT DISTINCT t.Id, t.Name 
                FROM  {type} t
                WHERE t.AccountId = {accountId} 
                {(ids?.Any() ?? false ? $" and t.Id in ({string.Join(", ", ids)})" : (type != "Type2" ? "" : $" and t.siteId = {siteId}")) }";

        public string DateQuery(DateTime start, DateTime end) =>
            $@"i.time >= '{start:yyyy-MM-dd}' and i.time < '{end:yyyy-MM-dd}' ";

        public string CountByIdQuery(DateTime start, DateTime end, int accountId, int siteId, List<int> ids, string type) =>
            $@"SELECT       t.Id, Count(*) as 'Count' FROM INCIDENTS i
                JOIN        {type} t on t.id = i.{type}id
                WHERE       i.accountid={accountId} and (i.SiteId = {siteId} or {siteId} = 0) and {DateQuery(start, end)} {(ids?.Any() ?? false ? $"and t.Id in ({string.Join(", ", ids)})" : "")}
                GROUP BY    t.Id";

        public string CountByDateQuery(DateTime start, DateTime end, int accountId, int siteId, string format, string type, List<int> ids) =>
            $@"SELECT       Count(*) as 'Count', FORMAT(CAST(i.[Time] AS DATE), '{format}') as 'Date' FROM INCIDENTS i
                JOIN        {type} t on t.id = i.{type}id and t.Id in ({string.Join(",", ids)})
                WHERE       i.accountid={accountId} and (i.SiteId = {siteId} or {siteId} = 0) and {DateQuery(start, end)}
                GROUP BY    FORMAT(CAST(i.[Time] AS DATE), '{format}')";

        public string CountByGroupDateQuery(DateTime start, DateTime end, int accountId, int siteId, string format, string type) =>
            $@"SELECT FORMAT(CAST(i.[Time] AS DATE), '{format}') as 'Date', t.Id, COUNT(*) as 'Count' from INCIDENTS i
                JOIN {type} t on t.id = i.{type}id
                WHERE i.accountid={accountId} and (i.SiteId = {siteId} or {siteId} = 0) and {DateQuery(start, end)}
                GROUP BY FORMAT(CAST(i.[Time] AS DATE), '{format}'), t.Id";

        public List<dynamic> GetMinMaxStatsDate(int accountId, int siteId)
        {
            List<dynamic> values = new List<dynamic>();
            DateTime start = dbContext.Database.SqlQuery<DateTime?>($"SELECT TOP 1 Time FROM Incidents WHERE AccountId = {accountId} and (SiteId = {siteId} or {siteId} = 0) ORDER BY Time").First() ?? DateTime.Now;
            DateTime end = DateTime.Now;

            values.Add(new
            {
                DateMode = $"{start:yyyy-MM-dd}",
                MonthMode = $"{start:yyyy-MM}",
                YearMode = Convert.ToInt32($"{start:yyyy}")
            });
            values.Add(new
            {
                DateMode = $"{end:yyyy-MM-dd}",
                MonthMode = $"{end:yyyy-MM}",
                YearMode = Convert.ToInt32($"{end:yyyy}")
            });

            return values;
        }

        public void FixImages()
        {
            var query = $@"select ri.*, r.IncidentId from ReportImage ri 
                        join Reports r on r.Id = ri.ReportId
                        where image is not null";
            var images = dbContext.Database.SqlQuery<IncidentImages>(query).ToList();
            var groupByIncident = images.GroupBy(e => e.IncidentId);

            foreach (var group in groupByIncident)
            {

                var counter = 1;
                foreach (var image in group.ToList())
                {
                    var q = $@"UPDATE Incidents
                            set Image{counter}='{image.Image}'
                            where Id={group.Key}";
                    var res = dbContext.Database.ExecuteSqlCommand(q);
                    counter++;
                }
            }
        }

        public ExportStatisticsFields GetExportPreFields(int category, int siteId)
        {
            return _exportFieldsRepository.GetByPredicate(e => e.SiteId == siteId && e.Category == category).FirstOrDefault();
        }

        public void SaveExportPreFields(List<ExportStatisticsFields> data)
        {
            foreach (var row in data)
            {
                if (row.Id > 0)
                {
                    _exportFieldsRepository.Update(row);
                }
                else
                {
                    _exportFieldsRepository.Save(row);
                }
            }
        }

        public List<IncidentObjectModel> GetIncidentByRounds(int siteId, int accountId, int guardRoundId, DateTime start, DateTime end)
        {
            var tenantGuards = dbContext.ObjectGuardRound.Where(e => e.GuardRoundId == guardRoundId).Select(e => e.ObjectId).ToList();
            var incidents = GetAllForList(siteId, accountId, date: start, endDate: end, 0, 0, 0)
                .Where(e => e.TenantId.HasValue && tenantGuards.Contains(e.TenantId.Value)).ToList();

            var tenants = dbContext.Tenants.Where(e => tenantGuards.Contains(e.Id)).ToList();

            return tenants.Select(e => new IncidentObjectModel()
            {
                IsScanned = incidents.Any(x => x.TenantId.HasValue && x.TenantId.Value == e.Id),
                Name = e.Name,
                NumberOfScans = incidents.Count(x => x.TenantId.HasValue && x.TenantId.Value == e.Id),
                Guards = string.Join(", ", incidents.Where(x => x.TenantId.Value == e.Id).SelectMany(x => x.Guards).Select(y => y.Name).Distinct().ToList()),
            }).ToList();
        }

        public List<GuardModel> GetGuardAccounts(int accountId)
        {
            var users = dbContext.Users.Where(u => u.AccountId == accountId && u.IsGuard).ToList();

            return users.Select(u => new GuardModel()
            {
                Id = u.Id,
                Name = $"{u.FirstName} {u.LastName}"
            }).ToList();
        }

        private int DEFAULT_DESC_FIELD_ID => 35;
        private string DESC_FIELD_TYPE => "TEXTAREA";

        public void UpdateDescription(int id, string message)
        {
            var incident = dbContext.Incidents.FirstOrDefault(x => x.Id == id);
            if (incident == null)
            {
                throw new Exception("Incident not found.");
            }
            incident.ActionDescription = message;

            var descriptionField = dbContext.DailyReportFields.FirstOrDefault(x => x.DefaultLabel == "Description")?.Id ?? DEFAULT_DESC_FIELD_ID;
            var fields = dbContext.Type1FieldValues.FirstOrDefault(e => e.IncidentId == id && e.Type1_FieldId == descriptionField);
            if (fields != null)
            {
                fields.Value = message;
            }
            else
            {
                dbContext.Type1FieldValues.Add(new Type1FieldValue()
                {
                    Type1_FieldId = descriptionField,
                    IncidentId = id,
                    Value = message,
                    Type = DESC_FIELD_TYPE
                });
            }

            dbContext.SaveChanges();
        }

        public class IncidentImages : ReportImage
        {
            public int IncidentId { get; set; }
        }
    }
}
