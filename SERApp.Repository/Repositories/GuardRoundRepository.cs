﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SERApp.Repository.Repositories
{
    public class GuardRoundRepository : IGuardRoundRepository
    {
        private readonly string connString = AppSettings.ConnectionString;
        private readonly SERAppDBContext db;
        private IIncidentRepository incidentRepository;
        public GuardRoundRepository()
        {
            db = new SERAppDBContext(connString);
            incidentRepository = new IncidentRepository();
        }

        public void SaveNotes(GuardRoundLogs data)
        {
            var log = db.GuardRoundLogs.FirstOrDefault(e => e.Id == data.Id);
            if (log == null)
            {
                throw new Exception("Guard round record not found.");
            }
            log.Notes = data.Notes;
            db.SaveChanges();
        }

        public List<GuardRoundLogModel> GetActiveRoundsByDate(DateTime start, DateTime end, int siteId, int accountId)
        {
            var list = new List<GuardRoundLogModel>();
            var active = db.GuardRounds.Where(e => e.GuardRoundLogs.Any(x => x.StartDate >= start && x.StartDate < end) && (e.SiteId == siteId || siteId == 0)).ToList();
            
            if (!active.Any())
            {
                return list;
            }
            var logs = db.GuardRoundLogs.Where(x => x.StartDate >= start && x.StartDate < end && (x.GuardRound.SiteId == siteId || siteId == 0)).ToList();
            
            var allincidents = this.incidentRepository.GetAllForList(siteId, accountId, date: start, endDate: end, 0, 0, 0).Where(x => x.RoundLogId.HasValue).ToList();

            foreach (var e in active)
            {
                var objects = db.ObjectGuardRound.Include(x => x.Object).Where(x => x.GuardRoundId == e.Id && x.Object != null).Select(x => x.ObjectId).ToList();
                var currLogsIds = logs.Where(x => x.GuardRoundId == e.Id).Select(x => x.Id).ToList();
                var incidents = allincidents.Where(x => objects.Contains(x.TenantId.Value) && currLogsIds.Contains(x.RoundLogId.Value)).ToList();
                var incidentsIds = incidents.Select(x => x.Id).ToList();
                var incidentGuards = db.IncidentGuard.Where(x => incidentsIds.Contains(x.Id)).Select(x => x.GuardId).Distinct().ToList();
                var guards = string.Join(", ", db.Guards.Where(x => incidentGuards.Contains(x.Id)).Select(x => x.Name).ToList());

                var currLogs = logs.Where(x => x.GuardRoundId == e.Id).ToList().Select(x => new Logs()
                {
                    Id = x.Id,
                    EndTime = x.EndDate.HasValue ? x.EndDate.Value.ToString("yyyy-MM-dd HH:mm") : "",
                    StartTime = x.StartDate.ToString("yyyy-MM-dd HH:mm"),
                    Status = x.EndDate.HasValue ? "Completed" : "In progress",
                    Notes = x.Notes,
                    Scans = incidents.Where(xx => xx.RoundLogId.HasValue && xx.RoundLogId == x.Id).Select(y => new Scans()
                    {
                        Date = y.Date,
                        Vad = y.Type1Name,
                        Var = y.Type2Name,
                        Vem = y.Type3Name,
                        Guards = string.Join(", ", y.Guards.Select(yy => yy.Name).ToList())
                    })
                }); ;
                
                list.Add(new GuardRoundLogModel()
                {
                    Id = e.Id,
                    Name = e.Name,
                    NumberOfRounds = logs.Count(x => x.GuardRoundId == e.Id),
                    NumberOfScans = incidents.Count(),
                    NumberOfExpectedScans = objects.Count() * currLogs.Count(),
                    Guards = guards,
                    Logs = currLogs
                });
            }

            return list;
        }

        public List<GuardRoundLogs> GetRoundsByGuardId(int guardRoundId, int guardId, DateTime date)
        {
            return db.GuardRoundLogs.Where(l => l.GuardId == guardId && l.GuardRoundId == guardRoundId).ToList();
        }

        public bool IsPendingRound(int guardRoundId, int guardId)
        {
            return db.GuardRoundLogs.Any(e => e.GuardRoundId == guardRoundId && e.GuardId == guardId && !e.EndDate.HasValue);
        }

        public int StartRound(GuardRoundLogs data)
        {
            data.GuardId = GetGuardId(data.UserId, data.GuardRoundId);
            var isPending = IsPendingRound(data.GuardRoundId, data.GuardId);
            if (isPending)
            {
                throw new Exception("Previous round is still in progress.");
            }

            data.StartDate = DateTime.Now;
            db.GuardRoundLogs.Add(data);
            db.SaveChanges();

            return data.Id;
        }

        public int GetGuardId(int userId, int guardRoundId)
        {
            var user = db.Users.FirstOrDefault(x => x.Id == userId);
            var name = $"{user.FirstName} {user.LastName}";
            var round = db.GuardRounds.FirstOrDefault(x => x.Id == guardRoundId);
            var guard = db.Guards.FirstOrDefault(x => x.Name == name && x.SiteId == round.SiteId);

            if (guard == null)
            {
                db.Guards.Add(new Guard()
                {
                    Name = name,
                    CreatedDate = DateTime.Now,
                    SiteId = round.SiteId,
                });
                db.SaveChanges();
            }

            return guard.Id;
        }

        public void StopRound(GuardRoundLogs data)
        {
            data.GuardId = GetGuardId(data.GuardId, data.GuardRoundId);

            var latestRound = db.GuardRoundLogs
                .Where(e => e.GuardRoundId == data.GuardRoundId && e.GuardId == data.GuardId && !e.EndDate.HasValue)
                .OrderByDescending(e => e.StartDate)
                .FirstOrDefault();

            if (latestRound == null)
            {
                throw new Exception("Selected round has no in progress record.");
            }

            latestRound.EndDate = DateTime.Now;
            db.SaveChanges();
        }
    }
}
