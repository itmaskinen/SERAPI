﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SERApp.Repository.Repositories
{
    public class SectionRepository : SERApp.Repository.Repositories.Repository<Section>, IRepository<Section>, ISectionRepository
    {
        public IEnumerable<Section> GetSectionIncludeChildren()
        {
            var data = dbContext.Set<Section>()
               .Include(x => x.RyckrapportDataSections)
               //.Include(x=>x.RyckrapportDataSections.Select(r=>r.RyckrapportData))
               .Include(x => x.RyckrapportDataSections.Select(r => r.Section));
            // .Include(x => x.IncidentGuard);
            return data;
        }
    }
}
