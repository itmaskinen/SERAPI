﻿using SERApp.Data.Models;
using SERApp.Models.Common;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Repositories
{
    public class LocationRepository : ILocationRepository
    {
        private string connString = AppSettings.ConnectionString;
        protected SERAppDBContext dbContext;
        public LocationRepository()
        {
            dbContext = new SERAppDBContext(connString);
        }

        public void AddBuilding(Building building)
        {
            dbContext.Buildings.Add(building);
            dbContext.SaveChanges();
        }

        public void AddLevel(Level level)
        {
            dbContext.Levels.Add(level);
            dbContext.SaveChanges();
        }

        public void AddRoom(Room room)
        {
            dbContext.Rooms.Add(room);
            dbContext.SaveChanges();
        }

        public List<Building> GetBuildings(int accountId)
        {
            return dbContext.Buildings.Where(x => x.AccountId == accountId).ToList();
        }

        public List<Level> GetLevels(int accountId)
        {
            return dbContext.Levels.Where(x => x.AccountId == accountId).ToList();
        }

        public List<Room> GetRooms(int accountId)
        {
            return dbContext.Rooms.Where(x => x.AccountId == accountId).ToList();
        }

        public void DeleteBuilding(int id)
        {
            if (dbContext.Tenants.FirstOrDefault(x => x.BuildingId == id) != null)
            {
                throw new Exception("Unable to delete building because it has objects linked to it");
            }

            var building = dbContext.Buildings.FirstOrDefault(e => e.Id == id);
            dbContext.Buildings.Remove(building);
            dbContext.SaveChanges();
        }

        public void DeleteLevel(int id)
        {
            if (dbContext.Tenants.FirstOrDefault(x => x.LevelId == id) != null)
            {
                throw new Exception("Unable to delete level because it has objects linked to it");
            }

            var level = dbContext.Levels.FirstOrDefault(e => e.Id == id);
            dbContext.Levels.Remove(level);
            dbContext.SaveChanges();
        }

        public void DeleteRoom(int id)
        {
            if (dbContext.Tenants.FirstOrDefault(x => x.RoomId == id) != null)
            {
                throw new Exception("Unable to delete room because it has objects linked to it");
            }

            var room = dbContext.Rooms.FirstOrDefault(e => e.Id == id);
            dbContext.Rooms.Remove(room);
            dbContext.SaveChanges();
        }
    }
}
