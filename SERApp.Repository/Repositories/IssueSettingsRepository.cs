﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System.Linq;

namespace SERApp.Repository.Repositories
{
    public class IssueSettingsRepository : Repository<IssueSettings>, IRepository<IssueSettings>, IIssueSettingsRepository
    {

        public IssueSettings GetByAccountId(int accountId)
        {
            return dbContext.IssueSettings.SingleOrDefault(x => x.AccountId == accountId);
        }
    }
}
