﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
namespace SERApp.Repository.Repositories
{
    public class TagRepository : Repository<Tag>, IRepository<Tag>, ITagRepository
    {
        public IEnumerable<Tag> GetAllTagsIncludeChilds()
        {
            var data = dbContext.Set<Tag>()
               .Include(x => x.Tenant)
               .Include(x => x.Tenant.Site);

            return data;
        }

        public Tag GetTagIncludeChilds(int id)
        {
            var data = dbContext.Set<Tag>()
              .Include(x => x.Tenant)
              .SingleOrDefault(x => x.Id == id);

            return data;
        }
    }
}
