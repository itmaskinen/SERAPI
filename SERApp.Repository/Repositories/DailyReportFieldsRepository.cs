﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System.Collections.Generic;
using System.Linq;

namespace SERApp.Repository.Repositories
{
    public class DailyReportFieldsRepository : Repository<DailyReportFields>, IDailyReportFieldsRepository
    {
        public new List<DailyReportFields> GetAll()
        {
            return this.dbContext.DailyReportFields.Include("FieldType").ToList();
        }
    }
}
