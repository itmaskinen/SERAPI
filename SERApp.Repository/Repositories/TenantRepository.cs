﻿using SERApp.Data.Extensions;
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SERApp.Repository.Repositories
{
    public class TenantRepository
    {
        private string connString = AppSettings.ConnectionString;
        private IncidentRepository incidentRepository;
        private IGuardRoundRepository guardRoundRepository;
        private SERAppDBContext db;

        public TenantRepository()
        {
            db = new SERAppDBContext(connString);
            this.incidentRepository = new IncidentRepository();
            this.guardRoundRepository = new GuardRoundRepository();
        }

        public Tenant Get(int id)
        {
            return db.Tenants.Include(e => e.TenantTypeEnt)
                .Include(e => e.PremiseTypeEnt)
                .Include(e => e.SubPremiseTypeEnt)
                .Include(e => e.GuardRounds)
                .Include(e => e.Building)
                .Include(e => e.Level)
                .Include(e => e.Room)
                .Where(t => t.Id == id)
                .SingleOrDefault();
        }

        public PremiseType GetPremiseType(int id)
        {
            return db.PremiseTypes.Where(s => s.Id == id).SingleOrDefault();
        }

        public SubPremiseType GetSubPremiseType(int id)
        {
            return db.SubPremiseTypes.Where(s => s.Id == id).SingleOrDefault();
        }

        public JobTitle GetJobTitle(int id)
        {
            return db.JobTitles.Where(s => s.Id == id).SingleOrDefault();
        }

        public IQueryable<Tenant> GetAll() =>
            db.Tenants.Include(x => x.Site)
                .Include(x => x.TenantTypeEnt)
                .Include(x => x.SubPremiseTypeEnt)
                .Include(x => x.PremiseTypeEnt);

        public IQueryable<Tenant> GetAllByAccountId(int accountId) => GetAll().Where(x => x.Site.AccountId.Equals(accountId));

        public IQueryable<Tenant> GetAllBySiteId(int siteId) => GetAll().Where(t => t.SiteId == siteId);

        public List<ContactModel> GetAllTenantContactsByTenantId(int tenantId)
        {
            var contacts = (from tc in db.TenantContacts
                            join c in db.Contacts on tc.ContactId equals c.Id
                            where tc.TenantId == tenantId
                            select new { c, tc })
                            .Select(c => new ContactModel
                            {
                                Id = c.c.Id,
                                TenantContactId = c.tc.Id,
                                FullName = c.c.FullName,
                                EmailAddress = c.c.EmailAddress,
                                MobileNumber = c.c.MobileNumber,
                                PhoneNumber = c.c.PhoneNumber,
                                JobTypeId = c.tc.JobTypeId,
                                JobTitle = c.c.JobTitle,
                                IsActive = c.c.IsActive,
                            })

                            .ToList();
            return contacts;
        }

        public TenantContact GetTenantContact(int tenantId, int contactId)
        {
            return db.TenantContacts.Where(tc => tc.TenantId == tenantId && tc.ContactId == contactId).FirstOrDefault();
        }

        public void SaveTenantContacts(TenantContactModel tenantContactModel)
        {
            //CONTACT
            int contactId = 0;
            ContactModel contactModel = tenantContactModel;
            var contactData = db.Contacts.Where(c => c.Id == tenantContactModel.Id).SingleOrDefault();
            if (contactData != null)
            {
                contactData.FullName = contactModel.FullName;
                contactData.EmailAddress = contactModel.EmailAddress;
                contactData.PhoneNumber = contactModel.PhoneNumber;
                contactData.MobileNumber = contactModel.MobileNumber;
                contactData.LastUpdatedDate = DateTime.Now;
                contactData.JobTitle = contactModel.JobTitle;
                contactData.IsActive = contactModel.IsActive;
                db.SaveChanges();
            }
            else
            {
                var newContact = new Contact()
                {
                    FullName = contactModel.FullName,
                    EmailAddress = contactModel.EmailAddress,
                    PhoneNumber = contactModel.PhoneNumber,
                    MobileNumber = contactModel.MobileNumber,
                    CreatedDate = DateTime.Now,
                    LastUpdatedDate = DateTime.Now,
                    JobTitle = contactModel.JobTitle,
                    IsActive = contactModel.IsActive
                };
                db.Contacts.Add(newContact);
                db.SaveChanges();
                contactId = newContact.Id;
            }


            //TENANT CONTACT
            var tenantContact = db.TenantContacts.Where(tc => tc.Id == tenantContactModel.TenantContactId).SingleOrDefault();
            if (tenantContact != null)
            {
                tenantContact.JobTypeId = tenantContactModel.JobTypeId;
            }
            else
            {
                var newTenantContact = new TenantContact()
                {
                    ContactId = contactId,
                    TenantId = tenantContactModel.TenantId,
                    JobTypeId = tenantContactModel.JobTypeId
                };
                db.TenantContacts.Add(newTenantContact);
            }
            db.SaveChanges();
        }

        public int SaveTenant(TenantModel model, bool isPublic = false)
        {
            int IdToReturn = 0;
            var data = db.Tenants.Where(t => t.Id == model.Id).SingleOrDefault();
            if (data != null)
            {
                data.Name = model.Name;
                data.Email = model.Email;
                data.Phone = model.Phone;
                data.PermiseType = model.PermiseType;
                data.VisitingAddress = model.VisitingAddress;
                data.UserName = model.UserName;
                data.Password = model.Password;
                data.InsuranceCompany = model.InsuranceCompany;
                data.SecurityCompany = model.SecurityCompany;
                data.SiteManagerName = model.SiteManagerName;
                data.SiteManagerMobilePhone = model.SiteManagerPhone;
                data.SiteManagerPhone = model.SiteManagerPhone;
                data.ContractName = model.ContractName;
                data.ContractVatNo = model.ContractVatNo;
                data.InvoiceAddress = model.InvoiceAddress;
                data.InvoiceZipAndCity = model.InvoiceZipAndCity;
                data.ContactComments = model.ContactComments;
                data.CCTV = model.CCTV;
                data.IntrusionAlarm = model.IntrusionAlarm;

                if (!isPublic)
                {
                    data.LastUpdatedDate = DateTime.Now;
                }

                data.EmailTemplate = model.EmailTemplate;
                data.Image = model.Image;
                data.SiteManagerEmail = model.SiteManagerEmail;
                data.Flag = model.Flag;
                data.PropertyDesignation = model.PropertyDesignation;
                data.UseWrittenStatement = model.UseWrittenStatement;
                data.SiteId = model.SiteId;
                data.TenantType = model.TenantType;
                data.SubPremiseType = model.SubPremiseType;
                data.City = model.City;
                data.ZipCode = model.ZipCode;
                data.IsActive = model.IsActive;
                data.PostZipCode = model.PostZipCode;
                data.PostAddress = model.PostAddress;
                data.PostCity = model.PostCity;
                data.LastUpdatedBy = model.LastUpdatedBy;
                data.EnableNFC = model.EnableNFC;
                data.IsNFCReady = model.IsNFCReady;
                if (isPublic)
                {
                    data.LastUpdatedByCustomer = DateTime.Now;
                }

                IdToReturn = data.Id;
                db.SaveChanges();
            }
            else
            {
                Tenant newTenant = new Tenant()
                {
                    Name = model.Name,
                    Email = model.Email,
                    EmailTemplate = model.EmailTemplate,
                    Image = model.Image,
                    Phone = model.Phone,
                    PermiseType = model.PermiseType,
                    VisitingAddress = model.VisitingAddress,
                    UserName = model.UserName,
                    Password = model.Password,
                    InsuranceCompany = model.InsuranceCompany,
                    SecurityCompany = model.SecurityCompany,
                    SiteManagerName = model.SiteManagerName,
                    SiteManagerMobilePhone = model.SiteManagerPhone,
                    SiteManagerPhone = model.SiteManagerPhone,
                    ContractName = model.ContractName,
                    ContractVatNo = model.ContractVatNo,
                    InvoiceAddress = model.InsuranceCompany,
                    InvoiceZipAndCity = model.InvoiceZipAndCity,
                    ContactComments = model.ContactComments,
                    CCTV = model.CCTV,
                    IntrusionAlarm = model.IntrusionAlarm,
                    LastUpdatedDate = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    SiteManagerEmail = model.SiteManagerEmail,
                    Flag = false,
                    PropertyDesignation = model.PropertyDesignation,
                    UseWrittenStatement = model.UseWrittenStatement,
                    SiteId = model.SiteId,
                    IsActive = model.IsActive,
                    TenantType = model.TenantType,
                    SubPremiseType = model.SubPremiseType,
                    City = model.City,
                    ZipCode = model.ZipCode,
                    PostCity = model.PostCity,
                    PostAddress = model.PostAddress,
                    PostZipCode = model.PostZipCode,
                    LastUpdatedBy = model.LastUpdatedBy,
                    EnableNFC = model.EnableNFC,
                };
                db.Tenants.Add(newTenant);
                db.SaveChanges();
                IdToReturn = newTenant.Id;
            }

            SaveObjectGuardRoundFromSaveTenant(IdToReturn, model.GuardRounds);
            SaveObjectLocations(model.Building, model.Level, model.Room, model.AccountId, IdToReturn, model.SiteId);

            return IdToReturn;
        }

        public void DeleteTenant(int id)
        {
            var data = db.Tenants.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.Tenants.Remove(data);

                var objectrounds = db.ObjectGuardRound.Where(e => e.ObjectId == id).ToList();
                if (objectrounds.Any())
                {
                    db.ObjectGuardRound.RemoveRange(objectrounds);
                }
                db.SaveChanges();
            }
        }

        public void SaveTenantType(TenantTypeModel model)
        {
            var data = db.TenantTypes.Where(t => t.Id == model.Id).SingleOrDefault();
            if (data != null)
            {
                data.TypeName = model.TypeName;
                data.Description = model.Description;
                data.PageSetting = model.PageSetting;
                data.IsPopupMessage = model.IsPopupMessage;
                data.DefaultGuardRoundId = model.DefaultGuardRoundId;
            }
            else
            {
                var newTenantType = new TenantType()
                {
                    TypeName = model.TypeName,
                    Description = model.Description,
                    AccountId = model.AccountId,
                    PageSetting = model.PageSetting,
                    IsPopupMessage = model.IsPopupMessage,
                    DefaultGuardRoundId = model.DefaultGuardRoundId
                };

                db.TenantTypes.Add(newTenantType);
            }
            db.SaveChanges();
        }

        public void SavePremiseType(PremiseTypeModel model)
        {
            var data = db.PremiseTypes.Where(t => t.Id == model.Id).SingleOrDefault();
            if (data != null)
            {
                data.TypeName = model.TypeName;
                data.Description = model.Description;
            }
            else
            {
                var newPremiseType = new PremiseType()
                {
                    TenantTypeId = model.TenantTypeId,
                    TypeName = model.TypeName,
                    Description = model.Description,
                    AccountId = model.AccountId
                };

                db.PremiseTypes.Add(newPremiseType);
            }
            db.SaveChanges();
        }

        public void SaveSubPremiseType(SubPremiseTypeModel model)
        {
            var data = db.SubPremiseTypes.Where(t => t.Id == model.Id).SingleOrDefault();
            if (data != null)
            {
                data.PremiseTypeId = model.PremiseTypeId;
                data.TypeName = model.TypeName;
                data.Description = model.Description;
            }
            else
            {
                var newSubPremiseType = new SubPremiseType()
                {
                    TypeName = model.TypeName,
                    PremiseTypeId = model.PremiseTypeId,
                    Description = model.Description,
                    AccountId = model.AccountId
                };

                db.SubPremiseTypes.Add(newSubPremiseType);
            }
            db.SaveChanges();
        }

        public void SaveJobTite(JobTitleModel model)
        {
            var data = db.JobTitles.Where(t => t.Id == model.Id).SingleOrDefault();
            if (data != null)
            {
                data.TitleName = model.TitleName;
                data.Description = model.Description;
                data.IncludedOnExcelExport = model.IncludedOnExcelExport;
            }
            else
            {
                var newJobTitle = new JobTitle()
                {
                    TitleName = model.TitleName,
                    Description = model.Description,
                    AccountId = model.AccountId,
                    IncludedOnExcelExport = model.IncludedOnExcelExport
                };

                db.JobTitles.Add(newJobTitle);
            }
            db.SaveChanges();
        }

        public void DeleteTenantType(int id)
        {
            var data = db.TenantTypes.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.TenantTypes.Remove(data);
                db.SaveChanges();
            }
        }

        public void DeletePremiseType(int id)
        {
            var data = db.PremiseTypes.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.PremiseTypes.Remove(data);
                db.SaveChanges();
            }
        }

        public void DeleteSubPremiseType(int id)
        {
            var data = db.SubPremiseTypes.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.SubPremiseTypes.Remove(data);
                db.SaveChanges();
            }
        }

        public void DeleteJobTitle(int id)
        {
            var data = db.JobTitles.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.JobTitles.Remove(data);
                db.SaveChanges();
            }
        }

        public void DeleteTenantContact(int id)
        {
            var data = db.TenantContacts.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.TenantContacts.Remove(data);
                db.SaveChanges();
            }
        }

        public void DeleteGuardRound(int id)
        {
            var data = db.GuardRounds.Where(e => e.Id == id).FirstOrDefault();

            if (data != null)
            {
                var objects = db.ObjectGuardRound.Where(e => e.GuardRoundId == id).ToList();

                db.ObjectGuardRound.RemoveRange(objects);
                db.GuardRounds.Remove(data);
                db.SaveChanges();
            }
        }

        public void SaveObjectGuardRoundFromSaveTenant(int objectId, List<int> guardRoundIds)
        {
            var data = db.ObjectGuardRound.Where(e => e.ObjectId == objectId).ToList();

            var toBeAdded = guardRoundIds.Where(e => !data.Any(x => x.GuardRoundId == e)).Select(e => new ObjectGuardRound() { GuardRoundId = e, ObjectId = objectId });
            var toBeRemoved = data.Where(e => !guardRoundIds.Contains(e.GuardRoundId));

            db.ObjectGuardRound.AddRange(toBeAdded);
            db.ObjectGuardRound.RemoveRange(toBeRemoved);

            db.SaveChanges();
        }

        public void SaveObjectLocations(string building, string level, string room, int accountId, int id, int siteId)
        {
            var buildingExisting = new Building();
            if (!string.IsNullOrEmpty(building))
            {
                buildingExisting = db.Buildings.FirstOrDefault(e => e.AccountId == accountId && e.Name == building);
                if (buildingExisting is null)
                {
                    buildingExisting = new Building()
                    {
                        Name = building,
                        AccountId = accountId,
                        SiteId = siteId
                    };

                    db.Buildings.Add(buildingExisting);
                    db.SaveChanges();
                }
            }
            var levelExisting = new Level();
            if (!string.IsNullOrEmpty(level))
            {
                levelExisting = db.Levels.FirstOrDefault(e => e.AccountId == accountId && e.Name == level);
                if (levelExisting is null)
                {
                    levelExisting = new Level()
                    {
                        Name = level,
                        BuildingId = buildingExisting.Id,
                        AccountId = accountId,
                        SiteId = siteId
                    };
                    db.Levels.Add(levelExisting);
                    db.SaveChanges();
                }
            }

            var roomExisting = new Room();
            if (!string.IsNullOrEmpty(room))
            {
                roomExisting = db.Rooms.FirstOrDefault(e => e.AccountId == accountId && e.Name == room);
                if (roomExisting is null)
                {
                    roomExisting = new Room()
                    {
                        Name = room,
                        LevelId = levelExisting.Id,
                        BuildingId = buildingExisting.Id,
                        AccountId = accountId,
                        SiteId = siteId
                    };

                    db.Rooms.Add(roomExisting);
                    db.SaveChanges();
                }
            }

            var tenant = db.Tenants.FirstOrDefault(e => e.Id == id);
            tenant.BuildingId = buildingExisting.Id;
            tenant.LevelId = levelExisting.Id;
            tenant.RoomId = roomExisting.Id;

            db.SaveChanges();
        }

        public void SaveObjectGuardRound(List<int> tenantIds, int guardRoundId)
        {
            var data = db.ObjectGuardRound.Where(e => e.GuardRoundId == guardRoundId).ToList();

            var toBeAdded = tenantIds.Where(e => !data.Any(x => x.ObjectId == e)).Select(e => new ObjectGuardRound() { GuardRoundId = guardRoundId, ObjectId = e });
            var toBeRemoved = data.Where(e => !tenantIds.Contains(e.ObjectId));

            db.ObjectGuardRound.AddRange(toBeAdded);
            db.ObjectGuardRound.RemoveRange(toBeRemoved);

            db.SaveChanges();
        }

        private int GetGuardId(int userId, int siteId)
        {
            var user = db.Users.FirstOrDefault(x => x.Id == userId);
            var name = $"{user.FirstName} {user.LastName}";
            var guard = db.Guards.FirstOrDefault(x => x.Name == name && x.SiteId == siteId);

            if (guard == null)
            {
                db.Guards.Add(new Guard()
                {
                    Name = name,
                    CreatedDate = DateTime.Now,
                    SiteId = siteId,
                });
                db.SaveChanges();
            }

            return guard.Id;
        }

        public List<GuardRoundsModel> GetRoundLogs(int siteId, DateTime? date = null, int? guardId = null)
        {
            if (guardId.HasValue)
            {
                guardId = guardRoundRepository.GetGuardId(guardId.Value, siteId);
            }

            var requestDate = date ?? DateTime.Now;
            var site = db.Sites.FirstOrDefault(e => e.Id == siteId);

            var incidents = this.incidentRepository.GetAllForList(siteId, site.AccountId, date: requestDate, endDate: requestDate, 0, 0, 0).ToList();

            var logsEnddate = date.HasValue ? date.Value.AddDays(1): DateTime.MinValue;
            var logs = db.GuardRoundLogs.Include(x => x.GuardRound).Where(e => (!date.HasValue || (e.StartDate >= date && e.StartDate < logsEnddate)) && (!guardId.HasValue || e.GuardId == guardId.Value) && e.EndDate.HasValue).OrderByDescending(e => e.StartDate).ToList();

            var roundIds = logs.Select(e => e.GuardRoundId).ToList();
            var tenantRounds = db.ObjectGuardRound.Where(e => roundIds.Contains(e.GuardRoundId)).ToList();
            var tenantIds = tenantRounds.Select(e => e.ObjectId).ToList();
            var tenants = db.Tenants.Where(e => tenantIds.Contains(e.Id)).ToList();


            var result = new List<GuardRoundsModel>();
            foreach (var log in logs)
            {
                var currentTenantRounds = tenantRounds.Where(e => e.GuardRoundId == log.GuardRoundId).Select(e => e.ObjectId).ToList();
                var currentTenants = tenants.Where(e => currentTenantRounds.Contains(e.Id)).ToList();
                var currentTenantIds = currentTenants.Select(e => e.Id).ToList();
                var objects = new List<ObjectModel>();
                var roundIncidents = incidents.Where(e => currentTenantIds.Contains(e.TenantId.Value)).ToList();
                foreach (var tenant in currentTenants)
                {
                    var incidentTenants = roundIncidents?.Where(x => x.TenantId.Value == tenant.Id && (!x.RoundLogId.HasValue || x.RoundLogId.Value == log.Id));

                    objects.Add(new ObjectModel()
                    {
                        Id = tenant.Id,
                        Name = tenant.Name,
                        IsScanned = incidentTenants.Any(),
                        Date = incidentTenants.Any() ? incidentTenants.OrderByDescending(x => x.Date).Select(x => x.Time).Max().ToString("yyyy-MM-dd HH:mm") : null,
                    });
                }
                result.Add(new GuardRoundsModel()
                {
                    Id = log.Id,
                    Name = log.GuardRound.Name,
                    Start = log.StartDate,
                    End = log.EndDate,
                    SiteId = siteId,
                    Objects = objects,
                });
            }

            return result;
        }


        public List<GuardRoundsModel> GetGuardRounds(int siteId, bool excludeObjects = false, DateTime? date = null, string type = "template", int? guardId = null)
        {
            var requestDate = date ?? DateTime.Now;
            var endDate = requestDate;
            var rounds = db.GuardRounds.Where(e => e.SiteId == siteId).Include(e => e.GuardRoundLogs).ToList();
            switch (type)
            {
                case "active":
                    rounds = rounds.Where(e => e.SiteId == siteId && 
                        e.GuardRoundLogs.Any(x => !x.EndDate.HasValue)).ToList();

                    if (rounds.Any())
                    {
                        requestDate = rounds.SelectMany(e => e.GuardRoundLogs.Select(x => x.StartDate)).Min();
                        endDate = DateTime.Now;
                    }
                    break;
            }

            if (!excludeObjects && rounds.Any())
            {
                var result = new List<GuardRoundsModel>();
                var roundIds = rounds.Select(e => e.Id).ToList();
                var tenantRounds = db.ObjectGuardRound.Where(e => roundIds.Contains(e.GuardRoundId)).ToList();
                var tenantIds = tenantRounds.Select(e => e.ObjectId).ToList();
                var tenants = db.Tenants.Where(e => tenantIds.Contains(e.Id)).ToList();

                var site = db.Sites.FirstOrDefault(e => e.Id == siteId);

                endDate = endDate.AddDays(1);

                var logIds = rounds.SelectMany(e => e.GuardRoundLogs.Select(x => x.Id)).ToList();
                var incidents = db.Incidents
                    .Include(e => e.IncidentGuard)
                    .Where(e => logIds.Contains(e.RoundLogId ?? 0) && ((date.HasValue && e.Time >= requestDate && e.Time < endDate) || !date.HasValue))
                    .ToList();

                foreach (var item in rounds)
                {
                    var currentTenantRounds = tenantRounds.Where(e => e.GuardRoundId == item.Id).Select(e => e.ObjectId).ToList();
                    var currentTenants = tenants.Where(e => currentTenantRounds.Contains(e.Id)).ToList();
                    var currentTenantIds = currentTenants.Select(e => e.Id).ToList();

                    var roundIncidents = incidents.Where(e => currentTenantIds.Contains(e.TenantId.Value)).ToList();

                    if (guardId.HasValue)
                    {
                        var actualGuardId = guardRoundRepository.GetGuardId(guardId.Value, item.Id);
                        roundIncidents = incidents.Where(e => e.IncidentGuard.Any(x => x.Id == actualGuardId)).ToList();
                    }

                    var inProgressRound = item.GuardRoundLogs.FirstOrDefault(e => !e.EndDate.HasValue);

                    var objects = new List<ObjectModel>();
                    foreach(var tenant in currentTenants)
                    {
                        var incidentTenants = roundIncidents?.Where(x => x.TenantId.Value == tenant.Id);
                        if (type == "active")
                        {
                            incidentTenants = incidentTenants.Where(x => x.RoundLogId.HasValue && x.RoundLogId.Value == inProgressRound.Id);
                        }

                        objects.Add(new ObjectModel()
                        {
                            Id = tenant.Id,
                            Name = tenant.Name,
                            NFCTag = tenant.Id.NFCTag(),
                            IsNFCReady = tenant.IsNFCReady,
                            IsNFCEnabled = tenant.EnableNFC,
                            IsManual = incidentTenants.Any(x => !(x.FromWeb ?? false) && !(x.FromScan ?? false)),
                            IsScanned = type != "template" && incidentTenants.Any(),
                            Date = type == "template" ? null:
                                    incidentTenants.Any() ? incidentTenants.OrderByDescending(x => x.Time).Select(x => x.Time).Max().ToString("yyyy-MM-dd HH:mm") : null,
                        });
                    }

                    var recentRound = type == "template" ? null: item.GuardRoundLogs.OrderByDescending(e => e.StartDate).FirstOrDefault();

                    result.Add(new GuardRoundsModel()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Description = item.Description,
                        Start = recentRound?.StartDate,
                        End = recentRound?.EndDate,
                        SiteId = siteId,
                        Objects = objects,
                        ActiveRoundLogId = inProgressRound?.Id,
                        StartMessage = item.StartMessage,
                        StopMessage = item.StopMessage
                    });
                }
                return result;
            }

            return rounds.Select(item => new GuardRoundsModel()
            {
                Id = item.Id,
                Name = item.Name,
            }).ToList();
        }

        public void SetNFCReady(int id)
        {
            var data = db.Tenants.FirstOrDefault(e => e.Id == id);
            data.IsNFCReady = true;

            db.SaveChanges();
        }

        public GuardRoundsModel GetGuardRound(int id)
        {
            var data = db.GuardRounds.FirstOrDefault(e => e.Id == id);
            var tenantIds = db.ObjectGuardRound.Where(e => e.GuardRoundId == id).Select(x => x.ObjectId).ToList();
            return new GuardRoundsModel()
            {
                Id = id,
                Name = data.Name,
                Description = data.Description,
                SiteId = data.SiteId,
                ObjectIds = tenantIds,
                StartMessage = data.StartMessage,
                StopMessage = data.StopMessage
            };
        }

        public void SaveGuardRounds(GuardRoundsModel guardRounds)
        {
            var data = db.GuardRounds.FirstOrDefault(e => e.Id == guardRounds.Id);
            if (data != null)
            {
                data.Name = guardRounds.Name;
                data.Description = guardRounds.Description;
                data.UpdatedDate = DateTime.Now;
                data.StartMessage = guardRounds.StartMessage;
                data.StopMessage = guardRounds.StopMessage;
            }
            else
            {
                data = new GuardRounds
                {
                    Description = guardRounds.Description,
                    Name = guardRounds.Name,
                    SiteId = guardRounds.SiteId,
                    CreatedDate = DateTime.Now,
                    StartMessage = guardRounds.StartMessage,
                    StopMessage = guardRounds.StopMessage,
                };
                db.GuardRounds.Add(data);
            }

            db.SaveChanges();

            SaveObjectGuardRound(guardRounds.ObjectIds, data.Id);
        }

        public List<TenantModel> GetTenantsByIds(List<int> ids)
        {
            var concat = string.Join(",", ids);
            var lists = db.Database.SqlQuery<TenantModel>($"SELECT Id, Name, SiteId FROM Tenants WHERE Id in ({concat})").ToList();
            return lists;
        }
    }
}
