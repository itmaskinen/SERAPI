﻿namespace SERApp.Models
{
    public class TenantEmailSettingModel
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Foot { get; set; }
    }
}
