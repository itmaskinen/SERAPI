﻿using System;
using System.Collections.Generic;

namespace SERApp.Models
{
    public class AccountModuleModel
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int ModuleId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public AccountModel Account { get; set; }
        public List<ModuleModel> Modules { get; set; }
    }
}