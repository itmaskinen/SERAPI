﻿namespace SERApp.Models
{
    public class Issue_IssueType3Model
    {
        public int Id { get; set; }
        public int IssueType3Id { get; set; }
        public bool Value { get; set; }
        public int IssueWebId { get; set; }
    }
}
