﻿using System;

namespace SERApp.Models
{
    public class Type2Model
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public int AccountId { get; set; }
        public int SiteId { get; set; }
        public bool IsActive { get; set; }
    }
}
