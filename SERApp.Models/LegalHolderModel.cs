﻿namespace SERApp.Models
{
    public class LegalHolderModel
    {
        public int Id { get; set; }
        public string LegalHolder { get; set; }
        public int AccountId { get; set; }
        public bool IsActive { get; set; }
        public AccountModel Account { get; set; }
    }
}
