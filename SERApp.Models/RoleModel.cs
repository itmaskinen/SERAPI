﻿namespace SERApp.Models
{
    public class RoleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsModule { get; set; }
        public bool IsUser { get; set; }
    }
}
