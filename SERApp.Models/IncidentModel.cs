﻿using SERApp.Models.Common;
using System;
using System.Collections.Generic;

namespace SERApp.Models
{
    public class IncidentModel
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public TimeModel TimeHour { get; set; }
        public DateModel TimeDate { get; set; }
        public int Quantity { get; set; }
        public int PoliceResponse { get; set; }

        public string ActionDescription { get; set; }

        public int? Type1Id { get; set; }
        public Type1Model Type1 { get; set; }

        public int? Type2Id { get; set; }
        public Type2Model Type2 { get; set; }

        public int? Type3Id { get; set; }
        public Type3Model Type3 { get; set; }

        public int? TenantId { get; set; }
        public TenantModel Tenant { get; set; }

        public List<IncidentGuardModel> IncidentGuard { get; set; }
        public List<ReportModel> Reports { get; set; }
        public List<IncidentReportFieldValue> FieldValues { get; set; }
        public int AccountId { get; set; }
        public int SiteId { get; set; }

        public bool IsDeleted { get; set; }

        public bool ShouldSend { get; set; }

        public bool WithBlueLight { get; set; }
        public string BlueLightOption { get; set; }
        public int TimeZoneOffset { get; set; }
        public bool FromScan { get; set; }
        public bool? FromWeb { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string UserName { get; set; }
        public int? RoundLogId { get; set; }
        public bool IsNFCScanned { get; set; }
        public List<DateModel> TimeDates { get; set; }

        public IncidentModel()
        {
            this.FieldValues = new List<IncidentReportFieldValue>();
        }
    }

    public class IncidentModelV2 : IncidentModel
    {
        public List<string> Images { get; set; }
    }

    public class IncidentModelV1 : IncidentModel
    {
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }
        public string Image5 { get; set; }

    }

    public class SaveIncidentDescription
    {
        public string Description { get; set; }
        public int Id { get; set; }
    }

    public class SaveIncidentModel : IncidentModel
    {
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }
        public string Image5 { get; set; }
        public List<string> DeletedImages { get; set; }
    }

    public class PublicIncidentModel
    {
        public int Id { get; set; }
        public string Site { get; set; }
        public DateTime Date { get; set; }
        public string Vad { get; set; }
        public string Var { get; set; }
        public string Vem { get; set; }
        public string Description { get; set; }
    }

    public class IncidentObjectModel
    {
        public string Name { get; set; }
        public bool IsScanned { get; set; }
        public int NumberOfScans { get; set; }
        public string Guards { get; set; }
    }
}
