﻿namespace SERApp.Models
{
    public class ReportTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
