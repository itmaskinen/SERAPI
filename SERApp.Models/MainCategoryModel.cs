﻿namespace SERApp.Models
{
    public class MainCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AccountId { get; set; }
    }
}
