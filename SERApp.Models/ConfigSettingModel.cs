﻿namespace SERApp.Models
{
    public class ConfigSettingModel
    {
        public int Id { get; set; }
        public string SettingName { get; set; }
        public string SettingLabel { get; set; }
        public string DefaultValue { get; set; }
        public string InputType { get; set; }
        public int ModuleId { get; set; }
    }
}
