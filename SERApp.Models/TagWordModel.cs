﻿namespace SERApp.Models
{
    public class TagWordModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AccountId { get; set; }

    }
}
