﻿namespace SERApp.Models.Enums
{
    public class IncidentStatisticsEnum
    {
        public enum DateType
        {
            DAY,
            MONTH,
            YEAR
        }

        public enum DataType
        {
            Vad,
            Var,
            Vem
        }
    }
}
