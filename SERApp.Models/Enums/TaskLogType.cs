﻿namespace SERApp.Models.Enums
{
    public enum TaskLogType
    {
        Confirm = 1,
        ActionLog = 2,
        ActionExtension = 3,
        OldSystem = 4,
    }
}
