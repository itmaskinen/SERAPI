﻿namespace SERApp.Models
{
    public class AuthRequestModel
    {
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string ResendToEmailAddress { get; set; }
        public string Password { get; set; }
    }

    public class AuthResponseModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AuthResponseMessage { get; set; }
        public bool IsAuthenticated { get; set; }

        public string Token { get; set; }
        public UserModel CurrentUser { get; set; }
    }
}
