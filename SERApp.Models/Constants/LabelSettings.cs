﻿namespace SERApp.Models.Constants
{
    public class LabelSettings
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int ModuleId { get; set; }

    }
}
