﻿namespace SERApp.Models.Constants
{
    public class ConfigSettings
    {
        public const string SERAppEntitiesKey = "SERAppDBContext";
        public const string SERAppWebConfigFileSource = @"\SEPAppBlack\Web.config";
        public const string SERAppAppConfigFileSource = @"\SEPAppBlack\App.Settings.config";
    }
}
