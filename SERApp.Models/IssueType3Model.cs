﻿namespace SERApp.Models
{
    public class IssueType3Model
    {
        public int Id { get; set; }
        public string Name { get; set; }
        // public bool Value { get; set; }
        public int AccountId { get; set; }
    }
}
