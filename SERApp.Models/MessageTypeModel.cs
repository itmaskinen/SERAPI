﻿namespace SERApp.Models
{
    public class MessageTypeModel
    {
        public int Id { get; set; }
        public string ConfirmationName { get; set; }
        public string Description { get; set; }
        public bool IsUsed { get; set; }
    }
}
