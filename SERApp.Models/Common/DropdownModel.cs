﻿namespace SERApp.Models.Common
{
    public class DropdownModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Extra { get; set; }
    }
}
