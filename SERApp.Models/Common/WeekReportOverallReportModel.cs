﻿using System.Collections.Generic;

namespace SERApp.Models.Common
{
    public class WeekReportOverallReportModel
    {
        public IEnumerable<WeekReportDisplayModel> WeekSummaryDisplay { get; set; }
        public IEnumerable<WeekTypeReportModel> Type1Report { get; set; }
        public IEnumerable<WeekTypeReportModel> Type2Report { get; set; }
        public IEnumerable<WeekTypeReportModel> Type3Report { get; set; }
        public IEnumerable<MonthReportModel> Model { get; set; }
    }
}
