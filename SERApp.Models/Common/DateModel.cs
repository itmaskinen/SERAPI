﻿namespace SERApp.Models.Common
{
    public class DateModel
    {
        public int year { get; set; }
        public int month { get; set; }
        public int day { get; set; }
    }
}
