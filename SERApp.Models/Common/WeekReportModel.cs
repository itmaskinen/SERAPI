﻿namespace SERApp.Models.Common
{
    public class WeekReportModel
    {
        public int AccountId { get; set; }
        public int SiteId { get; set; }
        public DateModel From { get; set; }
        public DateModel To { get; set; }
    }
}
