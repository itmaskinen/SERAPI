﻿namespace SERApp.Models.Common
{
    public class WeekReportDisplayModel
    {
        public string Date { get; set; }
        public string Summary { get; set; }
        public string PoliceResponse { get; set; }
        public string Quantity { get; set; }
    }
}
