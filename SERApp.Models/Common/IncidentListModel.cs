﻿using System;
using System.Collections.Generic;

namespace SERApp.Models.Common
{
    public class IncidentListModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public int? Type1Id { get; set; }
        public string Type1Name { get; set; }
        public int? Type2Id { get; set; }
        public string Type2Name { get; set; }
        public int? Type3Id { get; set; }
        public string Type3Name { get; set; }

        public int Q { get; set; }
        public int P { get; set; }
        public string Description { get; set; }
        public int AccountId { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public int? TenantId { get; set; }
        public string TenantName { get; set; }

        public List<GuardIncidentModel> Guards { get; set; }

        public string ClosedFrom { get; set; }
        public string ClosedTo { get; set; }
        public bool HasImageAttached { get; set; }
        public bool HasAdditionalText { get; set; }
        public int? RoundLogId { get; set; }

        public IncidentListModel()
        {
            Guards = new List<GuardIncidentModel>();
        }

    }

    public class IncidentTempList : IncidentListModel
    {
        public string FieldValues { get; set; }
        public string FieldType { get; set; }
        public string FieldLabel { get; set; }
        public string Guard { get; set; }
    }

    public class GuardIncidentModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public int IncidentId { get; set; }

        public int SiteId { get; set; }

    }
}
