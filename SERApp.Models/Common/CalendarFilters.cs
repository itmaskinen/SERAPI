﻿using System;

namespace SERApp.Models.Common
{
    public class CalendarFilters
    {
        public int SiteId { get; set; }
        public int AccountId { get; set; }
        public int UserId { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
    }
}
