﻿namespace SERApp.Models.Common
{
    public class TimeModel
    {
        public int hour { get; set; }
        public int minute { get; set; }
        public int second { get; set; }
    }
}
