﻿namespace SERApp.Models.Common
{
    public class SummaryFilterModel
    {
        public DateModel date { get; set; }
        public int accountId { get; set; }
        public int siteId { get; set; }
    }
}
