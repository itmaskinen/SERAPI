﻿namespace SERApp.Models.Common
{
    public class FileResultModel
    {
        public byte[] Data { get; set; }
    }
}
