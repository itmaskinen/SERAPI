﻿namespace SERApp.Models.Common
{
    public class FileModel
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string ByteString { get; set; }
        public string DirectoryFullPath { get; set; }
        public double FileSize { get; set; }
        public string ModuleType { get; set; }
        public int AccountId { get; set; }
        public string Extension { get; set; }
        public byte[] Bytes { get; set; }
    }
}
