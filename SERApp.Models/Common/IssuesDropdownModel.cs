﻿using System.Collections.Generic;

namespace SERApp.Models.Common
{
    public class IssuesDropdownModel
    {
        public List<DropdownModel> Contacts { get; set; }
        public List<DropdownModel> IssueType1 { get; set; }
        public List<DropdownModel> IssueType2 { get; set; }
    }
}
