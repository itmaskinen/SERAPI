﻿using System.Collections.Generic;

namespace SERApp.Models.Common
{
    public class WeekTypeReportModel
    {
        public string Day { get; set; }
        public Dictionary<string, string> Columns { get; set; }
    }
}
