﻿namespace SERApp.Models
{
    public class EmailTemplateVariablesModel
    {
        public int Id { get; set; }
        public string VariableName { get; set; }
        public string VariableData { get; set; }
        public int EmailTemplateId { get; set; }
    }
}
