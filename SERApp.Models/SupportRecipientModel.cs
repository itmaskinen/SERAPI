﻿namespace SERApp.Models
{
    public class SupportRecipientModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public int AccountId { get; set; }
    }
}
