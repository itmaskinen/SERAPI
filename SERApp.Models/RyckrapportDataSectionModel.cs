﻿namespace SERApp.Models
{
    public class RyckrapportDataSectionModel
    {
        public int RyckrapportDataSectionId { get; set; }
        public int RyckrapportDataId { get; set; }
        public int SectionId { get; set; }

        public SectionModel Section { get; set; }
        public RyckrapportDataModel RyckrapportData { get; set; }
    }
}
