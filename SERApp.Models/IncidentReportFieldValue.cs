﻿namespace SERApp.Models
{
    public class IncidentReportFieldValue
    {
        public int Id { get; set; }
        public int FieldId { get; set; }
        public string FieldName { get; set; }
        public object Value { get; set; }
        public string Type { get; set; }
    }
}
