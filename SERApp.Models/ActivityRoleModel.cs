﻿namespace SERApp.Models
{
    public class ActivityRoleModel
    {
        public int ActivityRoleId { get; set; }
        public string Name { get; set; }
        public int AccountId { get; set; }
    }
}
