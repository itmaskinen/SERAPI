﻿namespace SERApp.Models
{
    public class ContractFilesModel
    {
        public string FileName { get; set; }
        public string Url { get; set; }
    }
}
