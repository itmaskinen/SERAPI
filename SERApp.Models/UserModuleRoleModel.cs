﻿using System;
using System.Collections.Generic;

namespace SERApp.Models
{
    public class UserModuleRoleModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ModuleId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? RoleId { get; set; }
        public int? SiteId { get; set; }
        public ModuleModel Module { get; set; }
        public UserModel User { get; set; }
        public RoleModel Role { get; set; }
    }

    public class UserModuleRoleModelBulk
    {
        public List<int> SiteIds { get; set; }
        public int UserId { get; set; }
        public int ModuleId { get; set; }
    }
}
