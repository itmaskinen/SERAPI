﻿namespace SERApp.Models
{
    public class DeleteResultModel
    {
        public bool IsDeleted { get; set; }
    }
}
