﻿namespace SERApp.Models
{
    public class IssueType2Model
    {
        public int IssueType2Id { get; set; }
        public string Name { get; set; }

        public int AccountId { get; set; }
    }
}
