﻿using System;
using System.Collections.Generic;

namespace SERApp.Models
{
    public class GuardRoundsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SiteId { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public string StartMessage { get; set; }
        public string StopMessage { get; set; }
        public string Description { get; set; }
        public List<int> ObjectIds { get; set; }
        public List<ObjectModel> Objects { get; set; }
        public int? ActiveRoundLogId { get; set; }
    }

    public class ObjectModel
    {
        public string Name { get; set; }
        public string NFCTag { get; set; }
        public int Id { get; set; }
        public bool IsNFCReady { get; set; }
        public bool IsScanned { get; set; }
        public bool IsManual { get; set; }
        public string Date { get; set; }
        public bool IsNFCEnabled { get; set; }
    }
}
