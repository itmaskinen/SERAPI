﻿using SERApp.Models.Enums;

namespace SERApp.Models
{
    public class IntervalModel
    {
        public int IntervalId { get; set; }
        public string Name { get; set; }
        public IntervalEnum IntervalType { get; set; }
        public int Value { get; set; }
        public int AccountId { get; set; }
    }
}
