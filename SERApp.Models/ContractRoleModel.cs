﻿namespace SERApp.Models
{
    public class ContractRoleModel
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public int AccountId { get; set; }
    }
}
