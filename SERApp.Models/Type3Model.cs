﻿using System.Collections.Generic;

namespace SERApp.Models
{
    public class Type3Model
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public bool IsTenantRequired { get; set; }
        public int TenantTypeId { get; set; }
        public int AccountId { get; set; }
        public int SiteId { get; set; }
        public bool IsActive { get; set; }
        public List<int> TenantTypeTypeIds { get; set; }

        public string TenantType { get; set; }
    }
}
