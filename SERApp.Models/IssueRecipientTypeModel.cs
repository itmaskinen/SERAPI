﻿namespace SERApp.Models
{
    public class IssueRecipientTypeModel
    {
        public int IssueRecipientTypeId { get; set; }
        public string Name { get; set; }
        public bool Hidden { get; set; }
    }
}
