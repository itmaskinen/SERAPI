﻿using System.Collections.Generic;

namespace SERApp.Models
{
    public class Files
    {
        public string Name { get; set; }
        public string Format { get; set; }
        public string Icon { get; set; }
        public bool IsFolder { get; set; }
        public string Uri { get; set; }
        public List<Files> Children { get; set; }
    }
}
