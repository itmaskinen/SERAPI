﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class GuardRoundLogModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumberOfRounds { get; set; }
        public int NumberOfScans { get; set; }
        public int NumberOfExpectedScans { get; set; }
        public string Guards { get; set; }
        public IEnumerable<Logs> Logs { get; set; }
    }

    public class Logs
    {
        public int Id { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }
        public string Guard { get; set; }
        public IEnumerable<Scans> Scans { get; set; }
    }

    public class Scans
    {
        public DateTime Date { get; set; }
        public string Vad { get; set; }
        public string Var { get; set; }
        public string Vem { get; set; }
        public string Guards { get; set; }
    }
}
