﻿using System;

namespace SERApp.Models
{
    public class SummaryModel
    {
        public int SummaryId { get; set; }
        public DateTime Date { get; set; }
        public string SummaryValue { get; set; }
        public int AccountId { get; set; }
        public int SiteId { get; set; }
    }
}
