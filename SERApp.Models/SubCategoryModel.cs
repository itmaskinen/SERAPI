﻿namespace SERApp.Models
{
    public class SubCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AccountId { get; set; }
    }
}
