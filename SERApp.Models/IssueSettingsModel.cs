﻿namespace SERApp.Models
{
    public class IssueSettingsModel
    {
        public int IssueSettingId { get; set; }
        public int StatusInterval { get; set; }
        public int AccountId { get; set; }
    }
}
