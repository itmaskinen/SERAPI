﻿namespace SERApp.Models
{
    public class ContractTagWordModel
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public int TagWordId { get; set; }

        public ContractModel Contract { get; set; }
        public TagWordModel TagWord { get; set; }
        public bool IsSelected { get; set; }
    }
}
