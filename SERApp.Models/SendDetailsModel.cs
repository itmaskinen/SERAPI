﻿using System.Collections.Generic;

namespace SERApp.Models
{
    public class SendDetailsModel
    {
        public int IssueWebId { get; set; }
        public int IssueNumber { get; set; }
        public List<IssueRecipientModel> Recipients { get; set; }
        public List<OtherRecipients> otherRecipients { get; set; }
        public string additionalMessage { get; set; }
    }
}
