﻿using System;
using System.Collections.Generic;

namespace SERApp.Models
{
    public class ConfirmMultipleResultModel
    {
        public List<DateTime> Dates { get; set; }
        public List<int> Ids { get; set; }
    }
}
