﻿namespace SERApp.Models
{
    public class IssueType1Model
    {
        public int IssueType1Id { get; set; }
        public string Name { get; set; }

        public int AccountId { get; set; }
    }
}
