﻿using System.Threading.Tasks;

namespace SERApp.Models
{
    public class TaskSubCategoryModel
    {
        public int Id { get; set; }
        public int SubCategoryId { get; set; }
        public int TaskId { get; set; }
        public virtual Task Task { get; set; }
        public virtual SubCategoryModel SubCategory { get; set; }

    }
}
