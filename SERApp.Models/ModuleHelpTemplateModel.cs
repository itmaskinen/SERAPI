﻿namespace SERApp.Models
{
    public class ModuleHelpTemplateModel
    {
        public int Id { get; set; }
        public int ModuleId { get; set; }
        public string Name { get; set; }
        public string ContentValue { get; set; }
    }
}
