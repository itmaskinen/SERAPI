﻿using System.Collections.Generic;

namespace SERApp.Models
{
    public class SectionModel
    {
        public int SectionId { get; set; }
        public string SectionValue { get; set; }
        //public int RyckrapportDataId { get; set; }
        public int SiteId { get; set; }

        public List<RyckrapportDataSectionModel> RyckrapportDataSections { get; set; }
    }
}
