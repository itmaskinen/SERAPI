﻿namespace SERApp.Models
{
    public class OpponentModel
    {
        public int Id { get; set; }
        public string OpponentHolderName { get; set; }
        public int AccountId { get; set; }

        public AccountModel Account { get; set; }
    }
}
