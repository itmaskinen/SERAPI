CREATE TABLE GuardRoundLogs (
	Id int not null IDENTITY(1,1),
	StartDate datetime not null,
	EndDate datetime null,
	GuardId int not null,
	UserId int not null,
	Notes varchar(max),
	AccountId int not null,
	StartMessage varchar(max),
	StopMessage varchar(max),
	GuardRoundId int not null
)