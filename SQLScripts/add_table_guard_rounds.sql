CREATE TABLE GuardRounds (
	Id int IDENTITY(1,1) NOT NULL,
	[Name] varchar(200),
	SiteId int,
	Description varchar(500)
)