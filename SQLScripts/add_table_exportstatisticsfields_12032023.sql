CREATE TABLE ExportStatisticsFields(
	[Id] [int] IDENTITY(1,1) NOT NULL,
    Category int NOT NULL,
	[Value] nvarchar(max),
	SiteId int NOT NULL
);