declare @addedSetting varchar(200), @setting varchar(200), @configSettingId int
set @addedSetting = ', "InitialMessageValue": ""}'
set @configSettingId = (select id from configsettings where settingname = 'ContactPublicPageData')

update Setting
set Value = REPLACE(Value, '}', @addedSetting)
where ConfigSettingId = @configSettingId