CREATE TABLE Building
(
	Id int IDENTITY(1,1) NOT NULL,
	[Name] varchar(200) NOT NULL,
	IsActive bit NOT NULL,
	AccountId int NOT NULL,
	SiteId int not null
)

CREATE TABLE [Level]
(
	Id int IDENTITY(1,1) NOT NULL,
	[Name] varchar(200) NOT NULL,
	IsActive bit NOT NULL,
	BuildingId int not null,
	AccountId int NOT NULL,
	SiteId int not null
)

CREATE TABLE Room
(
	Id int IDENTITY(1,1) NOT NULL,
	[Name] varchar(200) NOT NULL,
	IsActive bit NOT NULL,
	LevelId int null,
	BuildingId int not null,
	AccountId int NOT NULL,
	SiteId int not null
)

ALTER TABLE Tenants
ADD BuildingId int null, [LevelId] int null, RoomId int null

ALTER TABLE Room
Add SiteId int not null
ALTER TABLE [Level]
Add SiteId int not null
ALTER TABLE Building
Add SiteId int not null