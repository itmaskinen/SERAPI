update EmailTemplates
set body = 'Hej, <br/><br/> En l�senords�terst�llning har beg�rts f�r ditt konto. <br/><br/> Klicka p� l�nken nedan f�r att �terst�lla ditt l�senord. Denna l�nk upph�r att g�lla efter 30 minuter. <br/><br/> <a href="{changePassLink}" target="_blank">{changePassLink}</a> <br/><br/> V�nliga h�lsningar <br/> SER-teamet   '
where RoutineName='ForgotPassword'