CREATE TABLE TenantGuardRound (
	Id int not null IDENTITY(1,1),
	ObjectId int not null,
	GuardRoundId int not null,
	CreatedDate datetime not null,
	UpdatedDate datetime null
)