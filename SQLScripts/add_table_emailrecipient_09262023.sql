CREATE TABLE [dbo].[EmailRecipient] (
	Id int NOT NULL IDENTITY(1,1),
	AccountId int NOT NULL,
	ModuleId int NOT NULL,
	Recipient nvarchar(100) NOT NULL,
	SentDate datetime NOT NULL DEFAULT GETDATE()
)