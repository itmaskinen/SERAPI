﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace SERApp.Service.Services
{
    public interface IType3Service
    {
        IEnumerable<Type3Model> GetAllType3(int accountId, int siteId, bool isActiveOnly = true);
        Type3Model GetType3(int id);

        bool DeleteType3(int id);
        bool AddType3(Type3Model model);
        bool ToggleIsActive(int id);


    }
    public class Type3Service : IType3Service
    {
        IRepository<Type3TenantType> _type3TenantType;

        IType3Repository _repository;
        IIncidentRepository _incidentRepository;
        IRepository<Tenant> _tenantRepository;
        IRepository<TenantType> tenantTypeRepository;
        IRepository<Type3TenantType> type3TenantRepository;
        public Type3Service()
        {
            _type3TenantType = new Repository<Type3TenantType>();

            _repository = new Type3Repository();
            _incidentRepository = new IncidentRepository();
            _tenantRepository = new Repository<Tenant>();
            tenantTypeRepository = new Repository<TenantType>();
            type3TenantRepository = new Repository<Type3TenantType>();
        }


        public bool AddType3(Type3Model model)
        {
            try
            {
                if (model.Id != 0)
                {
                    var existingData = _repository.Get(model.Id);

                    existingData.Name = model.Name;
                    existingData.IsTenantRequired = model.IsTenantRequired;
                    existingData.AccountId = model.AccountId;
                    existingData.TenantTypeId = model.TenantTypeId;
                    existingData.SiteId = model.SiteId;

                    _repository.Update(existingData);

                    var existingRecords = _type3TenantType.GetByPredicate(e => e.Type3Id == model.Id).ToList();

                    foreach (var existingRecord in existingRecords)
                    {
                        _type3TenantType.Delete(existingRecord.Id);
                    }

                    foreach (var type3TenantTypeId in model.TenantTypeTypeIds)
                    {
                        _type3TenantType.Save(new Type3TenantType
                        {
                            Type3Id = model.Id,
                            TenantTypeId = type3TenantTypeId
                        });
                    }
                }
                else
                {
                    if (_repository.GetAll().Any(r => r.Name == model.Name && r.AccountId == model.AccountId))
                    {
                        return false;
                    }

                    var type3 = new Type3()
                    {
                        Name = model.Name,
                        IsTenantRequired = model.IsTenantRequired,
                        AccountId = model.AccountId,
                        TenantTypeId = model.TenantTypeId,
                        SiteId = model.SiteId,
                        IsActive = true,
                        Type3TenantTypes = model.TenantTypeTypeIds != null ? model.TenantTypeTypeIds.Select(e => new Type3TenantType
                        {
                            TenantTypeId = e
                        }).ToList() : new List<Type3TenantType>()
                    };

                    _repository.Save(type3);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteType3(int id)
        {
            if (_incidentRepository.GetAll().Any(r => r.Type3Id.HasValue && r.Type3Id == id))
            {
                return false;
            }

            _repository.Delete(id);
            return true;
        }

        public IEnumerable<Type3Model> GetAllType3(int accountId, int siteId, bool isActiveOnly = true)
        {
            var se = new CultureInfo("sv-SE");
            var data = _repository.GetAllType3IncludeChildren();

            if (accountId != 0)
            {
                data = data.Where(x => x.AccountId == accountId);
            }
            data = data.Where(e => !isActiveOnly || e.IsActive);

            var tenantTypeType3s = type3TenantRepository.GetAllByRawString($"SELECT * FROM Type3TenantTypes WHERE Type3Id in ({string.Join(",", data.Select(e => e.Id))})");//type3tenanttypes
            var tenantTypes = tenantTypeRepository.GetAllByRawString($"SELECT * FROM TenantTypes WHERE Id in ({string.Join(",", tenantTypeType3s.Select(e => e.TenantTypeId))})").ToList();

            var list = new List<Type3Model>();

            foreach (var row in data)
            {
                string tenantType = string.Empty;
                var tenantTypeType3 = tenantTypeType3s.FirstOrDefault(e => e.Type3Id == row.Id);
                if (tenantTypeType3 != null && row.IsTenantRequired)
                {
                    tenantType = tenantTypes.FirstOrDefault(e => e.Id == tenantTypeType3.TenantTypeId)?.TypeName;
                }
                list.Add(new Type3Model()
                {
                    Id = row.Id,
                    Name = row.Name,
                    IsTenantRequired = row.IsTenantRequired,
                    AccountId = row.AccountId,
                    TenantTypeId = row.TenantTypeId,
                    TenantType = tenantType,
                    IsActive = row.IsActive,
                });
            }

            return list.OrderBy(a => a.Name, StringComparer.Create(se, false)).ToList();
        }

        public Type3Model GetType3(int id)
        {
            var model = _repository.GetType3IncldeChildren(id);

            return new Type3Model()
            {
                Id = model.Id,
                Name = model.Name,
                IsTenantRequired = model.IsTenantRequired,
                AccountId = model.AccountId,
                TenantTypeId = model.TenantTypeId,
                TenantTypeTypeIds = model.Type3TenantTypes.Select(e => e.TenantTypeId).Distinct().ToList()
                //Tenant = model.Tenant != null ? new TenantModel()
                //{
                //    Name = model.Tenant.Name,
                //    Id = model.Tenant.Id,                    
                //}: new TenantModel(),
                //TenantId = model.TenantId
            };
        }

        public bool ToggleIsActive(int id)
        {
            var type = _repository.Get(id);
            if (type != null)
            {
                type.IsActive = !type.IsActive;
                _repository.Update(type);

                return type.IsActive;
            }
            throw new Exception("Type 3 not found");
        }
    }
}
