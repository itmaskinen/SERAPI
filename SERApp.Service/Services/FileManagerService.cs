﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using SERApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class FileManagerService
    {
        private readonly string storageConString;
        private readonly CloudBlobContainer container;
        public FileManagerService()
        {
            storageConString = ConfigurationManager.AppSettings["azurestorageconnectionstring"];


            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this.storageConString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            container = blobClient.GetContainerReference("ser-files");
        }

        public async Task<List<Files>> GetFilesAndFolders(int accountId)
        {
            return await ListFilesAndFolders(this.container, $"FileManager/{accountId}");
        }

        static async Task<List<Files>> ListFilesAndFolders(CloudBlobContainer container, string prefix = "FileManager/")
        {
            var lists = new List<Files>();
            BlobContinuationToken continuationToken = null;
            do
            {
                var resultSegment = await container.ListBlobsSegmentedAsync(prefix: prefix, currentToken: continuationToken);
                continuationToken = resultSegment.ContinuationToken;

                foreach (IListBlobItem item in resultSegment.Results)
                {
                    var list = new Files();
                    if (item is CloudBlobDirectory)
                    {
                        // It's a folder
                        CloudBlobDirectory directory = (CloudBlobDirectory)item;
                        Console.WriteLine($"Folder: {directory.Prefix}");

                        var prefixSplit = directory.Prefix.Split('/');
                        list.Name = prefixSplit[prefixSplit.Length - 2];
                        list.IsFolder = true;
                        list.Children = await ListFilesAndFolders(container.GetDirectoryReference(directory.Prefix).Container, directory.Prefix);
                    }
                    else if (item is CloudBlockBlob)
                    {
                        // It's a file
                        CloudBlockBlob blob = (CloudBlockBlob)item;
                        Console.WriteLine($"File: {blob.Name}");

                        list.Name = blob.Name.Replace(prefix, "");
                        list.Uri = blob.Uri.AbsoluteUri;
                    }

                    lists.Add(list);
                }
            } while (continuationToken != null);

            return lists;
        }
    }
}
