﻿
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SERApp.Service.Services
{
    public interface IType2Service
    {
        bool AddType2(Type2Model model);
        void UpdateType2(Type2Model model);
        bool DeleteType2ById(int id);
        bool ToggleIsActive(int id);
        IEnumerable<Type2Model> GetAllType2(int accountId, int siteId, bool isActiveOnly = false);
        Type2Model GetType2ById(int id);
    }

    public class Type2Service : IType2Service
    {
        private IRepository<Type2> _repository;
        private IIncidentRepository _incidentRepository;
        public Type2Service()
        {
            _incidentRepository = new IncidentRepository();
            _repository = new Repository<Type2>();
        }

        public bool AddType2(Type2Model model)
        {
            if (model.Id == 0)
            {
                if (_repository.GetAll().Any(x => x.Name == model.Name && x.AccountId == model.AccountId && model.SiteId == x.SiteId))
                {
                    return false;
                }

                _repository.Save(new Type2()
                {
                    CreatedDate = DateTime.Now,
                    Id = model.Id,
                    LastUpdatedDate = model.LastUpdatedDate,
                    Name = model.Name,
                    AccountId = model.AccountId,
                    SiteId = model.SiteId,
                    IsActive = true
                });

            }
            else
            {
                UpdateType2(model);
            }
            return true;
        }

        public void UpdateType2(Type2Model model)
        {
            var existingCustomType = _repository.Get(model.Id);
            existingCustomType.LastUpdatedDate = DateTime.Now;
            existingCustomType.Name = model.Name;
            existingCustomType.AccountId = model.AccountId;
            existingCustomType.SiteId = model.SiteId;
            _repository.Update(existingCustomType);
        }

        public bool DeleteType2ById(int id)
        {
            if (_incidentRepository.GetAll().Any(r => r.Type2Id.HasValue && r.Type2Id == id))
            {
                return false;
            }

            _repository.Delete(id);

            return true;
        }

        public IEnumerable<Type2Model> GetAllType2(int accountId, int siteId, bool isActiveOnly = true)
        {
            var data = _repository.GetAllByRawString($"SELECT * FROM Type2 WHERE (siteId = {siteId} or {siteId} = 0) AND AccountId = {accountId} {(isActiveOnly ? "AND IsActive = 1" : "")}").Select(x => new Type2Model()
            {
                CreatedDate = x.CreatedDate,
                Id = x.Id,
                LastUpdatedDate = x.LastUpdatedDate,
                Name = x.Name,
                AccountId = x.AccountId,
                SiteId = x.SiteId,
                IsActive = x.IsActive,
            }).ToList();

            return data;
        }

        public Type2Model GetType2ById(int id)
        {
            //use automapper for easy mapping
            var model = _repository.Get(id);

            return new Type2Model()
            {
                CreatedDate = model.CreatedDate,
                LastUpdatedDate = model.LastUpdatedDate,
                Name = model.Name,
                Id = model.Id,
                SiteId = model.SiteId
            };
        }

        public bool ToggleIsActive(int id)
        {
            var type2 = _repository.Get(id);
            if (type2 != null)
            {
                type2.IsActive = !type2.IsActive;
                _repository.Update(type2);

                return type2.IsActive;
            }
            throw new Exception("Type 1 not found");
        }
    }
}
