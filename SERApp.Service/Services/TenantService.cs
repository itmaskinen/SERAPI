﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Models.Constants;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using SERApp.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;

namespace SERApp.Service.Services
{
    public class TenantService
    {
        TenantRepository _tenantRepository;
        SiteRepository _siteRepository;
        CommonRepository _commonRepository;
        LogRepository _logRepository;
        IRepository<Account> _accountRepository;
        IRepository<User> _userRepository;
        IRepository<JobTitle> _jobTitleRepositry;
        IRepository<TenantContact> _tenantcontactRepository;
        IRepository<Contact> _contactRepository;
        IRepository<Contract> _contractRepository;
        IRepository<Tenant> _tenantRepo;
        IRepository<Type1> _type1Repo;
        IRepository<Type2> _type2Repo;
        IRepository<TenantType> _tenantType;
        IRepository<GuardRounds> _guardRounds;
        private readonly EmailRepository _emailRepository;
        IRepository<Setting> _settingRepo;
        IRepository<ObjectGuardRound> _tenantRoundsRepo;

        public TenantService()
        {
            _settingRepo = new Repository<Setting>();
            _tenantRepository = new TenantRepository();
            _logRepository = new LogRepository();
            _siteRepository = new SiteRepository();
            _commonRepository = new CommonRepository();
            _accountRepository = new Repository<Account>();
            _jobTitleRepositry = new Repository<JobTitle>();
            _tenantcontactRepository = new Repository<TenantContact>();
            _contactRepository = new Repository<Contact>();
            _emailRepository = new EmailRepository();
            _userRepository = new Repository<User>();
            _tenantRepo = new Repository<Tenant>();
            _contractRepository = new Repository<Contract>();
            _type1Repo = new Repository<Type1>();
            _type2Repo = new Repository<Type2>();
            _tenantType = new Repository<TenantType>();
            _tenantType = new Repository<TenantType>();
            _guardRounds = new Repository<GuardRounds>();
            _tenantRoundsRepo = new Repository<ObjectGuardRound>();

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        }

        public TenantModel Get(int id)
        {
            try
            {
                return entityToModel(_tenantRepository.Get(id));
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public TenantTypeModel GetTenantTypeModel(int id)
        {
            return GetTenantType(id, null);
        }

        public List<TenantTypeDailyReport> GetTenantTypeDailyReport(int id, int accountId, int siteId)
        {
            var vad = _type1Repo.GetAllByRawString($@"
                    SELECT t.* FROM Type1 t
                    LEFT JOIN Type1Setting ts on ts.Type1Id = t.Id and ts.SiteId = {siteId}
                    WHERE AccountId={accountId} AND (ts.IsActive = 1 or ts.Type1Id is null)").ToList(); ;
            var var = _type2Repo.GetAllByRawString($"SELECT * FROM Type2 WHERE (siteId = {siteId} or {siteId} = 0) AND AccountId = {accountId} AND IsActive = 1").ToList();
            var tenant = _tenantRepository.Get(id);
            if (tenant?.TenantType == null)
            {
                return null;
            }

            var data = GetTenantTypeModel(tenant.TenantType.Value);
            if (data != null)
            {
                if (data.PageSetting != null)
                {
                    var setting = data.PageSetting;
                    if (setting != null)
                    {
                        var infos = setting?.dailyReportInfos;
                        var result = new List<TenantTypeDailyReport>();
                        if (infos != null)
                        {
                            foreach (var x in infos)
                            {
                                result.Add(new TenantTypeDailyReport()
                                {
                                    Vad = new Selectable
                                    {
                                        Id = (int)x.vadId,
                                        Name = vad.FirstOrDefault(e => e.Id == (int)x.vadId)?.Name
                                    },
                                    Var = new Selectable
                                    {
                                        Id = (int)x.varId,
                                        Name = var.FirstOrDefault(e => e.Id == (int)x.varId)?.Name
                                    }
                                });
                            };
                        }
                        return result;
                    }
                }
            }

            return null;
        }

        public class TenantTypeDailyReport
        {
            public Selectable Vad { get; set; }
            public Selectable Var { get; set; }
        }

        public class Selectable
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
        public PremiseTypeModel GetPremiseTypeModel(int id)
        {
            return GetPremiseType(id, null);
        }
        public SubPremiseTypeModel GetSubPremiseTypeModel(int id)
        {
            return GetSubPremiseType(id, null);
        }

        public TenantTypeModel GetTenantType(int id, List<TenantType> tenantTypes)
        {

            if (tenantTypes == null)
            {
                tenantTypes = _commonRepository.GetTenantTypes();
            }


            var data = tenantTypes.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                return data.ToModel();
            }
            return new TenantTypeModel();
        }

        public PremiseTypeModel GetPremiseType(int id, List<PremiseType> premiseTypes)
        {
            if (premiseTypes == null)
            {
                premiseTypes = _commonRepository.GetPremiseTypes();
            }

            var data = premiseTypes.SingleOrDefault(x => x.Id == id);
            if (data != null)
            {
                return new PremiseTypeModel()
                {
                    Id = data.Id,
                    TenantTypeId = data.TenantTypeId,
                    AccountId = data.AccountId,
                    TypeName = data.TypeName,
                    Description = data.Description
                };
            }
            return new PremiseTypeModel();

        }

        public SubPremiseTypeModel GetSubPremiseType(int id, List<SubPremiseType> subPremiseTypes)
        {
            if (subPremiseTypes == null)
            {
                subPremiseTypes = _commonRepository.GetSubPremiseTypes();
            }

            var data = subPremiseTypes.SingleOrDefault(x => x.Id == id);
            if (data != null)
            {
                return new SubPremiseTypeModel()
                {
                    Id = data.Id,
                    PremiseTypeId = data.PremiseTypeId,
                    AccountId = data.AccountId,
                    TypeName = data.TypeName,
                    Description = data.Description,
                    PremiseTypeName = _tenantRepository.GetPremiseType(data.PremiseTypeId) != null ? _tenantRepository.GetPremiseType(data.PremiseTypeId).TypeName : "N / A"
                };
            }
            return new SubPremiseTypeModel();

        }

        public JobTitleModel GetGetJobTitle(int id)
        {
            var data = _tenantRepository.GetJobTitle(id);
            return new JobTitleModel()
            {
                Id = data.Id,
                TitleName = data.TitleName,
                Description = data.Description
            };
        }

        public byte[] GetXls(
            int accountId,
            int siteId = 0,
            int userId = 0,
            int moduleId = 0,
            string searchValue = "",
            int contactTypeData = 0,
            int categoryData = 0,
            int subCategoryData = 0,
            int filterActive = 0,
            string selectedIds = "",
            int guardRound = 0)
        {
            using (var excel = new ExcelPackage())
            {
                var se = new CultureInfo("sv-SE");
                var tenants = GetAllByAccountId(
                    accountId,
                    siteId,
                    userId,
                    moduleId,
                    searchValue,
                    contactTypeData,
                    categoryData,
                    subCategoryData,
                    filterActive,
                    string.IsNullOrEmpty(selectedIds) ? null : selectedIds.Split(',').Select(e => Convert.ToInt32(e)).ToList(),
                    guardRound)
                    .OrderBy(x => x.Name, StringComparer.Create(se, false))
                    .ThenBy(x => x.TenantTypeName, StringComparer.Create(se, false))
                    .ToList();

                var objectType = _tenantType.GetByPredicate(e => e.Id == contactTypeData).FirstOrDefault();
                var pageSetting = objectType.PageSetting;
                JObject parse = new JObject();
                if (pageSetting != null)
                {
                    parse = JObject.Parse(pageSetting);
                }

                var summarySheet = excel.Workbook.Worksheets.Add($"Contacts");
                var defaultFields = new List<string>()
                {
                    "title", "email", "phone", "postAddress", "visitingAddress", "comment"
                };

                var shownColumns = new List<string>();

                var column = 1;


                if (pageSetting == null || parse.ContainsKey("title") && parse["title"]["isVisible"].Value<bool>() == true)
                {
                    shownColumns.Add("title");

                    summarySheet.Cells[1, column].Value = parse.ContainsKey("title") ? parse["title"]["label"].Value<string>() : "Company / Brand";
                    summarySheet.Cells[1, column].Style.Font.Bold = true;

                    column++;
                }

                if (pageSetting == null || parse.ContainsKey("email") && parse["email"]["isVisible"].Value<bool>() == true)
                {
                    shownColumns.Add("email");

                    summarySheet.Cells[1, column].Value = parse.ContainsKey("email") ? parse["email"]["label"].Value<string>() : "Email Address";
                    summarySheet.Cells[1, column].Style.Font.Bold = true;

                    column++;
                }

                if (pageSetting == null || parse.ContainsKey("phone") && parse["phone"]["isVisible"].Value<bool>() == true)
                {
                    shownColumns.Add("phone");

                    summarySheet.Cells[1, column].Value = parse.ContainsKey("phone") ? parse["phone"]["label"].Value<string>() : "Phone";
                    summarySheet.Cells[1, column].Style.Font.Bold = true;

                    column++;
                }

                if (pageSetting == null || parse.ContainsKey("postAddress") && parse["postAddress"]["isVisible"].Value<bool>() == true)
                {
                    shownColumns.Add("postAddress");

                    summarySheet.Cells[1, column].Value = parse.ContainsKey("postAddress") ? parse["postAddress"]["label"].Value<string>() : "Post Address";
                    summarySheet.Cells[1, column].Style.Font.Bold = true;

                    column++;
                }

                if (pageSetting == null || parse.ContainsKey("visitingAddress") && parse["visitingAddress"]["isVisible"].Value<bool>() == true)
                {
                    shownColumns.Add("visitingAddress");

                    summarySheet.Cells[1, column].Value = parse.ContainsKey("email") ? parse["visitingAddress"]["label"].Value<string>() : "Visiting Address";
                    summarySheet.Cells[1, column].Style.Font.Bold = true;

                    column++;
                }

                if (pageSetting == null || parse.ContainsKey("comment") && parse["comment"]["isVisible"].Value<bool>() == true)
                {
                    shownColumns.Add("comment");

                    summarySheet.Cells[1, column].Value = parse.ContainsKey("comment") ? parse["comment"]["label"].Value<string>() : "Comment";
                    summarySheet.Cells[1, column].Style.Font.Bold = true;

                    column++;
                }

                if (pageSetting == null || parse.ContainsKey("contacts") && parse["contacts"]["isVisible"].Value<bool>() == true)
                {
                    shownColumns.Add("contacts");

                    summarySheet.Cells[1, column].Value = "Butikschef/Site Manger";
                    summarySheet.Cells[1, column].Style.Font.Bold = true;

                    column++;

                    summarySheet.Cells[1, column].Value = "Butikschef/Site Manger Telefonummer";
                    summarySheet.Cells[1, column].Style.Font.Bold = true;

                    column++;

                    summarySheet.Cells[1, column].Value = "Butikschef/Site Manger Email";
                    summarySheet.Cells[1, column].Style.Font.Bold = true;

                    column++;
                }

                summarySheet.Cells[1, column].Value = "Guard Rounds";
                column++;
                summarySheet.Cells[1, column].Value = "Contact Type";
                column++;
                summarySheet.Cells[1, column].Value = "Category";
                column++;
                summarySheet.Cells[1, column].Value = "Sub Category";
                column++;
                summarySheet.Cells[1, column].Value = "Last Updated Date";
                column++;
                summarySheet.Cells[1, column].Value = "Last Updated By";
                column++;

                summarySheet.Cells[1, column].Style.Font.Bold = true;

                var guardRounds = _guardRounds.GetAll().ToList();
                var objectIds = tenants.Select(x => x.Id);
                var guardObjects = _tenantRoundsRepo.GetByPredicate(e => objectIds.Contains(e.ObjectId)).ToList();
                var tenantcontactsdata = _tenantcontactRepository.GetAllByRawString($"SELECT * FROM TenantContacts TC INNER JOIN Tenants T ON TC.TenantId=T.Id WHERE T.SiteId = {siteId}").ToList();
                var contactsdata = new List<Contact>();
                var jobtitledata = new List<JobTitle>();

                if (tenantcontactsdata.Any())
                {
                    contactsdata = _contactRepository.GetAllByRawString($"SELECT * FROM Contacts WHERE Id IN ({string.Join(", ", tenantcontactsdata.Select(x => x.ContactId))})").ToList();
                    jobtitledata = _jobTitleRepositry.GetAllByRawString($"SELECT * FROM JobTitles WHERE Id IN ({string.Join(", ", tenantcontactsdata.Select(x => x.JobTypeId))}) AND IncludedOnExcelExport = 1").ToList();
                }

                int startRow = 2;
                tenants.ForEach(x =>
                {
                    column = 1;
                    var addContactsMode = false;
                    var firstRow = startRow;
                    shownColumns.ForEach(col =>
                    {
                        switch (col)
                        {
                            case "title":
                                summarySheet.Cells[startRow, column].Value = x.Name;
                                break;
                            case "email":
                                summarySheet.Cells[startRow, column].Value = x.Email;
                                break;
                            case "phone":
                                summarySheet.Cells[startRow, column].Value = x.Phone;
                                break;
                            case "postAddress":
                                summarySheet.Cells[startRow, column].Value = x.PostAddress;
                                break;
                            case "visitingAddress":
                                summarySheet.Cells[startRow, column].Value = x.VisitingAddress;
                                break;
                            case "comment":
                                summarySheet.Cells[startRow, column].Value = x.ContactComments;
                                break;
                            case "contacts":
                                var tcData = tenantcontactsdata.Where(r => r.TenantId == x.Id).Select(r => new { ContactId = r.ContactId, JobId = r.JobTypeId });

                                var objectContacts = contactsdata.Where(v => tcData.Any(tc => tc.ContactId == v.Id) && !string.IsNullOrEmpty(v.FullName));

                                foreach (var cc in objectContacts)
                                {
                                    if (addContactsMode)
                                    {
                                        startRow++;
                                    }

                                    summarySheet.Cells[startRow, column].Value = string.IsNullOrEmpty(cc.FullName) ? "N/A" : cc.FullName;
                                    summarySheet.Cells[startRow, column + 1].Value = string.IsNullOrEmpty(cc.MobileNumber) ? "N/A" : cc.MobileNumber;
                                    summarySheet.Cells[startRow, column + 2].Value = string.IsNullOrEmpty(cc.EmailAddress) ? "N/A" : cc.EmailAddress;
                                    addContactsMode = true;
                                }
                                break;
                        }
                        column++;

                        var endColumn = shownColumns.Count + (shownColumns.Any(e => e == "contacts") ? 3 : 1);

                        var currGuardRoundsIds = guardObjects.Where(e => e.ObjectId == x.Id).Select(e => e.GuardRoundId);
                        var currGuardRounds = guardRounds.Where(e => currGuardRoundsIds.Contains(e.Id));
                        summarySheet.Cells[firstRow, endColumn].Value = currGuardRounds.Any() ? string.Join(", ", currGuardRounds.Select(e => e.Name)): "";
                        endColumn++;

                        summarySheet.Cells[firstRow, endColumn].Value = x.TenantTypeName;
                        endColumn++;

                        summarySheet.Cells[firstRow, endColumn].Value = x.PremiseTypeName;
                        endColumn++;

                        summarySheet.Cells[firstRow, endColumn].Value = x.SubPremiseTypeName;
                        endColumn++;

                        summarySheet.Cells[firstRow, endColumn].Value = x.LastUpdatedDate.ToString("yyyy-MM-dd");
                        endColumn++;

                        summarySheet.Cells[firstRow, endColumn].Value = x.LastUpdatedByName;

                    });

                    startRow++;
                });
                summarySheet.Cells.AutoFitColumns();
                return excel.GetAsByteArray();
            }
        }

        public List<TenantModel> GetAll()
        {
            try
            {
                return _tenantRepository.GetAll().ToList().Select(t => t.ToModel()).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }

        }
        public List<TenantModel> GetTenantListByCSV(string csv)
        {
            if (string.IsNullOrEmpty(csv)) return null;

            var d = csv.Split(',').Where(r => !string.IsNullOrEmpty(r)).ToList();
            return _tenantRepository.GetAll()
                .Where(r => d.Contains(r.Id.ToString()))
                .ToList()
                .Select(t => t.ToModel())
                .ToList();
        }

        public List<TenantModel> GetAllByAccountIdAndSiteId(int accountId, int siteId, int roundId)
        {
            try
            {
                return GetAllByAccountId(accountId, siteId, guardRound: roundId);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public List<TenantModel> GetAllByAccountId(
            int accountId,
            int siteId,
            int userId = 0,
            int moduleId = 0,
            string searchValue = "",
            int contactTypeData = 0,
            int categoryData = 0,
            int subCategoryData = 0,
            int filterActive = 0,
            List<int> selectedIds = null,
            int guardRound = 0)
        {
            try
            {
                var users = _userRepository.GetAllByRawString($"SELECT * FROM Users WHERE AccountId = {accountId}").ToList();

                IQueryable<Tenant> tenants = accountId == 0 ?
                    _tenantRepository.GetAll() :
                    _tenantRepository.GetAllByAccountId(accountId);

                if (selectedIds?.Any() ?? false)
                {
                    tenants = tenants.Where(x => selectedIds.Contains(x.Id));
                }
                else
                {
                    if (siteId > 0)
                        tenants = tenants.Where(x => x.SiteId == siteId);
                    else // siteId <= 0
                    {
                        var validSitesIds = _siteRepository.GetValidSites(accountId, userId, moduleId).Select(v => v.Id).Distinct().ToList();
                        tenants = tenants.Where(x => validSitesIds.Contains(x.SiteId));
                    }

                    if (!string.IsNullOrEmpty(searchValue))
                        tenants = tenants.Where(x => x.Name.ToLower().Contains(searchValue.ToLower()));

                    if (contactTypeData != 0)
                        tenants = tenants.Where(x => x.TenantType == contactTypeData);

                    if (categoryData != 0)
                        tenants = tenants.Where(x => x.PermiseType == categoryData);

                    if (subCategoryData != 0)
                        tenants = tenants.Where(x => x.SubPremiseType == subCategoryData);

                    if (filterActive == 1)
                        tenants = tenants.Where(x => x.IsActive);
                    else if (filterActive == 2)
                        tenants = tenants.Where(x => !x.IsActive);

                    var ids = tenants.Select(x => x.Id);

                    if (ids.Any() && guardRound != 0)
                    {
                        var tenantRounds = _tenantRoundsRepo.GetByPredicate(e => e.GuardRoundId == guardRound).Select(e => e.ObjectId);

                        tenants = tenants.Where(e => tenantRounds.Contains(e.Id));
                    }
                }

                return tenants.ToList().Select(x => x.ToModel(users.FirstOrDefault(e => e.Id == x.LastUpdatedBy))).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public List<TenantModel> GetAllBySiteId(int siteId)
        {
            try
            {
                return _tenantRepository.GetAllBySiteId(siteId).ToList().Select(t => t.ToModel()).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public List<TenantModel> GetTenantByTenantTypeId(int siteId = 0, int tenantTypeId = 0)
        {
            try
            {
                var data = _tenantRepository.GetAllBySiteId(siteId);

                if (!(tenantTypeId == -1 || tenantTypeId == 0))
                {
                    data = data.Where(r => r.TenantType == tenantTypeId);
                }

                return data.ToList().Select(t => t.ToModel()).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public List<TenantContactModel> GetAllTenantContactsByTenantId(int tenantId)
        {



            var data = _tenantRepository.GetAllTenantContactsByTenantId(tenantId).ToList();
            var strings = data.Select(x => x.JobTypeId).ToList();
            string joined = string.Join(",", strings);
            var jt = new List<JobTitle>();
            if (strings.Any())
            {
                jt = _jobTitleRepositry.GetAllByRawString($"SELECT * FROM JobTitles WHERE Id IN ({joined})").ToList();
            }


            return data.Select(t => new TenantContactModel()
            {
                Id = t.Id,
                ContactId = t.Id,
                TenantContactId = t.TenantContactId,
                FullName = t.FullName,
                EmailAddress = t.EmailAddress,
                MobileNumber = t.MobileNumber,
                PhoneNumber = t.PhoneNumber,
                JobTypeId = t.JobTypeId,
                JobTypeName = jt.Where(x => x.Id == t.JobTypeId).Any() ? jt.FirstOrDefault(x => x.Id == t.JobTypeId).TitleName : "",
                JobTitle = t.JobTitle,
                IsActive = t.IsActive,
            })
            .OrderBy(t => t.FullName)
            .ToList();
        }

        public void SaveTenantContact(TenantContactModel contact)
        {
            _tenantRepository.SaveTenantContacts(contact);
        }

        private string JobTypeName(int tenantId, int contactId)
        {
            var data = _tenantRepository.GetTenantContact(tenantId, contactId);
            switch (data.JobTypeId)
            {
                case 1: return "Shop Owner";
                case 2: return "Site Manager";
                case 3: return "Contact Manager";
                case 4: return "Cashier";
                case 5: return "Other";
                default: return "N / A";
            }
        }

        public int SaveTenant(TenantModel model, bool isPublic = false)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.Image) && !model.Image.Contains("ser-files/Tenant"))
                {
                    model.Image = Tools.File.SaveFileToBlob(new FileModel()
                    {
                        Id = model.Id,
                        AccountId = model.AccountId,
                        FileName = $"Photo_{model.Id}",
                        ModuleType = "Tenant",
                        ByteString = model.Image.Split(',').Length > 0 ? model.Image.Split(',')[1] : model.Image,
                        FileType = ".jpg"
                    });
                }

                model.Id = _tenantRepository.SaveTenant(model, isPublic);
                return model.Id;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Save, 0, 0);
                throw ex;
            }
        }
        public List<TenantModel> SendEmail(List<TenantModel> models)
        {

            var creatorTemplate = _emailRepository.GetEmailTemplateFromRoutineName("UpdateContactInformation");
            var siteId = _tenantRepo.Get(models.FirstOrDefault().Id).SiteId;
            var accountId = _siteRepository.Get(siteId).AccountId;
            var accountName = _accountRepository.Get(accountId).Company;

            var sitea = _siteRepository.Get(siteId);

            var facebookLink = sitea.FacebookLink;
            var instagramLink = sitea.InstagramLink;
            var youtubeLink = sitea.YoutubeLink;
            var emailAdd = sitea.PublicEmailAddress;
            var contact = sitea.PublicContactNumber;
            var siteName = sitea.Name;
            var GDPRTitle = sitea.GDPRTitle;
            var GDPRContent = sitea.GDPRContent;

            var returnData = new List<TenantModel>();
            foreach (var tenant in models)
            {
                var mm = new TenantModel();

                mm.Name = tenant.Name;
                mm.Email = tenant.Email;
                var server = ConfigurationManager.AppSettings["server"];
                var url = "";
                switch (server)
                {
                    case "Staging":
                        url = $"http://s4test.serapp.com/#/contact/{tenant.Id}/update";
                        break;
                    case "Production":
                        url = $"https://s4.serapp.com/#/contact/{tenant.Id}/update";
                        break;
                    case "Development":
                        url = $"http://localhost:55299/#/contact/{tenant.Id}/update";
                        break;
                    default:
                        url = $"http://localhost:55299/#/contact/{tenant.Id}/update";
                        break;
                }

                if (!string.IsNullOrEmpty(tenant.Email))
                {
                    Site tt = null;
                    try
                    {
                        //  tt =  _settingRepo.GetAllByRawString($"SELECT * FROM setting WHERE Name='PublicPageImage' AND AccountId = {accountId}").FirstOrDefault();
                        tt = _siteRepository.Get(tenant.SiteId);
                        //  tt = sit !=null sit.CompanyLogo : "";
                    }
                    catch
                    {

                    }

                    var setting = _settingRepo.GetAllByRawString($"SELECT S.* FROM Setting S INNER JOIN ConfigSettings CS ON CS.Id = S.ConfigSettingId WHERE CS.SettingName = 'ContactEmailData' AND S.AccountId = {accountId}").ToList();

                    if (setting != null && setting.Any())
                    {
                        var ss = setting.FirstOrDefault();

                        if (!string.IsNullOrEmpty(ss.Value))
                        {
                            var mod = JsonConvert.DeserializeObject<TenantEmailSettingModel>(ss.Value);
                            creatorTemplate.Subject = mod.Subject;

                            creatorTemplate.Body = creatorTemplate.Body.Replace("{{Subject}}", mod.Subject)
                                .Replace("{{Body}}", mod.Body)
                                .Replace("{{Footer}}", mod.Foot);
                        }
                        else
                        {
                            creatorTemplate.Body = creatorTemplate.Body.Replace("{{Subject}}", "Dags att uppdatera butikens information!")
                                .Replace("{{Body}}", "<p style='color: grey'>        Klicka på <span style='color: black; font - weight: bold'>STARTA</span> och kontrollera om din butiks uppgifter stämmer. Om inte, uppdatera informationen. Avsluta genom att klicka på <span style='color: black; font - weight: bold'>SPARA</span> även om du inte uppdaterat något!       </p>")
                                .Replace("{{Footer}}", "<p style='color: grey'>Vänligen,<br/> Centrumledningen för {{site}}</p> ");

                        }

                    }

                    var t = Service.Tools.Email.SendEmail(new EmailModel(accountId)
                    {
                        Name = accountName,
                        From = "s4@serapp.com",
                        To = tenant.Email,
                        Subject = creatorTemplate.Subject,
                        Body = creatorTemplate.Body.Replace("{{url}}", url)
                        .Replace("{{facebook}}", string.IsNullOrEmpty(facebookLink) ? "" : $"<a href='{facebookLink}'><img src='data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij48cGF0aCBkPSJNMjIuNjc1IDBoLTIxLjM1Yy0uNzMyIDAtMS4zMjUuNTkzLTEuMzI1IDEuMzI1djIxLjM1MWMwIC43MzEuNTkzIDEuMzI0IDEuMzI1IDEuMzI0aDExLjQ5NXYtOS4yOTRoLTMuMTI4di0zLjYyMmgzLjEyOHYtMi42NzFjMC0zLjEgMS44OTMtNC43ODggNC42NTktNC43ODggMS4zMjUgMCAyLjQ2My4wOTkgMi43OTUuMTQzdjMuMjRsLTEuOTE4LjAwMWMtMS41MDQgMC0xLjc5NS43MTUtMS43OTUgMS43NjN2Mi4zMTNoMy41ODdsLS40NjcgMy42MjJoLTMuMTJ2OS4yOTNoNi4xMTZjLjczIDAgMS4zMjMtLjU5MyAxLjMyMy0xLjMyNXYtMjEuMzVjMC0uNzMyLS41OTMtMS4zMjUtMS4zMjUtMS4zMjV6Ii8+PC9zdmc+'></a>")
                        .Replace("{{instagram}}", string.IsNullOrEmpty(instagramLink) ? "" : $"<a href='{instagramLink}'><img src='data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij48cGF0aCBkPSJNMTIgMi4xNjNjMy4yMDQgMCAzLjU4NC4wMTIgNC44NS4wNyAzLjI1Mi4xNDggNC43NzEgMS42OTEgNC45MTkgNC45MTkuMDU4IDEuMjY1LjA2OSAxLjY0NS4wNjkgNC44NDkgMCAzLjIwNS0uMDEyIDMuNTg0LS4wNjkgNC44NDktLjE0OSAzLjIyNS0xLjY2NCA0Ljc3MS00LjkxOSA0LjkxOS0xLjI2Ni4wNTgtMS42NDQuMDctNC44NS4wNy0zLjIwNCAwLTMuNTg0LS4wMTItNC44NDktLjA3LTMuMjYtLjE0OS00Ljc3MS0xLjY5OS00LjkxOS00LjkyLS4wNTgtMS4yNjUtLjA3LTEuNjQ0LS4wNy00Ljg0OSAwLTMuMjA0LjAxMy0zLjU4My4wNy00Ljg0OS4xNDktMy4yMjcgMS42NjQtNC43NzEgNC45MTktNC45MTkgMS4yNjYtLjA1NyAxLjY0NS0uMDY5IDQuODQ5LS4wNjl6bTAtMi4xNjNjLTMuMjU5IDAtMy42NjcuMDE0LTQuOTQ3LjA3Mi00LjM1OC4yLTYuNzggMi42MTgtNi45OCA2Ljk4LS4wNTkgMS4yODEtLjA3MyAxLjY4OS0uMDczIDQuOTQ4IDAgMy4yNTkuMDE0IDMuNjY4LjA3MiA0Ljk0OC4yIDQuMzU4IDIuNjE4IDYuNzggNi45OCA2Ljk4IDEuMjgxLjA1OCAxLjY4OS4wNzIgNC45NDguMDcyIDMuMjU5IDAgMy42NjgtLjAxNCA0Ljk0OC0uMDcyIDQuMzU0LS4yIDYuNzgyLTIuNjE4IDYuOTc5LTYuOTguMDU5LTEuMjguMDczLTEuNjg5LjA3My00Ljk0OCAwLTMuMjU5LS4wMTQtMy42NjctLjA3Mi00Ljk0Ny0uMTk2LTQuMzU0LTIuNjE3LTYuNzgtNi45NzktNi45OC0xLjI4MS0uMDU5LTEuNjktLjA3My00Ljk0OS0uMDczem0wIDUuODM4Yy0zLjQwMyAwLTYuMTYyIDIuNzU5LTYuMTYyIDYuMTYyczIuNzU5IDYuMTYzIDYuMTYyIDYuMTYzIDYuMTYyLTIuNzU5IDYuMTYyLTYuMTYzYzAtMy40MDMtMi43NTktNi4xNjItNi4xNjItNi4xNjJ6bTAgMTAuMTYyYy0yLjIwOSAwLTQtMS43OS00LTQgMC0yLjIwOSAxLjc5MS00IDQtNHM0IDEuNzkxIDQgNGMwIDIuMjEtMS43OTEgNC00IDR6bTYuNDA2LTExLjg0NWMtLjc5NiAwLTEuNDQxLjY0NS0xLjQ0MSAxLjQ0cy42NDUgMS40NCAxLjQ0MSAxLjQ0Yy43OTUgMCAxLjQzOS0uNjQ1IDEuNDM5LTEuNDRzLS42NDQtMS40NC0xLjQzOS0xLjQ0eiIvPjwvc3ZnPg=='></a>")
                        .Replace("{{youtube}}", string.IsNullOrEmpty(youtubeLink) ? "" : $"<a href='{youtubeLink}'><img src='data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij48cGF0aCBkPSJNMTkuNjE1IDMuMTg0Yy0zLjYwNC0uMjQ2LTExLjYzMS0uMjQ1LTE1LjIzIDAtMy44OTcuMjY2LTQuMzU2IDIuNjItNC4zODUgOC44MTYuMDI5IDYuMTg1LjQ4NCA4LjU0OSA0LjM4NSA4LjgxNiAzLjYuMjQ1IDExLjYyNi4yNDYgMTUuMjMgMCAzLjg5Ny0uMjY2IDQuMzU2LTIuNjIgNC4zODUtOC44MTYtLjAyOS02LjE4NS0uNDg0LTguNTQ5LTQuMzg1LTguODE2em0tMTAuNjE1IDEyLjgxNnYtOGw4IDMuOTkzLTggNC4wMDd6Ii8+PC9zdmc+'></a>")
                        .Replace("{{publicemail}}", emailAdd)
                        .Replace("{{publiccontact}}", contact)
                        .Replace("{{site}}", siteName)
                        .Replace("{{Image}}", tt != null ? tt.CompanyLogo : "")
                        .Replace("{{GDPRContent}}", GDPRContent)
                        .Replace("{{GDPRTitle}}", GDPRTitle)
                    });
                    mm.IsSent = t;
                }
                else
                {
                    mm.IsSent = false;
                }

                returnData.Add(mm);
            }


            var tenantIds = models.Select(x => x.Id).Distinct().ToList();
            var ids = string.Join(",", tenantIds);
            if (tenantIds.Any())
            {
                _tenantRepo.UpdateAll($"UPDATE Tenants SET LastSentUpdateEmail = '{DateTime.Now.ToUniversalTime()}' WHERE Id IN ({ids})");
            }

            return returnData;
        }
        public void DeleteTenant(int id)
        {
            try
            {
                _tenantRepository.DeleteTenant(id);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Delete, 0, 0);
                throw ex;
            }
        }

        public bool SaveTenantType(TenantTypeModel model)
        {
            if (model.Id == 0)
            {
                if (_commonRepository.GetTenantTypes().Any(x => x.TypeName == model.TypeName && x.AccountId == model.AccountId && x.AccountId == model.AccountId))
                {
                    return false;
                }
            }

            _tenantRepository.SaveTenantType(model);
            return true;
        }

        public bool SavePremiseType(PremiseTypeModel model)
        {
            if (model.Id == 0)
            {
                if (_commonRepository.GetPremiseTypes().Any(r => r.TypeName == model.TypeName && r.AccountId == model.AccountId))
                {
                    return false;
                }
            }

            _tenantRepository.SavePremiseType(model);
            return true;
        }

        public bool SaveSubPremiseType(SubPremiseTypeModel model)
        {
            if (model.Id == 0)
            {
                if (_commonRepository.GetSubPremiseTypes().Any(r => r.TypeName == model.TypeName && r.AccountId == model.AccountId))
                {
                    return false;
                }
            }

            _tenantRepository.SaveSubPremiseType(model);
            return true;
        }

        public void SaveJobTitle(JobTitleModel model)
        {
            if (model.Id == 0)
            {
                if (_commonRepository.GetJobTitles().Any(r => r.TitleName == model.TitleName && r.AccountId == model.AccountId))
                {
                    return;
                }
            }
            _tenantRepository.SaveJobTite(model);
        }

        public bool DeleteTenantType(int id)
        {
            if (_tenantRepository.GetAll().Any(r => r.TenantType == id)) return false;

            _tenantRepository.DeleteTenantType(id);
            return true;
        }

        public bool DeletePremiseType(int id)
        {
            if (_tenantRepository.GetAll().Any(r => r.PermiseType == id)) return false;

            _tenantRepository.DeletePremiseType(id);
            return true;
        }

        public bool DeleteSubPremiseType(int id)
        {
            if (_tenantRepository.GetAll().Any(r => r.SubPremiseType == id)) return false;

            _tenantRepository.DeleteSubPremiseType(id);
            return true;
        }

        public bool DeleteJobTittle(int id)
        {
            _tenantRepository.DeleteJobTitle(id);
            return true;
        }

        public void DeleteTenantContact(int id)
        {
            _tenantRepository.DeleteTenantContact(id);
        }

        public List<TenantTypeModel> GetAllTenantTypes(int accountId)
        {
            return _commonRepository.GetTenantTypes()
                .Where(c => c.AccountId == accountId)
                .Select(t => new TenantTypeModel()
                {
                    Id = t.Id,
                    AccountId = t.AccountId,
                    TypeName = t.TypeName,
                    Description = t.Description,
                    IsSystem = t.AccountId == 0,
                    PageSetting = t.PageSetting,
                    IsPopupMessage = t.IsPopupMessage,
                    DefaultGuardRoundId = t.DefaultGuardRoundId,
                })
                .OrderBy(t => t.TypeName)
                .ToList();
        }

        public List<PremiseTypeModel> GetAllPremiseTypes(int accountId)
        {


            return _commonRepository.GetPremiseTypes()
                .Where(c => c.AccountId == accountId)
                .Select(t => new PremiseTypeModel()
                {
                    Id = t.Id,
                    TenantTypeId = t.TenantTypeId,
                    TypeName = t.TypeName,
                    AccountId = t.AccountId,
                    Description = t.Description,
                    IsSystem = t.AccountId == 0,
                    TenantTypeName = GetTenantType(t.TenantTypeId, _commonRepository.GetTenantTypes()).TypeName
                })
                .OrderBy(t => t.TypeName)
                .ToList();
        }

        public List<SubPremiseTypeModel> GetAllSubPremiseTypes(int accountId)
        {
            return _commonRepository.GetSubPremiseTypes()
                .Where(c => c.AccountId == accountId)
                .Select(t => new SubPremiseTypeModel()
                {
                    Id = t.Id,
                    PremiseTypeId = t.PremiseTypeId,
                    TypeName = t.TypeName,
                    AccountId = t.AccountId,
                    Description = t.Description,
                    PremiseTypeName = _tenantRepository.GetPremiseType(t.PremiseTypeId).TypeName,
                    IsSystem = t.AccountId == 0
                })
                .OrderBy(t => t.TypeName)
                .ToList();
        }

        public List<JobTitleModel> GetAllGetJobTitles(int accountId)
        {
            return _commonRepository.GetJobTitles()
                .Where(c => c.AccountId == accountId)
                .Select(t => new JobTitleModel()
                {
                    Id = t.Id,
                    TitleName = t.TitleName,
                    AccountId = t.AccountId,
                    Description = t.Description,
                    IncludedOnExcelExport = t.IncludedOnExcelExport,
                    IsSystem = t.AccountId == 0
                })
                .OrderBy(t => t.TitleName)
                .ToList();
        }

        public TenantModel entityToModel(Tenant entity)
        {
            var account = _accountRepository.GetAllByRawString($"SELECT * FROM Accounts LEFT JOIN Sites ON Sites.AccountId = Accounts.Id WHERE Sites.Id = {entity.SiteId}").FirstOrDefault();
            var user = _userRepository.GetAllByRawString($"SELECT * FROM Users WHERE Id = {entity.LastUpdatedBy}").FirstOrDefault();
            var tenantGuards = _tenantRoundsRepo.GetByPredicate(e => e.ObjectId == entity.Id).ToList();

            var model = entity.ToModel();
            if (account != null)
            {
                model.AccountId = account.Id;
                model.Company = account.Company;
            }

            if (user != null)
            {
                model.LastUpdatedByName =
                    entity.LastUpdatedBy != 0 ? $"{user.FirstName} {user.LastName}" : string.Empty;
            }

            model.GuardRounds = tenantGuards.Select(e => e.GuardRoundId).ToList();

            return model;
        }

        public List<ContractModel> GetContracts(int id)
        {
            var query = $@"SELECT c.* from Contracts c
                            join Sites s on s.Id = c.SiteId
                            where TenantId = {id}";
            return _contractRepository.GetAllByRawString(query).Select(e => new ContractModel
            {
                Id = e.Id,
                SiteId = e.SiteId,
                ContractNo = e.ContractNo,
                ValidFrom = e.ValidFrom,
                ValidTo = e.ValidTo,
                RemindDate = e.RemindDate,
                CancelLatest = e.CancelLatest,
                SiteName = e.Site.Name
            }).ToList();
        }
    }
}
