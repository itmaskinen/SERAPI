export class UserModel {
    public AccountId : number;
    public Id : number;
    public IsActive: boolean;
    public IsDeleted: boolean;
    public CreatedDate: string;
    public LastUpdatedDate: string;
    public FirstName: string;
    public LastName: string;
    public Mobile: string;
    public PhoneNumber: string;
    public UserName: string;
    public RoleId: number;
    public EmailAddress: string;
    public Password: string;
    public PasswordReType: string;
    public Role : any = {
        Name : '',
        Description: ''
    };
}