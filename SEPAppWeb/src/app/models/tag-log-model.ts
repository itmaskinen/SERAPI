import { TagModel } from "./tag-model";
import { GuardModel } from "./guard-model";

export class TagLogModel {
    public Id: number;
    public TagId: number;
    public Tag: TagModel;
    public Time: Date;
    public GuardId : number;
    public Guard : GuardModel;
}
