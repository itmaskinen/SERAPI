export class SiteModel {
    public AccountId: number = 0;
    public Name: string = "";
    public Address: string = "";
    public City: string = "";
    public PostalCode: string = "";
    public AdditionalLocationInfo: string = "";
    public PhoneNumber: string = "";
    public FaxNumber: string = "";
    public ContactPerson: string = "";
    public ContactPersonNumber: string = "";
    public CreatedDate: string = "";
    public LastUpdatedDate: string = "";
    public IsActive: boolean = true;
    public IsDeleted: boolean = false;
}