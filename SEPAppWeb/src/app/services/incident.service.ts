import { Injectable } from '@angular/core';
import { BaseDataService, DataService } from '../core/services';
import { API_URL } from '../constants';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class IncidentService extends BaseDataService{

  constructor(protected dataService: DataService) 
  {
    super(dataService, API_URL);
  }

  public getAllIncidents(id: number): Observable<any>{

    console.log(id);
    let url = `Incident/GetAll?id=${id}`;    
    return super.getAll<any>(url);
  }

  public getIncident(id?: number): Observable<any>{
    let url = `Incident/Get?id=${id}`;
    return super.getAll<any>(url);
  }

  public saveIncident(data){
    let url = `Incident/Save`;
    return super.save<any>(url,data);
  }

  public deleteIncident(data){
      let url = `Incident/Delete`;
      return super.save<any>(url,data);
  }

}
