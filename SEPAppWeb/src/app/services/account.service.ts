import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IPagedResults } from '../core/interfaces';

@Injectable()
export class AccountService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }
    
    public getAccountsList(page:number, pageSize: number = 10, filter: any): Observable<IPagedResults<any>>{
        let url = `Account/GetAll`;    
        return super.getAll<any>(url);
    }

    public getAccount(id: number){
        let url = `Account/Get?id=${id}`;    
        return super.getById<any>(url);
    }

    public saveAccount(data){
        let url = `Account/Save`;
        return super.save<any>(url,data);
    }

    public deleteAccount(data){
         let url = `Account/Delete`;
        return super.save<any>(url,data);
    }
}
