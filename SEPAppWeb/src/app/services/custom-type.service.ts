import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IPagedResults } from '../core/interfaces';

@Injectable()
export class CustomTypeService extends BaseDataService {
  constructor(protected dataService: DataService) 
  {
    super(dataService, API_URL);
  }

  public getAllCustomTypes(page:number, pageSize: number = 10, filter: any): Observable<any>{
    let url = `CustomType/GetAll`;    
    return super.getAll<any>(url);
  }

  public getCustomType(id?: number): Observable<any>{
    let url = `CustomType/Get?id=${id}`;
    return super.getAll<any>(url);
  }

  public saveCustomType(data){
    let url = `CustomType/Save`;
    return super.save<any>(url,data);
  }

  public deleteCustomType(data){
      let url = `CustomType/Delete`;
      return super.save<any>(url,data);
  }

}
