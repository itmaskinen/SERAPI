import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IPagedResults } from '../core/interfaces';

@Injectable()
export class SiteService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }
    
    public getSitesList(page:number, pageSize: number = 10, filter: any): Observable<IPagedResults<any>>{
        let url = `Site/GetAll`;    
        return super.getAll<any>(url);
    }

    public getSitesByAccountList(id: number): Observable<IPagedResults<any>>{
        let url = `Site/GetByAccountId?id=${id}`;    
        return super.getAll<any>(url);
    }

    public getSite(id: number){
        let url = `Site/Get?id=${id}`;    
        return super.getById<any>(url);
    }

    public saveSite(data){
        let url = `Site/Save`;
        return super.save<any>(url,data);
    }

    public deleteSite(data){
         let url = `Site/Delete`;
        return super.save<any>(url,data);
    }
}
