import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IPagedResults } from '../core/interfaces';

@Injectable()
export class AuthenticationService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }

    public auth(data){
        let url = `Authentication/Login`;
        return super.save<any>(url,data);
    }

    public logoff(data){
        let url = `Authentication/Logoff?userId=${data.userId}`;
        return super.save<any>(url,data);
    }
}
