import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { DataService } from './data.service';
import { IPagedResults } from '../interfaces';

export class BaseDataService {
    constructor(protected dataService: DataService, protected baseUrl: string) {
     }

    protected getAll<T>(api?: string): Observable<T> {
        let url = `${this.baseUrl}/${api}`;

        this.validationUrl(url)

        return this.dataService.get(url)
            .map((response: any) => {
                return <T>response;
            })
            .do((response: T) => {
                //console.log(response);
            })
            .catch((error: Response | any) => {
                return Observable.throw(error.message);
            });
    }

    protected getAllPaged<T>(api?: string, params?: any): Observable<IPagedResults<T>> {
       let url = `${this.baseUrl}/${api}`;
        

        this.validationUrl(url);

        return this.dataService.get(url, params)
            .map((response: any) => {
                return <IPagedResults<T>>(response);
            })
            .do((response: IPagedResults<T>) => {
                //console.log(response);
            })
            .catch((error: Response | any) => {
                return Observable.throw(error.message);
            });
    }

    protected getById<T>(id: string): Observable<T> {
        const url: string = `${this.baseUrl}/${id}`;

        this.validationUrl(url);

        return this.dataService.get(url)
            .map((response: any) => {
                return <T>(response);
            })
            .do((response: T) => {
                //console.log(response);
            })
            .catch((error: Response | any) => {
                //console.log(error);
                return Observable.throw(error.message);
            });
    }

    protected save<T>(api: string, obj: Object): Observable<T> {
       let url = `${this.baseUrl}/${api}`;
        

        this.validationUrl(url);

        return this.dataService.post(url, obj)
            .map((response: any) => {
                return <T>(response);
            })
            .do((response: T) => {
                //console.log(response);
            })
            .catch((error: Response | any) => {
                console.log(error.message);

                return Observable.throw(error.message);
            });
    }

    protected saveAll<T>(fullUrl?: string, obj?: Object): Observable<T> {
      let url = this.baseUrl;
        if (fullUrl) {
            url = fullUrl;
        }

        this.validationUrl(url);

        return this.dataService.post(url, obj)
            .map((response: any) => {
                return <T>(response);
            })
            .do((response: T) => {
            })
            .catch((error: Response | any) => {
                console.log(error.message);
                return Observable.throw(error.message);
            });
    }

    protected delete(api: string, id: number): Observable<Response> {
        const url = `${this.baseUrl}/${api}`;

        this.validationUrl(url);

        return this.dataService.delete(url)
            .do((response: any) => {
                //console.log(response);
            })
            .catch((error: Response | any) => {
                //console.log(error);
                return Observable.throw(error.message);
            });
    }

    protected getFromExternalAPI<T>(fullUrl?: string, obj?: Object): Observable<T> {
        let url = this.baseUrl;
        if (fullUrl) {
            url = fullUrl;
        }

        this.validationUrl(url);

        return this.dataService.get(url, obj)
            .map((response: any) => {
                return <T>(response);
            })
            .do((response: T) => {
            })
            .catch((error: Response | any) => {
                console.log(error.message);
                return Observable.throw(error.message);
            });
    }

    protected download(api: string, mimeType: string, fileName: string): Observable<boolean> {
        const url = `${this.baseUrl}/${api}`;
        this.validationUrl(url);

        return this.dataService.downloadBlob(url, mimeType)
            .do((response: any) => {
                const downloadUrl = window.URL.createObjectURL(response);
                const anchor = <HTMLAnchorElement>document.getElementById('downloadAnchor');
                
                anchor.download = fileName;
                anchor.href = downloadUrl;
                document.body.appendChild(anchor);
                anchor.click();
            })
            .catch((error: Response | any) => {
                console.log(error.message);
                return Observable.throw(error.message);
            });
    }

    private validationUrl(url: string) {
        if (this.baseUrl === "") {
            throw "Url must be provided.";
        }
    }
}
