import { ElementRef } from "@angular/core";
import { StringHelper } from '../../shared/helpers';

import { API_URL } from '../../constants';
import { Router, ActivatedRoute, RouterStateSnapshot  } from '@angular/router';

import { Location } from '@angular/common';

export class BaseComponent {
    public isLoading: boolean = false;
    public isSaving: boolean = false;
    public accountId: number = 0;

    public secondaryUserData: any;
    public superAdminAccess: any;

    constructor(
       private baseLocation: Location,
       private baseRouter: Router,
       private baseRoute:ActivatedRoute) {
        this.baseInterceptUrl(window.location.pathname);
    }

    ///SUPER FUNCTIONS
    public baseInterceptUrl(url? : string){
        this.secondaryUserData = this.secondaryAdminUser();
        if(this.withSecondaryUserData() && this.URLReferrerHasSuperAdminAccess()) {
            this.accountId = this.secondaryUserData.AccountId;
            if(url.indexOf('superAdminAccess') === -1)
            {
                this.baseRouter.navigateByUrl(url + `?superAdminAccess=1&accountId=${this.accountId}`);
            }
        }
    }

    public getLastUrl(): string{
       return localStorage.getItem('lastUrl');
    }

    public setLastUrl(url: string){
        localStorage.setItem('lastUrl', url);
    }

    public lastAccessedURL(){
        return document.referrer;
    }

    public URLReferrerHasSuperAdminAccess(){
        return this.getLastUrl().indexOf('superAdminAccess') > -1;
    }

    public baseHasSuperAccessUrl(){
        let url = window.location.href;
        return url.indexOf('superAdminAccess') > -1 && Number(this.baseRoute.snapshot.queryParams['superAdminAccess']) > 0;
    }

    ///HELPER FUNCTIONS
    public getTextFromDropDownList (el: ElementRef): string {
        if (el === null) {
            throw 'Unable to get text from drop down list because the element is undefined. Please ensure the element ref is retrieved correctly.';
        }

        let text: string = el.nativeElement.value.trim();
        if (text.toLowerCase() === 'all' || text.toLowerCase() === 'none' || text.toLowerCase() === 'undefined') {
            text = '';
        }

        return text;
    }

    public csvToJson(csvString: string) {
        const lines = csvString.trim().split("\n");
        const result = [];
        const headers = lines[0].split(",");        
        for (let i = 1 ; i < lines.length; i++) {
            const obj = {};
            const currentline = lines[i].split(",");
            for (let j = 0; j < headers.length; j++) {
                obj[headers[j].trim()] = currentline[j].trim();
            }
            result.push(obj);
        }
        return JSON.stringify(result);
    }

    public randomString(length) {
        const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        length = length || 10;
        let string = '', rnd;
            while (length > 0) {
                rnd = Math.floor(Math.random() * chars.length);
                string += chars.charAt(rnd);
                length--;
            }
        return string;
    }

    public isVisible(data: any, propertyName: string): boolean {
        if (typeof data === "undefined" || data === null) {
            return false;
        }

        const fieldData = data[propertyName];

        if (typeof fieldData === "undefined" || fieldData === null) {
            return false;
        } 

        if (typeof fieldData === "string") {
            return StringHelper.isNullOrEmpty(fieldData);
        }

        return true;
    }

    public isNullOrEmpty(value: string) {
        return StringHelper.isNullOrEmpty(value);
    }

    ///PROPERTIES
    public currentLoggedUser(): any{
        let currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        return currentUserData;
    }

    public secondaryAdminUser(): any{
        if(this.withSecondaryUserData())
        {
            return JSON.parse(localStorage.getItem('secondaryAdminUser'));
        }
        return {};
    }

    public currentRole(): string{
        return this.currentLoggedUser().role;
    }

    public withSecondaryUserData(): boolean {
        return JSON.parse(localStorage.getItem('secondaryAdminUser'));
    }
    
    public hasSuperAccess(){
        return this.withSecondaryUserData() && this.baseHasSuperAccessUrl();
    }

}
