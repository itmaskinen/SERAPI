import { ToastOptions } from 'ng2-toastr';

export class NotficationOptions extends ToastOptions {
    toastLife = 2500;
    dismiss = 'auto';
    newestOnTop = true;
    showCloseButton = true;
    positionClass = 'toast-bottom-right';
    animate = 'flyRight'; // you can override any options available
    enableHTML = true;
}