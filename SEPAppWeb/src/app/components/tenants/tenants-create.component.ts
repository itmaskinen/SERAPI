import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { BaseComponent } from '../../core/components';

import { TenantService, SiteService } from '../../services';
import { TenantsModel } from '../../models';

@Component({
    selector: 'app-tenants-create',
    templateUrl: './tenants-create.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class TenantsCreateComponent extends BaseComponent implements OnInit {
    //
    @ViewChild('siteFilter') siteFilterFilterEl: ElementRef;
    public model: any;
    public selectedAccount: any;
    public accountsList: any;
    public hideAccountSelect: boolean = true;
    public showSpinner: boolean = false;
    public siteList: any;
    constructor(
        private tenantsService: TenantService,
        private siteService: SiteService, 
        private route: ActivatedRoute,
        private toastr: ToastsManager,
        private router: Router,
        private location: Location) {
            super(location, router, route);
        }

    public ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        this.model = new TenantsModel();
        this.model.AccountId = currentUser.accountId;
        this.loadSitesById(currentUser.accountId);
    }

    public loadSitesById(id:number){
        this.showSpinner = true;
        this.siteService.getSitesByAccountList(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response:any) => {
            this.siteList = response.Data;
        });
    }
    public saveChanges(){
        this.showSpinner = true;
        this.tenantsService.saveTenant(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
               this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            //this.toastr.success(response.Message, "Success"); 
            this.ngOnInit();
        });
    }

    public selectSite(site : any){
        let selectedSite = this.siteList.filter((item) => {
            return item.Name === site;
        })[0];
        
        this.model.SiteId = selectedSite.Id;
    }

}
