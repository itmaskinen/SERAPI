import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { BaseComponent } from '../../core/components';

import { AccountService, SiteService } from '../../services';
import { SiteModel } from '../../models';

@Component({
    selector: 'app-sites-create',
    templateUrl: './sites-create.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class SitesCreateComponent extends BaseComponent implements OnInit {
    @ViewChild('accountsFilter') accountsFilterEl: ElementRef;
    public model: any;
    public selectedAccount: any;
    public accountsList: any;
    public hideAccountSelect: boolean = true;
    public showSpinner: boolean = false;
    constructor(
        private siteService: SiteService, 
        private accountService: AccountService,
        private route: ActivatedRoute,
        private toastr: ToastsManager,
        private router: Router,
        private location: Location) {
            super(location, router, route);
        }

    public ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        this.model = new SiteModel();
        this.model.AccountId = currentUser.accountId;
        this.route.params.subscribe((params) => {
            if (params.length > 0) {
                const accountId = params['account'];
                this.selectedAccount = accountId;
                this.model.AccountId = accountId;
            }
        });
    }

    public saveChanges(){
        this.showSpinner = true;
        this.siteService.saveSite(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            //this.toastr.success(response.Message, "Success"); 
            this.ngOnInit();
        });
    }

}
