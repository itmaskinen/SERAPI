import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';
import { routerTransition } from '../../router.animations';

import { ToastsManager } from 'ng2-toastr';

import { SiteService } from '../../services';

@Component({
    selector: 'app-sites-list',
    templateUrl: './sites-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class SitesListComponent implements OnInit {
    public showSpinner: boolean = false;
    public itemPerPage: number = 100;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public isAdminOrUserEditor = false;
    public isSuperAdmin = false;
    public isAdmin = false;
    constructor( 
        private router: Router,
        private route:ActivatedRoute, 
        private toastr: ToastsManager,
        private siteService: SiteService) {}

    packageData: Subject<any> = new Subject<any[]>();  

    public ngOnInit() {
        if(this.isSuperAdmin)
        {
            this.loadData();
        }
    }

    public loadData(page?: number, id?: number){
        this.showSpinner = true;
        let filter = '';
        if (page != null) {
            this.currentPage = page; 
        } 
        if(id > 0)
        {
            this.loadDataById(id);
        }
        else {
            this.siteService.getSitesList(this.currentPage, this.itemPerPage, filter)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                 setTimeout(() => {
                    this.showSpinner = false;
                }, 1000);
            })
            .subscribe((response : IPagedResults<any>) => {
                this.packageData.next(response.Data);
                this.totalItems = response.Data.length;
                //this.toastr.success(response.Message, "Success"); 
            });
        }
        
    }

    public loadDataById(id:number){
        this.showSpinner = true;
        this.siteService.getSitesByAccountList(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : IPagedResults<any>) => {
            this.packageData.next(response.Data);
            this.totalItems = response.Data.length;
        });
    }

    public editSite(id:number){
        this.router.navigate([`objects/${id}/edit`]);  
    }

    public editAccountSite(id:number){
        if(this.isSuperAdmin)
        {
            this.router.navigate([`accounts/${id}/edit/objects`]);
        }
    }
}
