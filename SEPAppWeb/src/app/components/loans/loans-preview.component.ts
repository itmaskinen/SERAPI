import { Component, OnInit, ViewChild } from '@angular/core';
import { LoansListComponent } from './loans-list.component';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { LoanService, TenantService, SiteService } from '../../services';

import { LoanModel } from '../../models';
@Component({
    selector: 'app-loans-preview',
    templateUrl: './loans-preview.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class LoansPreviewComponent implements OnInit {
    isAdminOrUserEditor: boolean;
    model : any;
    status : any;
    badgeClass: any;
    customerType: any;
    customerTypeClass: any;
    returned: boolean = false;
    constructor(
        private loanService : LoanService,
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastsManager
    ) {}

    public ngOnInit() {
        this.model = new LoanModel();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
        else if(currentUser.role === 'superadmin')
        {

        }

    }

    public loadLoanPreview(data: any){
        //this.model = data;
        this.loadData(data.Id);
    }

    public loadData(id: number){
        this.loanService.getLoan(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {

            }, 1000);
        })
        .subscribe((response : any) => {
            this.model = response.Data;
            this.model.PhotoUrl = response.Data.PhotoUrl ? response.Data.PhotoUrl : '';
            if(this.model.Status === 'Active'){
                this.status = 'ACTIVE';
                this.badgeClass = ' badge-info';
            }
            else if(this.model.Status === 'NotConfirmed'){
                this.status = 'NOT CONFIRMED';
                this.badgeClass = ' badge-primary';
            }
            else if(this.model.Status === 'PassedReturnedData'){
                this.status = 'PASSED RETURNED DATE';
                this.badgeClass = ' badge-warning';
            }
            else if(this.model.Status === 'Returned'){
                this.status = 'RETURNED';
                this.badgeClass = ' badge-success';
                this.returned = true;
            }
            else if(this.model.Status === 'Deleted'){
                this.status = 'DELETED';
                this.badgeClass = ' badge-danger';
            }

            if(this.model.CustomerTypeId === 1)
            {
                this.customerType = 'CUSTOMER';
                this.customerTypeClass = 'badge-secondary';
            }
            else {
                this.customerType = 'TENANT';
                this.customerTypeClass = 'badge-dark';
            }

        });
    }

    public returnLoan(){
        this.loanService.returnLoan(this.model)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
 
            }, 1000);
        })
        .subscribe((response : any) => {
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                //this.toastr.success(response.Message, "Success"); 
                this.loadData(this.model.Id);
            }
        });
    }
}
