import { Component, OnInit, ViewChild } from '@angular/core';
import { LoansListComponent } from './loans-list.component';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { LoanService, TenantService, SiteService } from '../../services';

import { LoanModel } from '../../models';
@Component({
    selector: 'app-loans-confirm',
    templateUrl: './loans-confirm.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class LoansConfirmComponent implements OnInit {
    isAdminOrUserEditor: boolean;
    model : any;
    confirmed: boolean = false;
    encryptedEmail: string = "";
    showSpinner: boolean = false;
    constructor(
        private loanService : LoanService,
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastsManager
    ) {}

    public ngOnInit() {
        this.model = new LoanModel();
        let confirm = this.route.snapshot.queryParams['code'];
        if(confirm)
        {
            let raw = confirm.split(':');
            let email = raw[0];
            let id = raw[1];
            this.encryptedEmail = email;
            this.loadData(Number(id));
        }
    }

    public loadData(id?: number){
        this.showSpinner = true;
        this.loanService.getLoan(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.model = response.Data;
            this.model.Id = id;
            if(this.model.Status === "Active")
            {
                this.confirmed = true;
            }
        });
    }

    public confirmLoan(){
        this.showSpinner = true;
        let originalEmail = this.model.Customer.Email;
        this.model.Customer.Email = this.encryptedEmail;
        this.loanService.confirmLoan(this.model)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.model.Customer.Email = originalEmail;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                //this.toastr.success(response.Message, "Success"); 
                this.confirmed = true;
            }
        });
    }
}
