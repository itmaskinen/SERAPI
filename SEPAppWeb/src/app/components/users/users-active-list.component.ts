import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';
import { ToastsManager } from 'ng2-toastr';

import { UserService } from '../../services';

@Component({
    selector: 'app-users-active-list',
    templateUrl: './users-active-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class UsersActiveListComponent implements OnInit {
    //public showSpinner: boolean = false;
    public itemPerPage: number = 10;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public accountId: number = 0;
    public model: any;
    public isAdminOrUserEditor = false;
    public isSuperAdmin = false;
    constructor(private router: Router,
        private route:ActivatedRoute, 
        private toastr: ToastsManager,
        private userService: UserService) {}
    
    packageData: any[];  
    dataList : Array<any> = [];

    public ngOnInit() {
        this.loadData();

    }

    public loadData(page?: number, id?: number, filter: string = ""): void{
        //this.showSpinner = true;
        if (page != null) {
            this.currentPage = page; 
        } 
        if(id > 0)
        {
            this.loadDataById(id);
        }
        else {
            this.userService.getUsersList(this.currentPage, this.itemPerPage)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                 setTimeout(() => {
                    //this.showSpinner = false;
                }, 1000);
            })
            .subscribe((response : any) => {
                this.packageData = response.Data;
                if(filter !== '')
                {
                    this.dataList = response.Data.filter((item) => {
                        return item.Account.Name.toLowerCase().indexOf(filter.toLowerCase()) > -1;
                    });
                }        
                this.totalItems = response.TotalRecords;
                //this.toastr.success(response.Message, "Success");
            });
        }
    }
   
    public loadDataById(id:number){
        this.accountId = id;
        //this.showSpinner = true;
        this.userService.getUsersByAccountList(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                //this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.packageData = response.Data;
            this.totalItems = response.Data.length;
        });
    }

    public editUser(id:number){
        this.router.navigate([`${id}/edit`], { relativeTo: this.route });  
    }

    public editUserRoleModules(id:number){
        this.router.navigate([`${id}/module-roles/${this.accountId}`], { relativeTo: this.route }); 
    }

    public editAccountUser(id:number){
        if(this.isSuperAdmin)
        {
            this.router.navigate([`accounts/${id}/edit/users`]);  
        }
    }
}
