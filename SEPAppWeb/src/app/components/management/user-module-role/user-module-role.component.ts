import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { BaseComponent } from '../../../core/components';

import { SiteService, UserService, ManagerService, UserModuleRoleService } from '../../../services';
import { UserModel, UserModuleRoleModel } from '../../../models';

@Component({
    selector: 'app-user-module-role',
    templateUrl: './user-module-role.component.html',
    styleUrls: ['../../layout/layout.component.scss']
})
export class UserModuleRoleComponent extends BaseComponent implements OnInit {
    public model: any;
    public roleModel: any;
    public roleModels: Array<UserModuleRoleModel>;
    public modules: Array<any>;
    public moduleCount: number = 0;
    public sites: Array<any>;
    public roles: any;
    public showSpinner: boolean = false;
    constructor(
        private userRoleService: UserModuleRoleService,
        private siteService : SiteService,
        private managerService: ManagerService,
        private userService: UserService,
        private route: ActivatedRoute,
        private toastr: ToastsManager,
        private router: Router,
        private location: Location) {
            super(location, router, route);
        }

    public ngOnInit() {
        this.model = new UserModel();
        this.roleModel = new UserModuleRoleModel();
        this.roles = [
            { 'display' : 'None', 'value' : 0 },
            { 'display' : 'Admin', 'value' : 1 },
            { 'display' : 'Editor', 'value' : 2 },
            { 'display' : 'View', 'value' : 3 },
        ]
        this.route.params.subscribe((params) => {
            if (params) {
                const accountId = params['account'];
                const id = params['id'];
                this.model.AccountId = accountId;
                this.model.Id = id;
                this.roleModel.UserId = id;
                this.loadUserData(id);
                this.loadData(id);
                this.loadModulesByAccount(this.model.AccountId);
            }
        });
    }

    public loadData(id:number){
        this.showSpinner = true;
        this.userRoleService.GetUserModuleRolesById(id)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            this.roleModels = response.Data;
        });
    }

    public IsRoleSelected(roleId, moduleId, siteId){
        if(this.roleModels && this.roleModels.length > 0)
        {
            let selectedRole = this.roleModels.filter((item) => {
                return (item.RoleId === roleId && item.ModuleId === moduleId && item.SiteId === siteId);
            });
            return selectedRole.length > 0;
        }

        return false;
    }

    public loadUserData(id:number){
        this.showSpinner = true;
        this.userService.getUser(id)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            this.model = response.Data;
            //this.toastr.success(response.Message, "Success"); 
        });
    }

    public loadModulesByAccount(id:number){
        this.managerService.getModulesByAccountIdList(id)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                 setTimeout(() => {
                    this.showSpinner = false;
                }, 1000);
            })
            .subscribe((response : any) => {
                let data = response.Data;
                let adminDefaultModuleNodes = [
                    { 'Id' : 1, 'ShortName' : 'administration', 'level' : 'main' }, 
                    { 'Id' : 17, 'ShortName' : 'accounts' , 'level' : 'sub'},
                    { 'Id' : 19, 'ShortName' : 'users' , 'level' : 'sub'}, 
                    { 'Id' : 20, 'ShortName' : 'sites' , 'level' : 'sub'}];

                let dataModules = data.map((item) => {
                    return {
                        Id: item.Id,
                        ShortName : item.ShortName,
                        level: item.NodeLevel
                    }
                });
                this.modules = dataModules.concat(adminDefaultModuleNodes);
                this.moduleCount = this.modules.length;
                this.loadAccountSites(this.model.AccountId);
            });
    }
        
    public loadAccountSites(id:number){
        this.showSpinner = true;
        this.siteService.getSitesByAccountList(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.sites = response.Data;
        });
    }

    public saveChanges(){
        this.showSpinner = true;
        this.userRoleService.save(this.roleModel)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            //this.toastr.success(response.Message, "Success"); 
            this.ngOnInit();
        });
    }

    public onRoleChange(evt: any, moduleId: number, siteId: number){
        let roleId = Number(evt.currentTarget.value);
        this.roleModel = {
            RoleId : Number(roleId),
            ModuleId : Number(moduleId),
            SiteId : Number(siteId),
            UserId : this.model.Id
        }
        let data = JSON.stringify(this.roleModel);
        setTimeout(() => {
            this.saveChanges();
        }, 1000);
    }
    
    //can be used as a global function
    public createRange(number:number){
        var items: number[] = [];
        for(var i = 1; i <= number; i++){
            items.push(i);
        }
        return items;
    }
}

  
