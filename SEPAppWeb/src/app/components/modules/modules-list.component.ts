import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../router.animations';
import { ModuleService } from '../../services';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
    selector: 'app-modules-list',
    templateUrl: './modules-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class ModulesListComponent implements OnInit {
    public itemPerPage: number = 100;
    public currentPage: number = 1;
    public totalItems: number = 10;

    constructor(private router: Router,
        private route:ActivatedRoute, 
        private toastr: ToastsManager,
        private moduleService: ModuleService) {}

    packageData: Subject<any> = new Subject<any[]>(); 

    public ngOnInit() 
    {
        this.loadData();
    }
    public loadData(page?: number){
        let filter = '';
        if (page != null) {
            this.currentPage = page; 
        } 
        this.moduleService.getModulesList(this.currentPage, this.itemPerPage, filter)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            
        })
        .subscribe((response : IPagedResults<any>) => {
            this.packageData.next(response.Data);
            this.totalItems = response.TotalRecords;
            //this.toastr.success(response.Message, "Success"); 
        });
    }
}
