import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { GuardService } from '../../services';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { TagLogService } from '../../services/tag-log.service';
import { IPagedResults } from '../../core/interfaces';

@Component({
  selector: 'app-tag-logs-list',
  templateUrl: './tag-logs-list.component.html',
  styleUrls: ['./tag-logs-list.component.scss']
})
export class TagLogsListComponent implements OnInit {
  public showSpinner: boolean = false;
  public itemPerPage: number = 10;
  public currentPage: number = 1;
  public totalItems: number = 10;
  public isAdminOrUserEditor = false;
  public isSuperAdmin = false;
  //public isPreviewOn: boolean = false;
  
  constructor(    
    private router: Router,
    private route:ActivatedRoute, 
    private toastr: ToastsManager,
    private tagLogService: TagLogService) 
    {
      
    }
    packageData: Subject<any> = new Subject<any[]>();  

  ngOnInit() 
  {
    if(this.isSuperAdmin)
    {        
        this.loadData();
    }
  }

  public loadData(page?: number, id?: number){
    this.showSpinner = true;
    let filter = '';
    if (page != null) {
        this.currentPage = page; 
    } 

    this.tagLogService.getAllTagLogs(this.currentPage, this.itemPerPage, filter)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
        this.packageData.next(response.Data);
        this.totalItems = response.Data.length;
        //this.toastr.success(response.Message, "Success"); 
    });    
  }

  public loadDataById(id:number){
    this.showSpinner = true;
     this.tagLogService.getAllTagLogs(this.currentPage, this.itemPerPage, null)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
        this.packageData.next(response.Data);
        this.totalItems = response.Data.length;
       // this.toastr.success(response.Message, "Success"); 
    }); 
}

public editGuard(id:number){
    this.router.navigate([`${id}/edit`], { relativeTo: this.route });  
}

}
