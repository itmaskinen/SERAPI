import { Component, OnInit } from '@angular/core';
import { GuardModel } from '../../models';
import { GuardService, SiteService } from '../../services';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { IPagedResults } from '../../core/interfaces';

@Component({
  selector: 'app-guard-edit',
  templateUrl: './guard-edit.component.html',
  styleUrls: ['./guard-edit.component.scss']
})
export class GuardEditComponent implements OnInit {

  photoSrc: string;
  isAdminOrUserEditor: boolean;
  sitesList: any[];
  currentUser: any;
  showSpinner: boolean;
  model: GuardModel = <GuardModel>
  {
    SiteId:0,  
  };
  constructor(private guardService: GuardService,
    private route: ActivatedRoute,
    private siteService: SiteService,
    private toastr: ToastsManager,
    private router: Router) { }

  ngOnInit() {
    this.setDefaults();
    this.siteService.getSitesByAccountList(this.currentUser.accountId)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        this.sitesList = response.Data;
        this.toastr.success(response.Message, "Success"); 
    });  


    this.route.params.subscribe((params) => {         
      if (params) {
          const id = params['id'];
          this.loadData(id);
      }
  });

  }

  public setDefaults(){
    let now = new Date();
    this.photoSrc = "/assets/images/upload-empty.png";
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser.role === 'admin')
    {
        this.isAdminOrUserEditor = true;
    }
    else if(this.currentUser.role === 'user')
    {
        this.isAdminOrUserEditor = false;
    }
}

  public loadData(id: number){
    this.showSpinner = true;
    
    
    this.guardService.getGuard(id)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
      this.model = response.Data;      
      console.log(this.model.SiteId);
      
      this.toastr.success(response.Message, "Success"); 
    });      
}


public saveChanges() : void{
  //alert('test');
  this.showSpinner = true;
  this.guardService.saveGuard(this.model)
  .catch((err: any) => {
      this.toastr.error(err, 'Error');
      return Observable.throw(err);
  }).finally(() => {
       setTimeout(() => {
          this.showSpinner = false;
      }, 1000);
  })
  .subscribe((response) => {
      if(response.ErrorCode)
      {
          this.toastr.error(response.Message, "Error"); 
      }
      else {
          this.toastr.success(response.Message, "Success"); 
      }
      this.ngOnInit();
  });
}
}
