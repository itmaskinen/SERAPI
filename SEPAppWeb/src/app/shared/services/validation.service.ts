import { FormArray, FormGroup, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

export class ValidationService{
    static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
        let config = {
            'required': 'Required',
            'invalidCreditCard': 'Is invalid credit card number',
            'invalidEmailAddress': 'Invalid email address',
            'invalidPassword': 'Invalid password. Password must be at least 6 characters long, and contain a number.',
            'invalidMoile' : 'Invalid Mobile number',
            'minLengthArray' : 'Required minimum one select the check box',
            'minlength': `Minimum length ${validatorValue.requiredLength}`,
            'matchPassword' : 'Password not patch',
            'isUserIsDuplicateByEmail' : 'That email address is taken. Try another one',
            'isAccountIsDuplicateByName' : 'That Account name is taken. Try another one'
        };

        return config[validatorName];
    }

    static creditCardValidator(control) {
        if (control.value.match(/^$/))
            return null;
        // Visa, MasterCard, American Express, Diners Club, Discover, JCB
        if (control.value.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)) {
            return null;
        } else {
            return { 'invalidCreditCard': true };
        }
    }

    static emailValidator(control) {
        // RFC 2822 compliant regex
        if (control.value.match(/^$/))
            return null;
        
        if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
        } else {
            return { 'invalidEmailAddress': true };
        }
        
    }

    static passwordValidator(control) {
        if (control.value.match(/^$/))
            return null;
        // {6,100}           - Assert password is between 6 and 100 characters
        // (?=.*[0-9])       - Assert a string has at least one number
        if (control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/)) {
            return null;
        } else {
            return { 'invalidPassword': true };
        }
    }
    static checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
        return (group: FormGroup) => {
          let passwordInput = group.controls[passwordKey],
              passwordConfirmationInput = group.controls[passwordConfirmationKey];
          if (passwordInput.value !== passwordConfirmationInput.value) {
              return {'passwordNotMatch' : true };
          }
          else {
              return null;
          }
        }
    }

    static matchPassword(AC: AbstractControl) {
        let password = AC.get('newPassword').value; // to get value in input tag
        let confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
        if(password != confirmPassword) {
            AC.get('confirmPassword').setErrors( {matchPassword: true} )
            return {'matchPassword' : true };
        } else {
            return null
        }
    }

    static mobiledValidator(control) {
        if((control.value && control.value !=''))
        {
             if (control.value.match(/^$/)){
                return null;
             }
             // {4,100}           - Assert password is between 4 and 100 characters
             // (?=.*[0-9])       - Assert a string has at least one number
             if (control.value.match(/^[0-9]{10,12}$/)) {
                return null;
             } else {
                return { 'invalidMoile': true };
             }
        }
        else {
            return null;
        }
    }

    static minLengthArray(min: number) {
        return (c: AbstractControl): {[key: string]: any} => {
            if (c.value.length >= min){
                return null;
            }else{
                return { 'minLengthArray': true };
                // return { 'minLengthArray': {valid: false }};
            }
        }
    }

    static multipleCheckboxRequireOne(fa: FormArray) {
        let valid = false;
        
        for (let x = 0; x < fa.length; ++x) {
          if (fa.at(x).value) {
            valid = true;
            break;
          }
        }
        return valid ? null : {
          multipleCheckboxRequireOne: true
        };
    }

    static isUserIsDuplicateByEmail(userService){
        return (c: AbstractControl): {[key: string]: any} => {
                let email = c.value;
                if(!c.value) return { 'isUserIsDuplicateByEmail' : false };
                return new Promise(resolve => {
                    userService.getUserIsDuplicateByEmail(email)
                    .subscribe((response: any) => {
                        if(!response.success){
                            resolve({
                                'isUserIsDuplicateByEmail' : false
                            })
                        }
                        else {
                            resolve(null)
                        }
                    });
                })
            };
    }
        
    static isAccountIsDuplicateByName(accountservice){
        return (c: AbstractControl): {[key: string]: any} => {
                let dbName = c.value;
                if(!c.value) return { 'isAccountIsDuplicateByName' : false };
                return new Promise(resolve => {
                    accountservice.getAccountIsDuplicateByName(dbName)
                    .subscribe((response: any) => {
                        if(!response.success){
                            resolve({
                                'isAccountIsDuplicateByName' : false
                            })
                        }
                        else {
                            resolve(null)
                        }
                    });
                })
        };
    }
}
