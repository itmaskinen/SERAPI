import { Pipe, PipeTransform } from '@angular/core';

// tslint:disable-next-line:use-pipe-transform-interface
@Pipe({
    name: 'nullableBoolean'
})
export class NullableBooleanPipe implements PipeTransform {
    public transform(value: boolean | undefined) {
        if (value === null || value === undefined) {
            return '';
        }

        return value ? 'Yes' : 'No';
    }
}
