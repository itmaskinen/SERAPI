﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SERApp.API.Extensions
{
    public static class HeaderExtension
    {
        public static string Encrypt(this string value)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] Key = { 12, 13, 14, 15, 16, 17, 18, 19 };
            byte[] IV = { 12, 13, 14, 15, 16, 17, 18, 19 };

            ICryptoTransform encryptor = des.CreateEncryptor(Key, IV);

            var suffix = ";PUBLICSERAPP;INCIDENTS";
            byte[] IDToBytes = ASCIIEncoding.ASCII.GetBytes($"{value}{suffix}");
            byte[] encryptedID = encryptor.TransformFinalBlock(IDToBytes, 0, IDToBytes.Length);
            return Convert.ToBase64String(encryptedID);
        }

        public static string Decrypt(this string encrypted)
        {
            byte[] Key = { 12, 13, 14, 15, 16, 17, 18, 19 };
            byte[] IV = { 12, 13, 14, 15, 16, 17, 18, 19 };

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            ICryptoTransform decryptor = des.CreateDecryptor(Key, IV);

            byte[] encryptedIDToBytes = Convert.FromBase64String(encrypted);
            byte[] IDToBytes = decryptor.TransformFinalBlock(encryptedIDToBytes, 0, encryptedIDToBytes.Length);
            return ASCIIEncoding.ASCII.GetString(IDToBytes);
        }

        public static int? GetIdFromHeader(this string apikey)
        {
            try
            {
                var decrypt = Decrypt(apikey);

                var splits = decrypt.Split(';');
                if (splits.Length < 3)
                {
                    return null;
                }
                if (splits[1] == "PUBLICSERAPP" && splits[2] == "INCIDENTS")
                {
                    return Convert.ToInt32(splits[0]);
                }

                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}