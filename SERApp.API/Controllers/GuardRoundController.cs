﻿using QRCoder;
using SERApp.Data.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    [Authorize]
    public class GuardRoundController : ApiController
    {
        private readonly IGuardRoundRepository repository;
        public GuardRoundController()
        {
            repository = new GuardRoundRepository();
        }

        [HttpPost]
        [Route("Round/Start")]
        public IHttpActionResult StartRound(GuardRoundLogs data)
        {
            try
            {
                return Ok(repository.StartRound(data));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
        }

        [HttpPut]
        [Route("Round/Stop")]
        public IHttpActionResult StopRound(GuardRoundLogs data)
        {
            try
            {
                repository.StopRound(data);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
        }

        [HttpPut]
        [Route("Round/Notes")]
        public IHttpActionResult SaveRoundNotes(GuardRoundLogs data)
        {
            try
            {
                repository.SaveNotes(data);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
        }

        [HttpGet]
        [Route("Rounds")]
        public IHttpActionResult GetRoundLogs(int guardRoundId, int guardId, DateTime date)
        {
            try
            {
                return Ok(repository.GetRoundsByGuardId(guardRoundId, guardId, date));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
        }

        [HttpGet]
        [Route("Rounds/Active")]
        public IHttpActionResult GetActiveRoundsByDate(DateTime start, DateTime end, int siteId, int accountId)
        {
            try
            {
                return Ok(repository.GetActiveRoundsByDate(start, end.AddDays(1), siteId, accountId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
        }

        [HttpGet]
        [Route("GuardRound/Qr")]
        [AllowAnonymous]
        public HttpResponseMessage GenerateQrCodeImage(int roundId)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode($"{roundId}", QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            var qr = qrCode.GetGraphic(10);

            using (MemoryStream stream = new MemoryStream())
            {
                qr.Save(stream, ImageFormat.Jpeg);

                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new ByteArrayContent(stream.ToArray());
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");

                return response;
            }
        }
    }
}