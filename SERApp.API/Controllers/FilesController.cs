﻿using SERApp.Service.Services;
using System.Threading.Tasks;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class FilesController : ApiController
    {
        [HttpGet]
        [Route("Files/{accountId}")]
        public async Task<IHttpActionResult> GetFiles(int accountId)
        {
            var service = new FileManagerService();
            var aa = await service.GetFilesAndFolders(accountId);
            return Ok(aa);
        }
    }
}