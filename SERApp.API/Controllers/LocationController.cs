﻿using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    //[Authorize]
    public class LocationController: ApiController
    {
        private readonly ILocationRepository _repository;
        public LocationController()
        {
            _repository = new LocationRepository();
        }

        [HttpGet]
        [Route("Location/Building")]
        public IHttpActionResult GetBuildings(int accountId)
        {
            return Ok(_repository.GetBuildings(accountId));
        }

        [HttpGet]
        [Route("Location/Level")]
        public IHttpActionResult GetLevels(int accountId)
        {
            return Ok(_repository.GetLevels(accountId));
        }

        [HttpGet]
        [Route("Location/Room")]
        public IHttpActionResult GetRooms(int accountId)
        {
            return Ok(_repository.GetRooms(accountId));
        }
        [HttpDelete]
        [Route("Location/Building")]
        public IHttpActionResult DeleteBuilding(int id)
        {
            _repository.DeleteBuilding(id);
            return Ok();
        }

        [HttpDelete]
        [Route("Location/Level")]
        public IHttpActionResult DeleteLevel(int id)
        {
            _repository.DeleteLevel(id);
            return Ok();
        }

        [HttpDelete]
        [Route("Location/Room")]
        public IHttpActionResult DeleteRoom(int id)
        {
            _repository.DeleteRoom(id);
            return Ok();
        }

        //[HttpGet]
        //[Route("Location/Building")]
        //public IHttpActionResult GetBuildings(int accountId)
        //{
        //    return Ok(_repository.GetBuildings(accountId));
        //}

        //[HttpGet]
        //[Route("Location/Level")]
        //public IHttpActionResult GetLevels(int accountId)
        //{
        //    return Ok(_repository.GetLevels(accountId));
        //}

        //[HttpGet]
        //[Route("Location/Room")]
        //public IHttpActionResult GetRooms(int accountId)
        //{
        //    return Ok(_repository.GetRooms(accountId));
        //}
    }
}