﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class DepartmentController : ApiController
    {
        private DepartmentService _departmentService;
        public DepartmentController()
        {
            _departmentService = new DepartmentService();
        }

        [HttpGet]
        [Route("Department/Get")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<DepartmentModel>))]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _departmentService.Get(id);
                return new ResponseDataModel<DepartmentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Department Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Department/GetAllByAccountId")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<DepartmentModel>>))]
        public IHttpActionResult GetAllByAccountId(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _departmentService.GetAllByAccountId(accountId);
                return new ResponseDataModel<List<DepartmentModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Department Data",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Department/Save")]
        [Authorize]
        public IHttpActionResult SaveAccountModule(DepartmentModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _departmentService.SaveDepartment(model);
                return new ResponseDataModel<DepartmentModel>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value ? "Successfully Saved Department Record" : "Department name already exists",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Department/Delete")]
        [Authorize]
        public IHttpActionResult DeleteAccountModule(DepartmentModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _departmentService.DeleteDepartment(model.Id);
                return new ResponseDataModel<DepartmentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Department Record"
                };
            }));
        }

    }
}
