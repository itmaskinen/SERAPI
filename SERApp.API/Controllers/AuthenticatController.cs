﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class AuthenticatController : ApiController
    {
        private AuthenticationService _authService;
        private UserService _userService;
        private SiteRoleAuthenticationService _siteRoleAuthService;
        public AuthenticatController()
        {
            _authService = new AuthenticationService();
            _siteRoleAuthService = new SiteRoleAuthenticationService();
            _userService = new UserService();
        }

        [HttpGet]
        [Route("Authentication/GetToken")]
        public Object GetToken()
        {
            string key = "ser4secretkey5512"; //Secret key which will be used later during validation    
            var issuer = "https://ser4api.azurewebsites.net/";  //normally this will be your site URL    

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));


            //Create Security Token object by giving required parameters    
            var token = new JwtSecurityToken(issuer, //Issure    
                            issuer,  //Audience    
                            permClaims,
                            expires: DateTime.Now.AddDays(30),
                            signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);
            return new { data = jwt_token };
        }

        [HttpPost]
        [Route("Authentication/Login")]
        public IHttpActionResult Login(AuthRequestModel model)
        {
            try
            {
                var data = _authService.Authenticate(model.UserName, model.Password);
                if (data != null)
                {
                    var permClaims = new List<Claim>();
                    permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
                    data.Token = GetTokenString(permClaims, DateTime.Now.AddDays(30));
                }

                return Ok(new
                {
                    Success = data.IsAuthenticated,
                    Message = data.AuthResponseMessage,
                    Data = data
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        private string GetTokenString(List<Claim> claims, DateTime expiryDate)
        {
            string key = "ser4secretkey5512"; //Secret key which will be used later during validation    
            var issuer = "https://ser4api.azurewebsites.net/";  //normally this will be your site URL    

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create Security Token object by giving required parameters    
            var token = new JwtSecurityToken(issuer, //Issure    
                            issuer,  //Audience    
                            claims,
                            expires: expiryDate,
                            signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt_token;
        }

        [HttpPost]
        [Route("Authentication/Logoff")]
        public IHttpActionResult Logoff(int userId)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _authService.LogOff(userId);
                return new ResponseDataModel<AuthResponseModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = data.AuthResponseMessage,
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Authentication/ForgotPassword")]
        public IHttpActionResult ForgotPassword(AuthRequestModel model)
        {
            try
            {
                var claim = new List<Claim>()
                {
                    new Claim(JwtRegisteredClaimNames.Jti, model.EmailAddress)
                };
                var token = GetTokenString(claim, DateTime.Now.AddMinutes(30));
                _authService.SendResetPassword(token, model.EmailAddress);

                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
        }

        [HttpPost]
        [Route("Authentication/DecodeToken")]
        public IHttpActionResult DecodeToken(AuthResponseModel model)
        {
            try
            {
                string key = "ser4secretkey5512"; //Secret key which will be used later during validation    
                var issuer = "https://ser4api.azurewebsites.net/";  //normally this will be your site URL   

                var tokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = issuer,       // Replace with the issuer from your JWT
                    ValidAudience = issuer,   // Replace with the audience from your JWT
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)),
                };

                // Create a JWT token handler
                var tokenHandler = new JwtSecurityTokenHandler();

                // Validate and decode the JWT token
                SecurityToken validatedToken;
                var claimsPrincipal = tokenHandler.ValidateToken(model.Token, tokenValidationParameters, out validatedToken);

                return Ok(claimsPrincipal.Claims.FirstOrDefault(e => e.Type == JwtRegisteredClaimNames.Jti).Value);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
        }

        [HttpGet]
        [Route("Authentication/GetRole")]
        [Authorize]
        public IHttpActionResult GetRole(int userId, int moduleId, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _siteRoleAuthService.GetRole(userId, moduleId, siteId);
                return new ResponseDataModel<RoleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Role",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Authentication/GetRoleByModule")]
        [Authorize]
        public IHttpActionResult GetRoleByModule(int userId, int moduleId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _siteRoleAuthService.GetRolesByModule(userId, moduleId);
                return new ResponseDataModel<List<UserModuleRoleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Role",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Authentication/GetUsers")]
        [Authorize]
        public IHttpActionResult GetUsers(int moduleId, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var se = new CultureInfo("sv-SE");
                var data = _siteRoleAuthService.GetUsers(moduleId, siteId).OrderBy(x => x.User.FirstName, StringComparer.Create(se, false)).ThenBy(x => x.User.LastName, StringComparer.Create(se, false)).ToList();
                return new ResponseDataModel<List<UserModuleRoleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Role",
                    Data = data
                };
            }));
        }
    }
}
