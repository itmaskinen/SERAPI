﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System.Net;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IssueController : ApiController
    {
        private readonly IIssueService _service;
        public IssueController()
        {
            _service = new IssueService();
        }

        [HttpPost]
        [Route("Issue/Save")]
        [Authorize]
        public IHttpActionResult Get(IssueModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                model.IsSuccess = _service.SaveIssue(model);
                return new ResponseDataModel<IssueModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved and Sent Issue",
                    Data = model
                };
            }));
        }
    }
}
