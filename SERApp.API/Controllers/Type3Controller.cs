﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class Type3Controller : ApiController
    {
        private IType3Service _service;
        public Type3Controller()
        {
            _service = new Type3Service();
        }

        [HttpGet]
        [Route("Type3/Get")]
        [Authorize]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetType3(id);
                return new ResponseDataModel<Type3Model>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Type3",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Type3/GetAll")]
        [Authorize]
        public IHttpActionResult GetAll(int accountId, int siteId, bool isActiveOnly = true)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetAllType3(accountId, siteId, isActiveOnly);
                return new ResponseDataModel<IEnumerable<Type3Model>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Type3",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Type3/Save")]
        [Authorize]
        public IHttpActionResult Save([FromBody] Type3Model model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _service.AddType3(model);
                return new ResponseDataModel<Type3Model>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value ? "Successfully Save Type3" : "Type 3 already exists",
                    Data = model
                };
            }));
        }


        [HttpDelete]
        [Route("Type3/Delete")]
        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var success = _service.DeleteType3(id);
                var message = success ? "Successfully Deleted Type3" : "Cannot delete Type3 because it is associated to a daily report.";
                return new ResponseDataModel<Type3Model>
                {
                    Success = success,
                    StatusCode = HttpStatusCode.OK,
                    Message = message,
                    Data = null
                };
            }));
        }

        [HttpPost]
        [Route("Type3/IsActive")]
        public IHttpActionResult ToggleIsActive(int id)
        {
            try
            {
                return Ok(_service.ToggleIsActive(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
        }

    }
}
