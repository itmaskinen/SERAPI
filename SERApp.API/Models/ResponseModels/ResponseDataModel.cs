﻿namespace SERApp.API.Models.ResponseModels
{
    public class ResponseDataModel<T> : ResponseModel where T : class
    {
        /// <summary>
        /// Contains the returned property of type T.
        /// </summary>
        public T Data { get; set; }
        public T result { get; set; }
    }
}
