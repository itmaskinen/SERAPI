﻿using System;
using System.Net;

namespace SERApp.API.Models.ResponseModels
{
    public class ResponseModel
    {
        /// <summary>
        /// The success integer code.
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// The Error integer code.
        /// </summary>
        public int ErrorCode { get; set; }

        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// The response string returned.
        /// </summary>
        public string Message { get; set; }

        public object Extras { get; set; }

        public string Token { get; set; }

        public DateTime TokenExpire { get; set; }

        public int TotalRecords { get; set; }
    }
}
