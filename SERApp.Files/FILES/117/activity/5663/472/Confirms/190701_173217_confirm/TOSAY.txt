I am Carlos J Villanueva III I have been working as a software/web 
developer for over 5 years now.

My first professional experience was working as a junior asp.net web
developer on Xhimera Industrial and Commercial Services Inc. I worked
on a project for a local School in Cebu City. The project was called
atenenoepost.com it is basically a system for managing school events,
student/teachers attendances and a tool for communication between 
parents,students and teachers. It was developed using asp.net mvc 3 and
MS SQL for the db. during my work there, I also had worked on another
project which is called energymetering.asia. It is a project for 
storing, reporting and monitoring data for energy metered devices that 
the company was also selling. It was also developed using asp.net mvc3
and MS SQL Server.

My neext professional experience was working in Gemango Software Services
From there, I was a part of a team that works with a project called 
mccPILOTLOG. It is basically an electronic logbook for pilots which 
has lots of useful features. One of it was syncing historicaldata 
 flights which are actually stored from different airline websites
using Selenium to mine data. The project was built using mono framework and SQLite for its db.

Next professional experience was working as a homebased web developer
in Lemontech. It was a startup company which had a client from the US
specifically Supplypro. So I was working as an offshore developer for
that company. The project I worked on was called SupplyPort.com. It 
is a web based inventory monitoring system, CRM and POS 
for hardware devices used for rockets. One of the clients of supplypro
was SpaceX. So it was fun working with the project. The project
was built using asp.net mvc and MS SQL for the database. I was part of a team
and we had daily scrum,sprint planning and retrospect

Next projects that I worked on as a Homebased web developer on Crimcheck.com was 
CrimeAssure, ClientsPortal and ApplicantLink. These projects is 
actually a single system which was divided into three different projects.
It is a web application which does background checks on applicants/people
It does Criminal Check, civil litigation,Sex offender,employment verification,
education verification,Federal Criminal, professional reference, MVR,drug screening,
and a lot more to say. It is also a partly CRM and POS system. It was built using Angularjs for 
the frontend, asp.net web api for the backend and mysql for the database. Lots of 
api integrations was also worked on this project.. In this project I was part of a team
we do daily scrum meeting, occasional mob programming, pair programming, code review and sprint planning.

The current project that I am working on is called SER4. I am working as a homebased
full stack web developer for a swedish company called ITmaskinen. The project is a web application property management
system with CRM. It is built using Angular 6 and asp.net web api. and MS SQL. In this project I do the development, 
management, debugging and deployment of the project.
