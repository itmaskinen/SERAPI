﻿using SERApp.Data.Models;

namespace SERApp.Data
{
    public class BimControlItemImage
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int BimControlItemId { get; set; }
        public virtual BimControlItem BimControlItem { get; set; }
    }
}
