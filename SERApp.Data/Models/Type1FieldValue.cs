﻿namespace SERApp.Data.Models
{
    public class Type1FieldValue
    {
        public int Id { get; set; }
        public int IncidentId { get; set; }
        public int Type1_FieldId { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
    }
}
