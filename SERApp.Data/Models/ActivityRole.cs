﻿namespace SERApp.Data.Models
{
    public class ActivityRole
    {
        public int ActivityRoleId { get; set; }
        public string Name { get; set; }
        public int AccountId { get; set; }
    }
}
