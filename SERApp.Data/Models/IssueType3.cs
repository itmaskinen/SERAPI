﻿namespace SERApp.Data.Models
{
    public class IssueType3
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public bool Value { get; set; }
        public int AccountId { get; set; }
    }
}
