﻿namespace SERApp.Data.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SiteId { get; set; }

        public Site Site { get; set; }
    }
}
