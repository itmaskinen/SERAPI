﻿namespace SERApp.Data.Models
{
    public class Interval
    {
        public int IntervalId { get; set; }
        public string Name { get; set; }
        public int IntervalType { get; set; }
        public int Value { get; set; }

        public int AccountId { get; set; }
    }
}
