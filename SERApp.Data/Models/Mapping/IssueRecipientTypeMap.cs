﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class IssueRecipientTypeMap : EntityTypeConfiguration<IssueRecipientTypes>
    {
        public IssueRecipientTypeMap()
        {
            this.HasKey(t => t.IssueRecipientTypeId);

            this.ToTable("IssueRecipientTypes");
            this.Property(t => t.IssueRecipientTypeId).HasColumnName("IssueRecipientTypeId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Hidden).HasColumnName("Hidden");

        }
    }
}
