﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class Type1SettingMap : EntityTypeConfiguration<Type1Setting>
    {
        public Type1SettingMap()
        {
            this.HasKey(e => e.Id);
            this.ToTable("Type1Setting");
            this.Property(t => t.Type1Id).HasColumnName("Type1Id");
            this.Property(t => t.SiteId).HasColumnName("SiteId");
            this.Property(t => t.IsActive).HasColumnName("IsActive");

            this.HasRequired(t => t.Type1)
                .WithMany(e => e.Settings)
                .HasForeignKey(e => e.Type1Id);
        }
    }
}
