﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class LevelMap: EntityTypeConfiguration<Level>
    {
        public LevelMap()
        {
            this.ToTable("Level");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.BuildingId).HasColumnName("BuildingId");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
        }
    }
}
