﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class IncidentTypeMap : EntityTypeConfiguration<IncidentType>
    {
        public IncidentTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("IncidentTypes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.IsReportRequired).HasColumnName("IsReportRequired");
            this.Property(t => t.RequiredReport).HasColumnName("RequiredReport");
            this.Property(t => t.ShownReport).HasColumnName("ShownReport");

        }
    }
}
