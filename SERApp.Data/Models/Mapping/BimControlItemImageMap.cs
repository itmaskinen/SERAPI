﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class BimControlItemImageMap : EntityTypeConfiguration<BimControlItemImage>
    {
        public BimControlItemImageMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("BimControlItemImage");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Url).HasColumnName("Url");

            //this.HasRequired(t => t.BimControlItem).WithMany(x=>x.Photos).HasForeignKey(x => x.BimControlItemId);

        }
    }
}
