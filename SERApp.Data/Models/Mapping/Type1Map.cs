﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class Type1Map : EntityTypeConfiguration<Type1>
    {
        public Type1Map()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("Type1");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.IsReportRequired).HasColumnName("IsReportRequired");
            this.Property(t => t.RequiredReport).HasColumnName("RequiredReport");
            this.Property(t => t.ShownReport).HasColumnName("ShownReport");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.SiteId).HasColumnName("SiteId");
        }
    }
}
