﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class SubPremiseTypeMap : EntityTypeConfiguration<SubPremiseType>
    {
        public SubPremiseTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("SubPremiseTypes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.PremiseTypeId).HasColumnName("PremiseTypeId");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
        }
    }
}
