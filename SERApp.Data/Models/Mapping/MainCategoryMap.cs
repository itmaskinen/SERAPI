﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class MainCategoryMap : EntityTypeConfiguration<MainCategory>
    {
        public MainCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("MainCategory");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
        }
    }
}
