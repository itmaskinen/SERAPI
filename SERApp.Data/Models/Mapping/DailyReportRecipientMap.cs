﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class DailyReportRecipientMap : EntityTypeConfiguration<DailyReportRecipient>
    {
        public DailyReportRecipientMap()
        {
            // Primary Key
            this.HasKey(t => t.RecipientId);


            // Table & Column Mappings
            this.ToTable("DailyReportRecipients");
            this.Property(t => t.RecipientId).HasColumnName("RecipientId");
            this.Property(t => t.SiteId).HasColumnName("SiteId");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.ReportType).HasColumnName("ReportType");
            this.Property(t => t.Email).HasColumnName("Email");
        }
    }
}
