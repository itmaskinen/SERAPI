﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class IntervalMap : EntityTypeConfiguration<Interval>
    {
        public IntervalMap()
        {
            // Primary Key
            this.HasKey(t => t.IntervalId);

            // Properties
            // Table & Column Mappings
            this.ToTable("Interval");
            this.Property(t => t.IntervalId).HasColumnName("IntervalId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.IntervalType).HasColumnName("IntervalType");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
        }
    }
}
