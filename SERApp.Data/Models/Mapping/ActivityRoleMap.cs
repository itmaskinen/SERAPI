﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class ActivityRoleMap : EntityTypeConfiguration<ActivityRole>
    {
        public ActivityRoleMap()
        {
            // Primary Key
            this.HasKey(t => t.ActivityRoleId);

            // Table & Column Mappings
            this.ToTable("ActivityRole");
            this.Property(t => t.ActivityRoleId).HasColumnName("ActivityRoleId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.AccountId).HasColumnName("AccountId");

        }
    }
}
