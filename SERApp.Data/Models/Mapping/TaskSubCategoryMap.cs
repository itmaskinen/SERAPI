﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class TaskSubCategoryMap : EntityTypeConfiguration<TaskSubCategory>
    {
        public TaskSubCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("TaskSubCategory");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SubCategoryId).HasColumnName("SubCategoryId");
            this.Property(t => t.TaskId).HasColumnName("TaskId");


            // Relationships
            this.HasRequired(t => t.Task)
                .WithMany(t => t.TaskSubCategories)
                .HasForeignKey(d => d.TaskId);

            this.HasRequired(t => t.SubCategory)
                .WithMany(t => t.TaskSubCategories)
                .HasForeignKey(d => d.SubCategoryId);

        }
    }
}
