﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class ObjectGuardRoundMap : EntityTypeConfiguration<ObjectGuardRound>
    {
        public ObjectGuardRoundMap()
        {
            this.ToTable("ObjectGuardRound");
            this.HasKey(e => e.Id);

            this.Property(t => t.ObjectId).HasColumnName("ObjectId");
            this.Property(t => t.GuardRoundId).HasColumnName("GuardRoundId");

            this.HasRequired(t => t.Object)
                .WithMany(t => t.ObjectGuardRounds)
                .HasForeignKey(t => t.ObjectId);
        }
    }
}
