﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models
{
    public class GuardRoundsMap : EntityTypeConfiguration<GuardRounds>
    {
        public GuardRoundsMap()
        {
            this.HasKey(e => e.Id);
            this.ToTable("GuardRounds");
            this.Property(e => e.Name).HasColumnName("Name");
            this.Property(e => e.Description).HasColumnName("Description");
            this.Property(e => e.SiteId).HasColumnName("SiteId");
        }
    }
}
