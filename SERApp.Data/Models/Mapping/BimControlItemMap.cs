﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class BimControlItemMap : EntityTypeConfiguration<BimControlItem>
    {
        public BimControlItemMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("BimControlItem");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SiteTaskLogId).HasColumnName("SiteTaskLogId");
            this.Property(t => t.Date).HasColumnName("Date");

            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.IssueId).HasColumnName("IssueId");

            this.Property(t => t.LittraId).HasColumnName("LittraId");
            this.Property(t => t.ErrorDescription).HasColumnName("ErrorDescription");

            //this.HasOptional(t => t.Issue);
            //this.HasRequired(t => t.SiteTaskLog).WithMany(x=>x.Items).HasForeignKey(x=>x.SiteTaskLogId);
        }
    }
}
