﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class BuildingMap: EntityTypeConfiguration<Building>
    {
        public BuildingMap()
        {
            this.ToTable("Building");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
        }
    }
}
