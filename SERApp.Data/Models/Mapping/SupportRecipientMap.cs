﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class SupportRecipientMap : EntityTypeConfiguration<SupportRecipient>
    {
        public SupportRecipientMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("SupportRecipient");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.AccountId).HasColumnName("AccountId");

        }
    }
}
