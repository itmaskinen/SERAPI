﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class SummaryMap : EntityTypeConfiguration<Summary>
    {
        public SummaryMap()
        {
            // Primary Key
            this.HasKey(t => t.SummaryId);

            // Properties
            this.Property(t => t.SummaryValue)
                .HasMaxLength(3000);

            this.Property(t => t.Date);

            // Table & Column Mappings
            this.ToTable("Summary");
            this.Property(t => t.SummaryId).HasColumnName("SummaryId");
            this.Property(t => t.SummaryValue).HasColumnName("SummaryValue");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.SiteId).HasColumnName("SiteId");
        }
    }
}
