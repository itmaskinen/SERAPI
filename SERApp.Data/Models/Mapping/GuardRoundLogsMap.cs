﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class GuardRoundLogsMap: EntityTypeConfiguration<GuardRoundLogs>
    {
        public GuardRoundLogsMap()
        {
            this.HasKey(x => x.Id);

            this.ToTable("GuardRoundLogs");

            this.HasRequired(x => x.GuardRound)
                .WithMany(x => x.GuardRoundLogs)
                .HasForeignKey(x => x.GuardRoundId);
        }
    }
}
