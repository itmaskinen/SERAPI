﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class RoomMap: EntityTypeConfiguration<Room>
    {
        public RoomMap()
        {
            this.ToTable("Room");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.LevelId).HasColumnName("LevelId");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
        }
    }
}
