﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class IssueRecipientMap : EntityTypeConfiguration<IssueRecipients>
    {
        public IssueRecipientMap()
        {
            this.HasKey(t => t.IssueRecipientId);

            this.ToTable("IssueRecipients");
            this.Property(t => t.IssueRecipientId).HasColumnName("IssueRecipientId");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Mobile).HasColumnName("Mobile");
            this.Property(t => t.IssueRecipientTypeId).HasColumnName("IssueRecipientTypeId");
            this.Property(t => t.SiteId).HasColumnName("SiteId");

            this.HasRequired(x => x.IssueRecipientType);
            this.HasRequired(x => x.Site);
        }
    }
}
