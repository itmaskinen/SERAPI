﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class IssueType2Map : EntityTypeConfiguration<IssueType2>
    {
        public IssueType2Map()
        {
            // Primary Key
            this.HasKey(t => t.IssueType2Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("IssueType2");
            this.Property(t => t.IssueType2Id).HasColumnName("IssueType2Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.AccountId).HasColumnName("AccountId");

        }
    }
}
