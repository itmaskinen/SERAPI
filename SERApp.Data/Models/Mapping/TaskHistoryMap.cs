﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{


    public class TaskHistoryMap : EntityTypeConfiguration<TaskHistory>
    {
        public TaskHistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("TaskHistory");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TaskId).HasColumnName("TaskId");
            this.Property(t => t.Actions).HasColumnName("Actions");
            this.Property(t => t.LogDateTime).HasColumnName("LogDateTime");
            this.Property(t => t.UserId).HasColumnName("UserId");
        }
    }
}
