﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class TagMap : EntityTypeConfiguration<Tag>
    {
        public TagMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("Tags");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RFIDData).HasColumnName("RFIDData");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.TenantID).HasColumnName("TenantID");
        }
    }
}
