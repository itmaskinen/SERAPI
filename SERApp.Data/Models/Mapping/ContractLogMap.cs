﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{

    public class ContractLogMap : EntityTypeConfiguration<ContractLog>
    {
        public ContractLogMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("ContractLog");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ContractId).HasColumnName("ContractId");
            this.Property(t => t.LogDateTime).HasColumnName("LogDateTime");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.UserId).HasColumnName("UserId");
        }
    }
}
