﻿namespace SERApp.Data.Models
{
    public class IssueType1
    {
        public int IssueType1Id { get; set; }
        public string Name { get; set; }

        public int AccountId { get; set; }
    }
}
