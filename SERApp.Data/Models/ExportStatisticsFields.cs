﻿namespace SERApp.Data.Models
{
    public class ExportStatisticsFields
    {
        public int Id { get; set; }
        public int Category { get; set; }
        public string Value { get; set; }
        public int SiteId { get; set; }
    }
}
