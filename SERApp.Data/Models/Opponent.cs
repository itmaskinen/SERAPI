﻿namespace SERApp.Data.Models
{
    public class Opponent
    {
        public int Id { get; set; }
        public string OpponentHolderName { get; set; }
        public int AccountId { get; set; }

        public Account Account { get; set; }
    }
}
