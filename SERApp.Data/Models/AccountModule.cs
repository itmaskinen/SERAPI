using System;

namespace SERApp.Data.Models
{
    public partial class AccountModule
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int ModuleId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public virtual Account Account { get; set; }
        public virtual Module Module { get; set; }
    }
}
