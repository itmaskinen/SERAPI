﻿namespace SERApp.Data.Models
{
    public class Setting
    {
        public int SettingId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int AccountId { get; set; }
        public int ConfigSettingId { get; set; }
        public int SiteId { get; set; }
        public ConfigSetting ConfigSetting { get; set; }
    }
}
