﻿namespace SERApp.Data.Models
{
    public class IncidentGuard
    {
        public int Id { get; set; }
        public int IncidentId { get; set; }
        public virtual Incident Incident { get; set; }

        public int GuardId { get; set; }
        public virtual Guard Guard { get; set; }
    }
}
