﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class GuardRoundLogs
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int GuardId { get; set; }
        public int UserId { get; set; }
        public string Notes { get; set; }
        public int AccountId { get; set; }
        public int GuardRoundId { get; set; }
        public GuardRounds GuardRound { get; set; }
    }
}
