﻿namespace SERApp.Data.Models
{
    public class TaskSubCategory
    {
        public int Id { get; set; }
        public int SubCategoryId { get; set; }
        public int TaskId { get; set; }
        public virtual Task Task { get; set; }
        public virtual SubCategory SubCategory { get; set; }

    }
}
