﻿using System;

namespace SERApp.Data.Models
{
    public class TaskHistory
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public string Actions { get; set; }
        public DateTime LogDateTime { get; set; }
        public int UserId { get; set; }
    }
}
