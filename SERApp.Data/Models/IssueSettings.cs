﻿namespace SERApp.Data.Models
{
    public class IssueSettings
    {
        public int IssueSettingId { get; set; }
        public int StatusInterval { get; set; }
        public int AccountId { get; set; }
    }
}
