﻿namespace SERApp.Data.Models
{
    public class ContractCategory
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
        public bool IsActive { get; set; }
        public int GroupId { get; set; }
    }
}
