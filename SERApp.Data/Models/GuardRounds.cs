﻿using System;
using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public class GuardRounds
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SiteId { get; set; }
        public string Description { get; set; }
        public string StartMessage { get; set; }
        public string StopMessage { get; set; }
        public DateTime CreatedDate { get; set; } 
        public DateTime? UpdatedDate { get; set; } 
        public ICollection<GuardRoundLogs> GuardRoundLogs { get; set; }
    }
}
