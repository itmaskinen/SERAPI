﻿namespace SERApp.Data.Models
{
    public class IssueRecipientTypes
    {
        public int IssueRecipientTypeId { get; set; }
        public string Name { get; set; }
        public bool Hidden { get; set; }
    }
}
