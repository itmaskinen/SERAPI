﻿using System;

namespace SERApp.Data.Models
{
    public class Type4
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public int AccountId { get; set; }
        public int SiteId { get; set; }
        public bool IsActive { get; set; }
    }
}
