﻿namespace SERApp.Data.Models
{
    public class ContractRole
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public int AccountId { get; set; }
    }
}
