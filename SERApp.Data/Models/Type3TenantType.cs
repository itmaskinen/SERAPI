﻿namespace SERApp.Data.Models
{
    public class Type3TenantType
    {
        public int Id { get; set; }
        public int Type3Id { get; set; }
        public int TenantTypeId { get; set; }

        public Type3 Type3 { get; set; }
        public TenantType TenantType { get; set; }
    }
}