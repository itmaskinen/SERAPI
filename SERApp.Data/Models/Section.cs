﻿using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public class Section
    {
        public int SectionId { get; set; }
        public string SectionValue { get; set; }
        public int SiteId { get; set; }
        //public int RyckrapportDataId { get; set; }

        public virtual ICollection<RyckrapportDataSection> RyckrapportDataSections { get; set; }
    }
}
