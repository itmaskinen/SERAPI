﻿namespace SERApp.Data.Models
{
    public class TagWord
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AccountId { get; set; }
    }
}
