﻿namespace SERApp.Data.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string RFIDData { get; set; }
        public string Description { get; set; }

        public int TenantID { get; set; }
        public virtual Tenant Tenant { get; set; }
    }
}
