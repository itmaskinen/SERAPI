﻿using System;

namespace SERApp.Data.Models
{
    public class Log
    {
        public int Id { get; set; }
        public int LogType { get; set; }
        public string ShortDescription { get; set; }
        public string Message { get; set; }
        public int AccountId { get; set; }
        public int? SiteId { get; set; }
        public string Module { get; set; }
        public string ReportType { get; set; }
        public int UserId { get; set; }
        public string Recepients { get; set; }
        public DateTime CreatedDate { get; set; }
        public Site Site { get; set; }
    }
}
