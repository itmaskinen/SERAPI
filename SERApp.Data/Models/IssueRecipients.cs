﻿namespace SERApp.Data.Models
{
    public class IssueRecipients
    {
        public int IssueRecipientId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }

        public int IssueRecipientTypeId { get; set; }
        public int SiteId { get; set; }

        public virtual IssueRecipientTypes IssueRecipientType { get; set; }
        public virtual Site Site { get; set; }
    }
}
