﻿namespace SERApp.Data.Models
{
    public class IssueType2
    {
        public int IssueType2Id { get; set; }
        public string Name { get; set; }

        public int AccountId { get; set; }
    }
}
