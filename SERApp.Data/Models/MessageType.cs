﻿namespace SERApp.Data.Models
{
    public partial class MessageType
    {
        public int Id { get; set; }
        public string ConfirmationName { get; set; }
        public string Description { get; set; }
        public bool? IsUsed { get; set; }
    }
}
