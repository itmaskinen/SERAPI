﻿namespace SERApp.Data.Models
{
    public class ObjectGuardRound
    {
        public int Id { get; set; }
        public int ObjectId { get; set; }
        public int GuardRoundId { get; set; }
        public Tenant Object { get; set; }
    }
}
