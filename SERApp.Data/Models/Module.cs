using System;
using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public partial class Module
    {
        public Module()
        {
            this.AccountModules = new List<AccountModule>();
            this.UserModules = new List<UserModuleRoles>();
        }

        public int Id { get; set; }
        public string ShortName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string NodeLevel { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDefault { get; set; }
        public virtual ICollection<AccountModule> AccountModules { get; set; }
        public virtual ICollection<UserModuleRoles> UserModules { get; set; }
    }
}
