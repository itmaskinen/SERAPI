﻿namespace SERApp.Data.Models
{
    public class Type1Setting
    {
        public int Id { get; set; }
        public int Type1Id { get; set; }
        public int SiteId { get; set; }
        public bool IsActive { get; set; }
        public Type1 Type1 { get; set; }
    }
}
