﻿namespace SERApp.Data.Models
{
    public class CompanyType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
