﻿namespace SERApp.Data.Models
{
    public class TaskMainCategory
    {
        public int Id { get; set; }
        public int MainCategoryId { get; set; }
        public int TaskId { get; set; }

        public virtual MainCategory MainCategory { get; set; }
        public virtual Task Task { get; set; }


    }
}
