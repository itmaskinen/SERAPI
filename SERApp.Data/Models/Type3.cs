﻿using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public class Type3
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsTenantRequired { get; set; }
        public int TenantTypeId { get; set; }
        public int AccountId { get; set; }
        public int SiteId { get; set; }
        public bool IsActive { get; set; }

        public ICollection<Type3TenantType> Type3TenantTypes { get; set; }
    }
}
