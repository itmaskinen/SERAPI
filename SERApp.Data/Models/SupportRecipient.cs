﻿namespace SERApp.Data.Models
{
    public class SupportRecipient
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public int AccountId { get; set; }
    }
}
