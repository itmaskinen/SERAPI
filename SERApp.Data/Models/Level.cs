﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class Level
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; } = true;
        public int BuildingId { get; set; }
        public int AccountId { get; set; }
        public int SiteId { get; set; }
        public ICollection<Tenant> Tenants { get; set; }
    }
}
