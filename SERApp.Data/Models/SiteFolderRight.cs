﻿namespace SERApp.Data.Models
{
    public class SiteFolderRight
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string Folder { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
