﻿namespace SERApp.Data.Models
{
    public class TenantContact
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public int ContactId { get; set; }
        public int JobTypeId { get; set; }
    }
}
