﻿using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public class Type1
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsReportRequired { get; set; }

        public string RequiredReport { get; set; }
        public string ShownReport { get; set; }
        public int AccountId { get; set; }
        public int SiteId { get; set; }
        public ICollection<Type1Setting> Settings { get; set; }
    }
}
