﻿using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public class MainCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<TaskMainCategory> TaskMainCategories { get; set; }
        public int AccountId { get; set; }
    }
}
