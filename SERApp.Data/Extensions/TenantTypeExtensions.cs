﻿using Newtonsoft.Json;
using SERApp.Data.Models;
using SERApp.Models;

namespace SERApp.Data.Extensions
{
    public static class TenantTypeExtensions
    {
        public static TenantTypeModel ToModel(this TenantType entity)
        {
            return new TenantTypeModel
            {
                Id = entity.Id,
                AccountId = entity.AccountId,
                TypeName = entity.TypeName,
                Description = entity.Description,
                PageSetting = string.IsNullOrEmpty(entity.PageSetting) ? null: JsonConvert.DeserializeObject<dynamic>(entity.PageSetting),
                DefaultGuardRoundId = entity.DefaultGuardRoundId,
            };
        }
    }
}