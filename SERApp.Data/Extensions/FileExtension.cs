﻿using System;
using System.IO;

namespace SERApp.Data.Extensions
{
    public static class FileExtension
    {
        public static string ConvertToBase64(this FileStream stream)
        {
            byte[] bytes;
            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                bytes = memoryStream.ToArray();
            }

            return Convert.ToBase64String(bytes);
        }
    }
}
