﻿using Newtonsoft.Json;
using SERApp.Data.Models;
using SERApp.Models;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace SERApp.Data.Extensions
{
    public static class TenantExtensions
    {
        public static TenantModel ToModel(this Tenant entity, User lastUpdatedBy = null, GuardRounds guardRounds = null)
        {
            var pageSetting = string.IsNullOrEmpty(entity?.TenantTypeEnt?.PageSetting ?? "") ? null : JsonConvert.DeserializeObject<dynamic>(entity.TenantTypeEnt?.PageSetting);

            var hasEmailField = true;
            if (pageSetting != null)
            {
                var dictionary = JsonConvert.DeserializeObject<IDictionary<string, object>>(entity.TenantTypeEnt?.PageSetting);
                if (dictionary.ContainsKey("email") && !bool.TryParse(dictionary["email"].ToString(), out hasEmailField))
                {
                    var email = JsonConvert.DeserializeObject<IDictionary<string, object>>(dictionary["email"].ToString());
                    if (email.ContainsKey("isVisible") && bool.TryParse(email.ContainsKey("isVisible").ToString(), out bool isVisible))
                    {
                        hasEmailField = isVisible;
                    }
                }
            }

            return new TenantModel
            {
                Id = entity.Id,
                Site = entity.Site.ToModel(),
                SiteId = entity.SiteId,
                SiteName = entity.Site?.Name,
                SiteManagerName = entity.SiteManagerName,
                SiteManagerMobilePhone = entity.SiteManagerPhone,
                SiteManagerPhone = entity.SiteManagerPhone,
                SiteManagerEmail = entity.SiteManagerEmail,
                ImageUrl = entity.Site?.CompanyLogo ?? string.Empty,
                Name = entity.Name,
                Email = entity.Email,
                EmailTemplate = entity.EmailTemplate,
                Image = entity.Image ?? string.Empty,
                Phone = entity.Phone,
                VisitingAddress = entity.VisitingAddress,
                UserName = entity.UserName,
                Password = entity.Password,
                InsuranceCompany = entity.InsuranceCompany,
                SecurityCompany = entity.SecurityCompany,
                ContractName = entity.ContractName,
                ContractVatNo = entity.ContractVatNo,
                InvoiceAddress = entity.InvoiceAddress,
                InvoiceZipAndCity = entity.InvoiceZipAndCity,
                ContactComments = entity.ContactComments,
                CCTV = entity.CCTV,
                IntrusionAlarm = entity.IntrusionAlarm,
                LastUpdatedDate = entity.LastUpdatedDate,
                CreatedDate = entity.CreatedDate,
                LastSentUpdateEmail = entity.LastSentUpdateEmail,
                Flag = entity.Flag,
                IsActive = entity.IsActive,
                PropertyDesignation = entity.PropertyDesignation,
                UseWrittenStatement = entity.UseWrittenStatement,
                TenantType = entity.TenantType ?? 0,
                TenantTypeName = entity.TenantTypeEnt?.TypeName ?? "N / A",
                PermiseType = entity.PermiseType ?? 0,
                PremiseTypeName = entity.PremiseTypeEnt?.TypeName ?? "N / A",
                SubPremiseType = entity.SubPremiseType ?? 0,
                SubPremiseTypeName = entity.SubPremiseTypeEnt?.TypeName ?? "N / A",
                City = entity.City,
                ZipCode = entity.ZipCode,
                PostAddress = entity.PostAddress,
                PostCity = entity.PostCity,
                PostZipCode = entity.PostZipCode,
                LastUpdatedBy = entity.LastUpdatedBy,
                IsPopup = entity.TenantTypeEnt?.IsPopupMessage ?? false,
                PageSetting = pageSetting,//string.IsNullOrEmpty(entity?.TenantTypeEnt?.PageSetting ?? "") ? null : JsonConvert.DeserializeObject<dynamic>(entity.TenantTypeEnt?.PageSetting),
                HasEmailField = hasEmailField,
                LastUpdatedByName = lastUpdatedBy == null ? "" : $"{lastUpdatedBy?.FirstName} {lastUpdatedBy?.LastName}",
                EnableNFC = entity.EnableNFC,
                IsNFCReady = entity.IsNFCReady,
                NFCTag = entity.Id.NFCTag(),
                Building = entity.Building?.Name,
                Level = entity.Level?.Name,
                Room = entity.Room?.Name,
            };
        }
        public static string NFCTag(this int value)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] Key = { 12, 13, 14, 15, 16, 17, 18, 19 };
            byte[] IV = { 12, 13, 14, 15, 16, 17, 18, 19 };

            ICryptoTransform encryptor = des.CreateEncryptor(Key, IV);

            byte[] IDToBytes = Encoding.ASCII.GetBytes($"{value}");
            byte[] encryptedID = encryptor.TransformFinalBlock(IDToBytes, 0, IDToBytes.Length);
            return Convert.ToBase64String(encryptedID);
        }
    }
}