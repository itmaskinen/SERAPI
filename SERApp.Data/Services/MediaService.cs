﻿using Azure.Storage.Blobs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace SERApp.Service.Services
{
    public class MediaService
    {
        private readonly string storagePath = ConfigurationManager.AppSettings["storagePath"];
        private readonly string storageConnectionString = ConfigurationManager.AppSettings["azurestorageconnectionstring"];
        private readonly string storageParentFolder = ConfigurationManager.AppSettings["storageParentFolder"];
        private BlobServiceClient blobServiceClient;
        public MediaService()
        {
            blobServiceClient = new BlobServiceClient(this.storageConnectionString);
        }
        public List<string> GetMedias(string prefix)
        {
            var containerClient = blobServiceClient.GetBlobContainerClient(storageParentFolder);
            var blobs = containerClient.GetBlobs(prefix: prefix).OrderByDescending(e => e.Properties.CreatedOn);

            List<string> names = new List<string>();

            foreach (var blob in blobs)
            {
                names.Add(string.Format("{0}/{1}/{2}", storagePath, storageParentFolder, blob.Name));
            }
            return names;
        }

        public bool HasMedias(string prefix)
        {
            try
            {
                var containerClient = blobServiceClient.GetBlobContainerClient(storageParentFolder);
                var blobs = containerClient.GetBlobs(prefix: prefix);
                return blobs.Any();
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
