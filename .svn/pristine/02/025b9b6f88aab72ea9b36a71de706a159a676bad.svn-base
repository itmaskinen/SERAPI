﻿using Hangfire;
using Hangfire.Storage;
using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class ReportTypeScheduleController : ApiController
    {
        private AccountService accountService;
        private ReportTypeScheduleService reportTypeScheduleService;
        private IncidentService incidentService;
        private ModuleService moduleService;

        public ReportTypeScheduleController()
        {
            this.accountService = new AccountService();
            this.reportTypeScheduleService = new ReportTypeScheduleService();
            this.incidentService = new IncidentService();
            this.moduleService = new ModuleService();
        }

        [HttpGet]
        [Route("ReportTypeSchulde")]
        public IHttpActionResult Get()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = reportTypeScheduleService.GetAll();
                return new ResponseDataModel<List<ReportTypeScheduleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Account Data",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("ReportTypeSchulde")]
        public IHttpActionResult Post([FromBody] ReportTypeScheduleModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = reportTypeScheduleService.Add(model);

                if (value)
                {
                    var incidents = this.incidentService.GetIncidents(accountId: model.AccountId, checkForSiteId: false);
                    var jobs = Hangfire.JobStorage.Current.GetConnection().GetRecurringJobs();

                    incidents.Where(e => !e.IsDeleted).ToList().ForEach(incident => 
                    {
                        var account = this.accountService.Get(incident.AccountId);

                        incident.Reports.ForEach(report => 
                        {
                            var module = this.moduleService.Get(model.ModuleId);
                            var jobId = $"{account.Name} {module.Title} {report.ReportType.Name} ({incident.Id}, {report.Id})";
                            var job = jobs.FirstOrDefault(e => e.Id == jobId);

                            if (job != null)
                            {
                                RecurringJob.AddOrUpdate<SendReport>(jobId, e => e.Send(incident.Id, report.Id, System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump")), model.Cron);
                            }
                        });
                    });
                }

                return new ResponseDataModel<ReportTypeScheduleModel>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value ? "Successfully Saved Incident Type" : "Type 1 already exists",
                    Data = model
                };
            }));
        }
    }
}
