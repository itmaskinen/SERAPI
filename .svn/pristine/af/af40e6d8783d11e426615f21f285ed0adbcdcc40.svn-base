﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Data.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class EmailController : ApiController
    {
        private EmailService _emailService;
        public EmailController()
        {
            _emailService = new EmailService();
        }

        [HttpGet]
        [Route("Email/GetEmailTemplate")]
        [ResponseType(typeof(ResponseDataModel<EmailTemplateModel>))]
        public IHttpActionResult GetEmailTemplate(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _emailService.GetEmailTemplate(id);
                return new ResponseDataModel<EmailTemplateModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Email Template Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Email/GetAllEmailTemplate")]
        [ResponseType(typeof(ResponseDataModel<List<EmailTemplateModel>>))]
        public IHttpActionResult GetAllEmailTemplate()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _emailService.GetAllEmailTemplate();
                return new ResponseDataModel<List<EmailTemplateModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Email Template Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Email/GetAllNonSystemEmailTemplates")]
        [ResponseType(typeof(ResponseDataModel<List<EmailTemplateModel>>))]
        public IHttpActionResult GetAllNonSystemEmailTemplates()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _emailService.GetAllNonSystemEmailTemplates();
                return new ResponseDataModel<List<EmailTemplateModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Non Systen Email Template Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Email/GetAllAccountEmailTemplate")]
        [ResponseType(typeof(ResponseDataModel<List<AccountEmailTemplateModel>>))]
        public IHttpActionResult GetAllAccountEmailTemplate(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _emailService.GetAllAccountEmailTemplate(id);
                return new ResponseDataModel<List<AccountEmailTemplateModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Account Email Template Data",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Email/SaveEmailTemplate")]
        public IHttpActionResult SaveEmailTemplate(EmailTemplateModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _emailService.SaveEmailTemplate(model);
                return new ResponseDataModel<EmailTemplateModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved EmailTemplateModel Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Email/SaveAccountEmailTemplate")]
        public IHttpActionResult SaveAccountEmailTemplate(AccountEmailTemplateModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _emailService.SaveAccountEmailTemplate(model);
                return new ResponseDataModel<AccountEmailTemplateModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved AccountEmailTemplateModel Record",
                    Data = model
                };
            }));
        }
    }
}
