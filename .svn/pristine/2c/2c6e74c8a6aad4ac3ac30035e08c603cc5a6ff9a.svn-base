﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IIncidentService
    {
        void AddIncident(IncidentModel model);
        IEnumerable<IncidentModel> GetIncidents(int id);
        IncidentModel GetIncident(int id);
        void DeleteIncident(int id);
        void EditIncident(IncidentModel model);

        void AddReports(List<ReportModel> models);
    }
    public class IncidentService : IIncidentService
    {
        private IIncidentRepository _repository;
        private IRepository<IncidentGuard> _incidentGuardRepository;
        private IRepository<Report> _reportRepository;
        public IncidentService()
        {
            _repository = new IncidentRepository();
            _incidentGuardRepository = new Repository<IncidentGuard>();
            _reportRepository = new Repository<Report>();
        }

        public void AddIncident(IncidentModel model)
        {

            
            if (model.Id > 0)
            {

                var existingIncidentGuards = _incidentGuardRepository.GetByPredicate(x => x.IncidentId == model.Id).ToList();
                existingIncidentGuards.ForEach(x => 
                {
                    _incidentGuardRepository.Delete(x.Id);
                });

                var existingReports = _reportRepository.GetByPredicate(x => x.IncidentId == model.Id).ToList();
                existingReports.ForEach(x => 
                {
                    _reportRepository.Delete(x.Id);
                });


                var existingIncident = _repository.Get(model.Id);
                existingIncident.IncidentTypeId = model.IncidentTypeId;
                existingIncident.PoliceResponse = model.PoliceResponse;
                existingIncident.Quantity = model.Quantity;
                existingIncident.TenantId = model.TenantId;
                existingIncident.Time = model.Time;

                existingIncident.IncidentGuard = model.IncidentGuard.Select(x => new IncidentGuard() {
                    IncidentId = existingIncident.Id,
                    GuardId = x.GuardId
                }).ToList();

                existingIncident.Reports = model.Reports.Select(x => new Report()
                {
                    AdditionalText = x.AdditionalText,
                    Address = x.Address,
                    PoliceAuthority = x.PoliceAuthority,
                    Cause = x.Cause,
                    Date = x.Date,
                    GuardId = x.GuardId,
                    IncidentText = x.IncidentText,
                    Phone = x.Phone,
                    Location = x.Location,
                    Plaintiff = x.Plaintiff,
                    PoliceReport = x.PoliceReport,
                    Resolution = x.Resolution,
                    Time = x.Time,
                    TenantId = x.TenantId,
                    ReportTypeId = x.ReportTypeId,
                }).ToList();

                _repository.Update(existingIncident);
            }
            else
            {
                _repository.Save(new Incident()
                {
                    ActionDescription = model.ActionDescription,
                    CustomTypeId = model.CustomTypeId,
                    IncidentTypeId = model.IncidentTypeId,
                    PoliceResponse = model.PoliceResponse,
                    Quantity = model.Quantity,
                    TenantId = model.TenantId,
                    Time = model.Time,
                    IncidentGuard = model.IncidentGuard.Select(x => new IncidentGuard()
                    {
                        GuardId = x.GuardId
                    }).ToList(),
                    Reports = model.Reports.Select(x=> new Report()
                    {
                        AdditionalText = x.AdditionalText,
                        Address = x.Address,
                        PoliceAuthority = x.PoliceAuthority,
                        Cause = x.Cause,
                        Date = x.Date,
                        GuardId = x.GuardId,
                        IncidentText = x.IncidentText,
                        Phone = x.Phone,
                        Location = x.Location,
                        Plaintiff = x.Plaintiff,
                        PoliceReport = x.PoliceReport,
                        Resolution = x.Resolution,
                        Time = x.Time,
                        TenantId = x.TenantId,
                        ReportTypeId = x.ReportTypeId,                        
                    }).ToList()
                });
            }
        }

        public void AddReports(List<ReportModel> models)
        {
            throw new NotImplementedException();
        }

        public void DeleteIncident(int id)
        {
            _repository.Delete(id);
        }

        public void EditIncident(IncidentModel model)
        {
            var existingData = _repository.Get(model.Id);
            existingData.IncidentTypeId = model.IncidentTypeId;
            existingData.PoliceResponse = model.PoliceResponse;
            existingData.Quantity = model.Quantity;
            existingData.TenantId = model.TenantId;
            existingData.Time = model.Time;
            _repository.Update(existingData);
        }

        public IncidentModel GetIncident(int id)
        {
            var existingData = _repository.GetWithChildsIncluded(id);
            var model = new IncidentModel()
            {
                ActionDescription = existingData.ActionDescription,
                CustomTypeId = existingData.CustomTypeId,
                Id = existingData.Id,
                IncidentTypeId = existingData.IncidentTypeId,
                Quantity = existingData.Quantity,
                PoliceResponse = existingData.PoliceResponse,
                TenantId = existingData.TenantId,
                Time = existingData.Time,
                Reports = existingData.Reports.Select(r => new ReportModel()
                {
                    AdditionalText = r.AdditionalText,
                    Address = r.Address,
                    PoliceAuthority = r.PoliceAuthority,
                    Cause = r.Cause,
                    Date = r.Date,
                    GuardId = r.GuardId,
                    Id = r.Id,
                    IncidentText = r.IncidentText,
                    Location = r.Location,
                    Phone = r.Phone,
                    Plaintiff = r.Plaintiff,
                    ReportTypeId = r.ReportTypeId,
                    PoliceReport = r.PoliceReport,
                    Time = r.Time,
                    TenantId = r.TenantId,
                    Resolution = r.Resolution,
                    ReportType = new ReportTypeModel()
                    {
                        Id = r.ReportType.Id,
                        Name = r.ReportType.Name
                    }
                })
                .ToList(),
                IncidentGuard = existingData.IncidentGuard.Select(x=> new IncidentGuardModel() {
                    Guard = new GuardModel() 
                    {
                        Id = x.Guard.Id,
                        Name = x.Guard.Name,
                        SiteId = x.Guard.SiteId,
                    },
                    GuardId = x.GuardId,
                    IncidentId = x.IncidentId
                }).ToList()
            };
            return model;
        }

        public IEnumerable<IncidentModel> GetIncidents(int id)
        {

            if (id > 0)
            {
                return _repository.GetAllIncidentsChildIncluded()
                    .Where(x => x.Tenant.Site.AccountId == id).Select(x => new IncidentModel()
                    {
                        ActionDescription = x.ActionDescription,
                        CustomTypeId = x.CustomTypeId,
                        Id = x.Id,
                        IncidentTypeId = x.IncidentTypeId,
                        Quantity = x.Quantity,
                        PoliceResponse = x.PoliceResponse,
                        TenantId = x.TenantId,
                        Time = x.Time,
                        Reports = x.Reports.Select(r => new ReportModel()
                        {
                            AdditionalText = r.AdditionalText,
                            Address = r.Address,
                            PoliceAuthority = r.PoliceAuthority,
                            Cause = r.Cause,
                            Date = r.Date,
                            GuardId = r.GuardId,
                            Id = r.Id,
                            IncidentText = r.IncidentText,
                            Location = r.Location,
                            Phone = r.Phone,
                            Plaintiff = r.Plaintiff,
                            ReportTypeId = r.ReportTypeId,
                            PoliceReport = r.PoliceReport,
                            Time = r.Time,
                            TenantId = r.TenantId,
                            Resolution = r.Resolution,
                            ReportType = new ReportTypeModel()
                            {
                                Id = r.ReportType.Id,
                                Name = r.ReportType.Name
                            }
                        }).ToList(),
                        CustomType = new CustomTypeModel()
                        {
                            Id = x.CustomType.Id,
                            Name = x.CustomType.Name
                        },
                        IncidentType = new IncidentTypeModel()
                        {
                            Id = x.IncidentType.Id,
                            Name = x.IncidentType.Name,
                            IsReportRequired = x.IncidentType.IsReportRequired,
                            RequiredReport = x.IncidentType.RequiredReport.Split(',')
                            .Select(r => new ReportTypeModel()
                            {
                                Id = ConvertToInt(r),
                            })
                            .ToList()
                        },
                        Tenant = new TenantModel()
                        {
                            Id = x.Tenant.Id,
                            Name = x.Tenant.Name
                        },
                        IncidentGuard = x.IncidentGuard.Select(y => new IncidentGuardModel()
                        {
                            Id = y.Id,
                            Guard = new GuardModel()
                            {
                                Id = y.Guard.Id,
                                Name = y.Guard.Name,
                            }
                        }).ToList()
                    }).ToList();
            }


            return _repository.GetAllIncidentsChildIncluded().Select(x => new IncidentModel()
            {
                ActionDescription = x.ActionDescription,
                CustomTypeId = x.CustomTypeId,
                Id = x.Id,
                IncidentTypeId = x.IncidentTypeId,
                Quantity = x.Quantity,
                PoliceResponse = x.PoliceResponse,
                TenantId = x.TenantId,
                Time = x.Time,
                CustomType = new CustomTypeModel()
                {
                    Id = x.CustomType.Id,
                    Name = x.CustomType.Name
                },
                IncidentType = new IncidentTypeModel()
                {
                    Id = x.IncidentType.Id,
                    Name = x.IncidentType.Name
                },
                Tenant = new TenantModel()
                {
                    Id = x.Tenant.Id,
                    Name = x.Tenant.Name
                },
                IncidentGuard = x.IncidentGuard.Select(y => new IncidentGuardModel()
                {
                    Id = y.Id,
                    Guard = new GuardModel()
                    {
                        Id = y.Guard.Id,
                        Name = y.Guard.Name,
                    }
                }).ToList()
            }).ToList();
        }
        private int ConvertToInt(string number)
        {
            int num = 0;
            Int32.TryParse(number, out num);
            return num;
        }
    }
}
