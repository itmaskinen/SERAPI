import { Component, OnInit } from '@angular/core';
import { TagService } from '../../services/tag.service';
import { SiteService, TenantService } from '../../services';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { TagModel } from '../../models';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-tags-create',
  templateUrl: './tags-create.component.html',
  styleUrls: ['./tags-create.component.scss']
})
export class TagsCreateComponent implements OnInit {
  isAdminOrUserEditor: boolean;
  photoSrc: string;
  model : any;
  SiteId: number;
  sites: any[];
  tenants: any[];
  currentUser: any;

  public showSpinner: boolean = false;
  constructor(private tagService: TagService, 
    private siteService: SiteService,
    private tenantService: TenantService,
    private route: ActivatedRoute,
    private toastr: ToastsManager,
    private router: Router) { }

  ngOnInit() {
    this.setDefaults();
    this.model = new TagModel();    
    this.siteService.getSitesByAccountList(this.currentUser.accountId)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        this.sites = response.Data;
        this.toastr.success(response.Message, "Success"); 
    });  


  }

  loadTenant()
  {
    this.tenants = [];
    this.tenantService.getTenantBySiteId(this.SiteId)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        
        this.tenants = response.Data;
        this.toastr.success(response.Message, "Success"); 
    }); 

  }

  public setDefaults(){
    let now = new Date();
    this.photoSrc = "/assets/images/upload-empty.png";
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser.role === 'admin')
    {
        this.isAdminOrUserEditor = true;
    }
    else if(this.currentUser.role === 'user')
    {
        this.isAdminOrUserEditor = false;
    }
}

  public saveChanges() : void{
    this.showSpinner = true;
    this.tagService.saveTag(this.model)
    .catch((err: any) => {
        this.toastr.error(err, 'Error');
        return Observable.throw(err);
    }).finally(() => {
         setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response) => {
        if(response.ErrorCode)
        {
            this.toastr.error(response.Message, "Error"); 
        }
        else {
            this.toastr.success(response.Message, "Success"); 
        }
        this.ngOnInit();
    });
}
}
