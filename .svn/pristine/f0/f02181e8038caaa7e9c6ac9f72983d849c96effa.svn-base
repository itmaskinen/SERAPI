import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../../../core/interfaces';
import { routerTransition } from '../../../../router.animations';

import { ToastsManager } from 'ng2-toastr';

import { ManagerService } from '../../../../services';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    isActive: boolean = false;
    showModule = false;
    showMenu: string = '';
    pushRightClass: string = 'push-right';
    currentUser: any;
    isSuperAdmin: boolean = false;
    public showSpinner: boolean = false;
    public itemPerPage: number = 10;
    public currentPage: number = 1;
    public modules: Array<any>;
    constructor(
        private translate: TranslateService, 
        public router: Router,
        private managerService: ManagerService
        ) {
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    public ngOnInit(){
        let currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.currentUser = currentUserData;
        if(this.currentUser.role == 'superadmin'){
            this.isSuperAdmin = true;
            this.addExpandClass('administration')
        }
        this.loadModulesByAccount(this.currentUser.accountId);
    }

    public loadModulesByAccount(id:number){
        this.managerService.getModulesByAccountIdList(id)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                 setTimeout(() => {
                    this.showSpinner = false;
                }, 1000);
            })
            .subscribe((response : any) => {
                let data = response.Data;
                let adminDefaultModuleNodes = [
                    {'ShortName' : 'administration', 'level' : 'main' }, 
                    {'ShortName' : 'accounts' , 'level' : 'sub'},
                    {'ShortName' : 'users' , 'level' : 'sub'}, 
                    {'ShortName' : 'sites' , 'level' : 'sub'}];
                let dataModules = data.map((item) => {
                    return {
                        ShortName : item.ShortName,
                        level: item.NodeLevel
                    }
                });
                this.modules = dataModules.concat(adminDefaultModuleNodes);
                this.modules.map((item) => {
                    this.setModuleVisibilityByRole(item.ShortName);
                })
            });
    }

    public eventCalled() {
        this.isActive = !this.isActive;
    }

    public addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    public isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    public toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    public rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    public changeLang(language: string) {
        this.translate.use(language);
    }

    public onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }

    public  setModuleVisibilityByRole(moduleName:string){
        let nodeItems = [];
        if(this.currentUser.role === 'superadmin')
        {
            let superAdminModules = { 
            'modules': [
               {'ShortName' : 'administration', 'level' : 'main' }, 
               {'ShortName' : 'modules' , 'level' : 'sub'}, 
               {'ShortName' : 'accounts' , 'level' : 'sub'},
               {'ShortName' : 'account-modules' , 'level' : 'sub'}, 
               {'ShortName' : 'users' , 'level' : 'sub'}, 
               {'ShortName' : 'sites' , 'level' : 'sub'}] 
            };

            let superAdminModuleNodes = superAdminModules['modules'];
            nodeItems = superAdminModuleNodes;
        }
        else if(this.currentUser.role === 'admin')
        {
            nodeItems = this.modules;
        }
        else if(this.currentUser.role === 'user'){
            //default modules by account
            let userModuleNodes = [];
            userModuleNodes = this.modules;
            nodeItems = userModuleNodes;
             //should depend on the assigned information
             //code here should overwrite the account module assigned
        }
        if(nodeItems && nodeItems.length > 0)
        {
            let nodesFiltered = nodeItems.filter(function(item){
                return moduleName === item.ShortName;
            });
            return nodesFiltered.length > 0;
        }
        return false;
    }
}
