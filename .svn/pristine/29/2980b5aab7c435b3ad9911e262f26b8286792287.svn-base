﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IncidentController : ApiController
    {
        private IncidentService _service;
        private EmailService _emailService;
        public IncidentController()
        {
            _service = new IncidentService();
            _emailService = new EmailService();
        }

        [HttpGet]
        [Route("Incident/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncident(id);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetAll")]
        public IHttpActionResult GetAll(int accountId =0,int siteId=0,string selectedDate = "",int selectedType1 = 0, int selectedType2 = 0, int selectedType3 = 0, string searchText = "")
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncidents(accountId,siteId,selectedDate,selectedType1,selectedType2,selectedType3, searchText);
                return new ResponseDataModel<IEnumerable<IncidentModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetGuardsToday")]
        public IHttpActionResult GetGuardsToday(int accountId = 0, int siteId = 0, string selectedDate = "")
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetGuardsToday(accountId, siteId, selectedDate);
                return new ResponseDataModel<IEnumerable<GuardModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Incident/GetIncidentRange")]
        public IHttpActionResult GetIncidentRange([FromBody]WeekReportModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncidentRange(model);
                return new ResponseDataModel<WeekReportOverallReportModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = data
                };
            }));
        }
        

        [HttpPost]
        [Route("Incident/Save")]
        public IHttpActionResult Save(IncidentModel model)
        {   
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.AddIncident(model);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Incident",
                    Data = model
                };
            }));
        }
        [HttpGet]
        [Route("Incident/GetAllSection")]
        public IHttpActionResult GetAllSection(int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetSectionModels().Where(r=>r.SiteId == siteId).OrderBy(x=>x.SectionValue);
                return new ResponseDataModel<IEnumerable<SectionModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Sections",
                    Data = data
                };
            }));
        }
      

        [HttpPost]
        [Route("Incident/Delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.DeleteIncident(id);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Incident",
                };
            }));
        }

        [HttpPost]
        [Route("Incident/SendDailyReport")]
        public IHttpActionResult SendDailyReport([FromBody]ReportEmailModel base64Data)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {

                var template = _emailService.GetEmailTemplateFromRoutineName(base64Data.routineName);

                base64Data.recepients.ToList().ForEach(e =>
                {
                    Service.Tools.Email.SendEmailWithAttachment("", e, template.Subject,
                        template.Body, "", base64Data.base64);
                });
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Sent Daily Report",
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetExcel")]
        public HttpResponseMessage GetExcel()
        {
            var path = @"C:\Users\lemon\Desktop\New folder (4)\EXPENSES.xlsx";
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            result.Content = new StreamContent(stream);
            //result.Content.Headers.ContentType =
            //    new MediaTypeHeaderValue("application/octet-stream");
            return result;
        }
    }
}
