﻿using Newtonsoft.Json;
using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class CommonController : ApiController
    {
        private CommonService _commonService;
        public CommonController()
        {
            _commonService = new CommonService();
        }

        [HttpGet]
        public IHttpActionResult GetAllFacilites()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetAllFacilites();
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully loaded All Facilities",
                    Data = data
                };
            }));
        }

        [HttpGet]
        public IHttpActionResult GetAllLoanTypes()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetAllLoanTypes();
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully loaded All Loan Types",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("Common/GetAllCompanyTypes")]
        public IHttpActionResult GetAllCompanyTypes()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetAllCompanyTypes();
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Company Types",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetAllUserRoles")]
        public IHttpActionResult GetAllUserRoles()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetAllUserRoles();
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All User Roles",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetDashboardCounters")]
        [ResponseType(typeof(ResponseDataModel<DashboardModel>))]
        public IHttpActionResult GetDashboardCounters()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetDashboardCounters();
                return new ResponseDataModel<DashboardModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Dashboard Counters",
                    Data = data,
                    TotalRecords = 1
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetDashboardLogCounters")]
        [ResponseType(typeof(ResponseDataModel<DashboardLogModel>))]
        public IHttpActionResult GetDashboardLogCounters()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetDashboardLogCounters();
                return new ResponseDataModel<DashboardLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Dashboard Log Counters",
                    Data = data,
                    TotalRecords = 1
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetMostRecentLogs")]
        [ResponseType(typeof(ResponseDataModel<LogModel>))]
        public IHttpActionResult GetMostRecentLogs()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetMostRecentLogs();
                return new ResponseDataModel<List<LogModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Most Recent Logs",
                    Data = data,
                    TotalRecords = 1
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetErrorLogs")]
        [ResponseType(typeof(ResponseDataModel<LogModel>))]
        public IHttpActionResult GetErrorLogs()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetErrorLogs();
                return new ResponseDataModel<List<LogModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Error Logs",
                    Data = data,
                    TotalRecords = 1
                };
            }));
        }
    }
}