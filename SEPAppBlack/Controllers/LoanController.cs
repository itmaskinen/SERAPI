﻿using Newtonsoft.Json;
using SERApp.Models;
using SERApp.Repository.Helpers;
using SERApp.Service.Models.ResponseModels;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEPAppBlack.Controllers
{
    public class LoanController : Controller
    {
        private LoanService _loanService;
        public LoanController()
        {
            _loanService = new LoanService();
        }

        [HttpGet]
        public JsonResult Get(int id)
        {
            var data = _loanService.Get(id);
            var response = JsonConvert.SerializeObject(new ResponseDataModel<LoanModel>
            {
                success = true,
                message = "Successfully Loaded Loan Data",
                data = data
            });
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAll()
        {
            var data = _loanService.GetAll();
            var response = JsonConvert.SerializeObject(new ResponseDataModel<List<LoanModel>>
            {
                success = true,
                message = "Successfully Loaded All Loan Data List",
                data = data
            });
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllWithFilter(int customerId, int typeId, int statusId, string itemName,
            string LoanedDate, string ReturnedDate, int confirmationType)
        {
            var data = _loanService.GetAllWithFilter(customerId, typeId, statusId, itemName, LoanedDate, ReturnedDate, confirmationType);
            var response = JsonConvert.SerializeObject(new ResponseDataModel<List<LoanModel>>
            {
                success = true,
                message = "Successfully Loaded Filtered Loan Data List",
                data = data
            });
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveLoan(LoanModel model)
        {
            try
            {
                _loanService.SaveLoan(model);
                var response = JsonResponseHelper.JsonSuccess("Successfully Saved Loan Record");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var response = JsonResponseHelper.JsonError(ex.Message, ex.HResult);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteLoan(int id)
        {
            try
            {
                _loanService.DeleteLoan(id);
                var response = JsonResponseHelper.JsonSuccess("Successfully Deleted Loan Record");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var response = JsonResponseHelper.JsonError(ex.Message, ex.HResult);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

    }
}