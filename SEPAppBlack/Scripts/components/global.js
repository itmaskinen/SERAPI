﻿(function () {
    //Generic Objects
    var initGlobal = function () {
        var SERApp = {
            ConfirmationTypes: [],
            Facilities: [],
            LoanTypes: [],
            Status: [
                { Name: 'All', Id: 0 },
                { Name: 'Active', Id: 1 },
                { Name: 'Returned', Id: 2 },
                { Name: 'Deleted / Archived', Id: 3 }
            ]
        };
    }

    _.defer(function () {
        initGlobal();
    })
});
