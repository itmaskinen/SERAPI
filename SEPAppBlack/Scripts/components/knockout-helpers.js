(function (ko) {
    // Knockout Extensions
    if (!ko.ext) {
        ko.ext = {}
    }

    ko.ext.renderComponent = function (element, name, params) {
        // container view model
        var Vm = function (n, p) {
            this.c = ko.observable({
                name: n,
                params: p
            })
        }

        // template html
        var div = $('<div data-bind="component: c"></div>')

        // element
        var el = $(element)
        if (!el.length) {
            console.warn("cannot find element for component", name, element)
            return
        }
        el.addClass("hide"); // hide this element initially

        // -- clear bindings as often being reused
        ko.ext.unapplyBindings(el)

        // clear and load
        el.empty().append(div)

        // apply
        ko.applyBindings(new Vm(name, params), el[0])

        // translate elements
        _.defer(function () {
            ko.ext.globalizeElement(el[0]);

            // display the element
            el.removeClass("hide");
        })
    }

    ko.ext.toJSON = function (model) {
        // this is a hack for dates.
        return JSON.parse(ko.mapping.toJSON(model))
    }

    ko.ext.globalizeElement = function (el) {
        var elems = $(el)
            .find('.translate');
        elems
            .each(function (item) {
                var elem = $(this);
                var rawText = elem.text();
                var translatedText = WpsApp.translate(rawText); // self.t(rawText);
                elem.text(translatedText);
                elem.removeClass('.translate');
            });
    }

    ko.ext.isValidEmail = function (email) {
        var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

        return $.trim(email).match(pattern) ? true : false;
    }

    // http://stackoverflow.com/a/13459885/51507
    ko.ext.unapplyBindings = function ($node) {

        // clean node first up anyway
        $node.off();
        ko.cleanNode($node[0])

        // now destroy the children
        $node.find("*").each(function (index, item) {
            $(item).off();
            ko.removeNode(item);
            $(item).remove();
            item = null;
        });

        $node.find("*").each(function (index, item) {
            $(item).off();
            ko.removeNode(item);
            $(item).remove();
            item = null;
        });
    }

    
})(ko);