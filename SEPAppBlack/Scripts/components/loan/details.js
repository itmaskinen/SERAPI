﻿(function (ko) {
    var componentId = "loan-details";
    //private functions
    var getLoan = function (loanId) {
        return $.ajax({
            url: '/Loan/Get?id=' + loanId,
            type: 'json',
            method: 'get'
        })
    }

    var ViewModel = function (params) {
        var self = this;
        self.editTitle = ko.observable('Edit Loan');
        self.loanData = ko.observable([]);
        self.confirmationTypeData = ko.observable([]);
        self.customerData = ko.observable([]);
        self.loanTypeData = ko.observable([]);
        self.facilityData = ko.observable([]);
        self.confirmationAudit = ko.observable([]);

        self.editMode = ko.observable(false);
        self.searchCustomerEnabled = ko.observable(true);
        self.customerFieldsEnabled = ko.observable(false);
        self.editEnabled = ko.observable(false);

        self.facilities = ko.observableArray([]);
        self.types = ko.observableArray([]);

        //events
        self.existensRadioClick = function (value) {
            if (value == 0) //exists
            {
                self.searchCustomerEnabled(true);
                self.customerFieldsEnabled(false);
            }
            else //new
            {
                self.searchCustomerEnabled(false);
                self.customerFieldsEnabled(true);
            }
            return true;
        }

        self.backToList = function () {
            ko.ext.renderComponent("#main-component-container", "loan-list");
        }

        self.openReturnForm = function () {
            bootbox.dialog({
                title: 'Return Loan Details',
                message: '<div id="modal-container"><p><i class="fa fa-spin fa-spinner"></i> Loading...</p></div>',
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-md btn-info'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-md btn-default'
                    }
                },
                callback: function (result) {
                    //console.log('This was logged in the callback: ' + result);
                }
            })
            setTimeout(function () {
                ko.ext.renderComponent("#modal-container", "return-loan-details");
            }, 1000);
        }

        self.openResendConfirmationForm = function () {
            bootbox.dialog({
                title: 'Resend Confirmation',
                message: '<div id="modal-container"><p><i class="fa fa-spin fa-spinner"></i> Loading...</p></div>',
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-md btn-info'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-md btn-default'
                    }
                },
                callback: function (result) {
                }
            })
            setTimeout(function () {
                ko.ext.renderComponent("#modal-container", "return-loan-details");
            }, 1000);
        }

        self.initUI = function () {
            if (params.Source == 'list-preview') {
                self.editEnabled(false);
                self.editMode(false);

                self.loanData(params.Loan);
                self.confirmationTypeData(params.Loan.ConfirmationType);
                self.customerData(params.Loan.Customer);
                self.loanTypeData(params.Loan.LoanType);
                self.ConfirmationTypeSetValue(params.Loan.ConfirmationType.Id);
                self.facilityData(params.Loan.Customer.Facility)

                self.editTitle('Loan Preview');
            }
            else {
                self.editEnabled(true);
                self.editMode(true);
                getLoan(params.loanId).done(function (response) {
                    var responseData = JSON.parse(response);
                    var data = responseData.data;

                    self.loanData(data);
                    self.confirmationTypeData(data.ConfirmationType);
                    self.customerData(data.Customer);
                    self.loanTypeData(data.LoanType);
                    self.ConfirmationTypeSetValue(data.ConfirmationType.Id);

                    self.editTitle('Edit Lease');
                });
            }
            $('.form-control').addClass('input-sm');
            $('.datetimepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
        }

        self.ConfirmationTypeSetValue = function (data) {
            if (data == 1) {
                $('#radio-email').attr('checked', 'checked');
            }
            else if (data == 2)
            {
                $('#radio-email').attr('checked', 'checked');
            }
        }

        self.bindData = function () {
            return new Promise((resolve) => {
                var returnData = [];
                getFacilities().done(function (response) {
                    var data = JSON.parse(response).data;
                    self.facilities(data);
                    returnData.push('facilities');
                });

                getLoanTypes().done(function (response) {
                    var data = JSON.parse(response).data;
                    self.types(data);
                    returnData.push('types');

                    setTimeout(function () {
                        resolve(); //just making sure the data is loaded before setting the value
                    }, 500)

                });

                self.confirmationAudit([
                    { 'Routine': 'Email Confirmation', 'User': 'Admin', 'Status': 'Sent', 'SentTo': 'themot@gmail.com', 'SentDate': '10-01-2018 11:21:00' },
                    { 'Routine': 'SMS Resend', 'User': 'Admin', 'Status': 'Sent', 'SentTo': '0431-5564166', 'SentDate': '11-01-2018 11:21:21' },
                    { 'Routine': 'Email Confirmation', 'User': 'Gloria A Andreasson', 'SentTo': 'themot@gmail.com', 'Status': 'Confirmed', 'SentDate': '11-01-2018  12:37:40' }
                ]);

            });
        }

        self.init = function () {
            //makes sure that the dom is loaded before the events trigger
            self.bindData().then(function () {
                _.defer(function () {
                    self.initUI();
                });
            });
            
        }

        self.init();
    }

    ko.components.register(componentId, {
        viewModel: ViewModel,
        template: { element: "t-" + componentId }
    });
})(ko);