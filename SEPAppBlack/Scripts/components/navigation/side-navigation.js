﻿(function (ko) {
    var componentId = "side-menu-navigation";
    var ViewModel = function (params) {
       self.navigate = function (component) {
           ko.ext.renderComponent("#main-component-container", component);
       }
       self.init = function () {

       }
        self.init();
    }

    ko.components.register(componentId, {
        viewModel: ViewModel,
        template: { element: "t-" + componentId }
    });
})(ko);